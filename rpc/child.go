package rpc

import (
	"net"
	"runtime/debug"
	"time"

	"gitee.com/nodep/nframe/log"
)

//通过config创建一个Rpc
//@return nil
func NewChild(cfg *RpcCfg, onLine func(context *RpcContext), offLine func(context *RpcContext), group int32, node int64) *Child {
	m := &Child{
		cfg:             cfg,
		Deadline:        time.Second * time.Duration(cfg.Timeout),
		privateHandle:   make(map[int32]func(msg []byte, context *RpcContext), 2),
		OnlineCallback:  onLine,
		OfflineCallback: offLine,
		firstLink:       true,
	}
	joinMsg := &Msg_1001{}
	joinMsg.Group = group
	joinMsg.Node = node
	joinMsg.Url = cfg.Door
	m.joinMsg = joinMsg.Marshal()
	m.privateHandle[CMD_Heart] = m.connectHandle
	return m
}

type Child struct {
	cfg *RpcCfg
	RpcWhiteFilter
	Deadline        time.Duration                                   //读数据过期时间,过期则认为连接断开
	OnlineCallback  func(context *RpcContext)                       //节点接入
	OfflineCallback func(context *RpcContext)                       //节点关闭
	privateHandle   map[int32]func(msg []byte, context *RpcContext) //内部响应函数
	firstLink       bool                                            //首次连接
	joinMsg         []byte                                          //加入消息
}

//连接成功
func (this *Child) connectHandle(msg []byte, context *RpcContext) {
	this.OnlineCallback(context)
}

//重连接
func (this *Child) relink(err error) {
	if this.firstLink {
		if err != nil {
			panic(err)
		}
		return
	} else {
		time.Sleep(time.Second)
		go this.Run()
	}
}

//启动对主节点的连接
func (this *Child) Run() {
	log.Admin.Sys("启动对master的连接", this.cfg.Master)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", this.cfg.Master)
	if err != nil {
		this.relink(err)
		return
	}
	conn, err := net.DialTCP("tcp4", nil, tcpAddr)
	if err != nil {
		this.relink(err)
		return
	}
	conn.SetKeepAlive(true)
	context := &RpcContext{
		Conn:   conn,
		Online: true,
		Handle: make(map[int32]func(n int64, msg []byte)),
	}
	this.firstLink = false
	defer func() {
		log.Admin.Debug("对master的连接断开")
		this.OfflineCallback(context)
		context.Close()
		if r := recover(); r != nil {
			this.relink(err) //尝试重新连接
			if log.Admin.Lv >= log.Lv_Debug {
				debug.PrintStack()
			}
		}
	}()
	//------------------循环读取消息------------------
	full := false
	var buf []byte = make([]byte, 64, 64)
	var newBuff []byte
	var pack *socketData = &socketData{
		msg: make([]byte, 0, 518),
	}
	pack.clear()
	go func() {
		tick := time.NewTicker(time.Second)
		for {
			select {
			case <-tick.C:
				if !context.Online {
					tick.Stop()
					return
				}
				context.SendPack(CMD_Heart, nil)
			}
		}
	}()
	//-------------发送join-------------
	context.SendBytes(CMD_Join, this.joinMsg)
	for {
		n, err := conn.Read(buf) //从conn中读取字符
		if err != nil {
			panic(err)
		} else {
			buf = buf[:n]
		}
		newBuff = append(newBuff, buf...)
		if len(newBuff) < 4 {
			continue
		}
		newBuff, full = pack.fromBytes(newBuff)
		if !full { //非完整包
			continue
		}
		if this.privateHandle[pack.cmd] != nil { //属于内部通信
			this.privateHandle[pack.cmd](pack.msg, context)
		} else { //属于外部通信
			context.Callback(pack.cmd, pack.msg)
		}
		pack.clear()
	}
}
