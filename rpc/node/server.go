package node

import (
	"bytes"
	"encoding/binary"
	"net"
	"runtime/debug"
	"sync"
	"time"

	"gitee.com/nodep/nframe/log"
	"gitee.com/nodep/nframe/rpc"
	"gitee.com/nodep/nframe/util"
)

type NodeServer struct {
	Online    func(context *rpc.RpcContext)
	Offline   func(context *rpc.RpcContext)
	port      string
	firstLink bool
	whiteDic  sync.Map
}

//TODO 还需要检查断网恢复功能
//启动
func (this *NodeServer) RunWithAddrs(port string) {
	this.port = port
	this.firstLink = true
	log.Admin.Debug("启动子节点网络对外入口", port)
	go this.startLis()
}

//开启监听
func (this *NodeServer) startLis() {
	defer func() {
		if r := recover(); r != nil {
			if this.firstLink {
				log.Admin.Error("NodeServer RunWithAddrs Go->Recover By ", r)
				panic(r) //启动失败必须报错
			} else { //其他网络原因导致则再次重新启动
				time.Sleep(time.Second * 3)
				go this.startLis()
			}
		}
	}()
	tcpAddr, err := net.ResolveTCPAddr("tcp4", ":"+this.port)
	checkErr(err)
	listener, err := net.ListenTCP("tcp4", tcpAddr)
	checkErr(err)
	for {
		conn, err := listener.Accept()
		if err != nil {
			if conn != nil {
				conn.Close()
			}
			continue
		}
		go this.lis(conn)
	}
	this.firstLink = false
}

//添加监听
func (this *NodeServer) lis(conn net.Conn) {
	ip := util.IPv4(conn.RemoteAddr().String())
	log.Admin.Debug("收到子节点连接请求IP = ", ip)
	_, ok := this.whiteDic.Load(ip)
	if !ok {
		log.Admin.Debug("子节点连接不在白名单内 = ", ip)
		conn.Close()
		return
	}
	context := &rpc.RpcContext{
		Ip:     ip,
		Conn:   conn,
		Online: true,
		Handle: make(map[int32]func(n int64, msg []byte)),
	}
	defer func() {
		if context != nil {
			context.Close()
			this.Offline(context)
		}
		if r := recover(); r != nil {
			log.Admin.Error("NodeServer node break ", context.Node, context.Ip, context.Group, " ->", r)
			if log.Admin.Lv >= log.Lv_Debug {
				debug.PrintStack()
			}
		}
	}()
	full := false
	var buf []byte = make([]byte, 64, 64)
	var newBuff []byte
	var pack *socketData = &socketData{
		msg: make([]byte, 0, 518),
	}
	pack.clear()
	deadLine := time.Second * 20
	for {
		conn.SetReadDeadline(time.Now().Add(deadLine))
		n, err := conn.Read(buf) //从conn中读取字符
		if err != nil {
			break
		} else {
			buf = buf[:n]
		}
		newBuff = append(newBuff, buf...)
		if len(newBuff) < 4 {
			continue
		}
		newBuff, full = pack.fromBytes(newBuff)
		if !full { //非完整包
			continue
		}
		if pack.cmd > 0 { //内部通信
			context.Callback(pack.cmd, pack.msg)
		} else if pack.cmd == rpc.CMD_Link {
			m := rpc.Unmarshal_1003(pack.msg)
			context.Node = m.Node
			this.Online(context)
		}
		pack.clear()
	}
}

//可以添加白名单
func (this *NodeServer) AddWhiteList(url string) {
	this.whiteDic.Store(util.IPv4(url), 1)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

//封包接收
type socketData struct {
	head    bool
	Len     int
	bodyLen int32
	cmd     int32
	flag    int16
	msg     []byte
}

//清除
func (this *socketData) clear() {
	this.Len = 0
	this.bodyLen = 0
	this.cmd = 0
	this.flag = 0
	this.msg = this.msg[:0]
	this.head = false
}

//解析封包.解析后的byte以及是否一个完整的包
//@return nil,true
func (this *socketData) fromBytes(buff []byte) ([]byte, bool) {
	b_buf := bytes.NewBuffer(buff)
	if !this.head {
		binary.Read(b_buf, binary.BigEndian, &this.bodyLen)
		this.Len = int(this.bodyLen)
	}
	if len(buff) < this.Len {
		this.head = true
		return buff, false //非完整的封包
	}
	if this.head {
		binary.Read(b_buf, binary.BigEndian, &this.bodyLen)
	}
	binary.Read(b_buf, binary.BigEndian, &this.cmd)
	binary.Read(b_buf, binary.BigEndian, &this.flag)
	this.msg = buff[10:this.Len]
	this.head = true
	return buff[this.Len:], true
}
