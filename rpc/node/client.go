package node

import (
	"net"
	"sync"
	"time"

	"gitee.com/nodep/nframe/log"
	"gitee.com/nodep/nframe/net/tcp"
	"gitee.com/nodep/nframe/rpc"
	"gitee.com/nodep/nframe/tran"
)

type NodeClient struct {
	Url     string //地址
	Node    int64  //节点
	Online  func(n int64)
	Offline func(n int64)
	working bool
	sync.Mutex
	conn    *net.TCPConn
	_closed bool
}

//TODO 需要子节点主动下线的功能（实际业务中，存在减少节点数的需求）
//停止子节点
func (this *NodeClient) Stop() {
	this.Lock()
	defer this.Unlock()
	this.working = false
	if this.conn != nil {
		this.conn.Close()
	}
}

//重新连接
func (this *NodeClient) relink() {
	if this.working {
		time.Sleep(time.Second * 3)
		go this.run()
	}
}

//开始子节点
func (this *NodeClient) Start() {
	this.working = true
	go this.run()
}

func (this *NodeClient) run() {
	log.Admin.Debug("尝试连接子节点", this.Url)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", this.Url)
	if err != nil {
		this.relink()
		return
	}
	this.Lock()
	if !this.working {
		this.Unlock()
		return
	}
	conn, err := net.DialTCP("tcp4", nil, tcpAddr)
	if err != nil {
		this.Unlock()
		this.relink()
		return
	}
	alive := true
	this.Unlock()
	defer func() {
		this._closed = true
		alive = false
		this.Offline(this.Node)
		if r := recover(); r != nil {
			log.Admin.Debug("子节点连接异常断开")
			this.relink()
		}
	}()
	this.conn = conn
	conn.SetKeepAlive(true)
	this._closed = false
	this.SendPack(rpc.CMD_Link, &rpc.Msg_1003{this.Node})
	tick := time.NewTicker(time.Second * 10)
	go func() {
		for {
			select {
			case <-tick.C:
				if !alive { //已关闭的连接
					tick.Stop()
					return
				}
				this.SendBytes(rpc.CMD_Heart, nil) //发送心跳
			}
		}
	}()
	this.Online(this.Node)
	var buf []byte = make([]byte, 64, 64)
	for {
		n, err := conn.Read(buf) //从conn中读取字符
		if err != nil {
			panic(err)
		} else {
			buf = buf[:n]
		}
	}
}

//发送pack
func (this *NodeClient) SendPack(cmd int32, pack tcp.TcpPack) {
	if pack == nil {
		this.SendBytes(cmd, nil)
	} else {
		this.SendBytes(cmd, pack.Marshal())
	}
}

//发送消息
func (this *NodeClient) SendBytes(cmd int32, msg []byte) {
	if this._closed || this.conn == nil {
		return
	}
	if msg != nil {
		bf := make([]byte, 0, 10+len(msg))
		bf = append(bf, tran.Int32ToBytes_B(int32(10+len(msg)))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		bf = append(bf, msg...)
		this.conn.Write(bf)
	} else {
		bf := make([]byte, 0, 10)
		bf = append(bf, tran.Int32ToBytes_B(int32(10))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		this.conn.Write(bf)
	}
}
