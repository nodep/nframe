package rpc

import (
	"bytes"
	"encoding/binary"
	"net"
	"sync"

	"gitee.com/nodep/nframe/log"
	"gitee.com/nodep/nframe/net/tcp"
	"gitee.com/nodep/nframe/tran"
)

const (
	Req_Join int32 = -1 //请求加入节点
)

//rpc配置
type RpcCfg struct {
	Tp       string //类型master|child
	Port     string //自己所开放的端口
	Door     string //自己对外的地址
	Master   string //相对主节点地址127.0.0.1:9000
	HeartSec int64  //每隔多久向所有节点发送一次心跳包
	Timeout  int64  //多久没有收到消息视为断链
}

type RpcNodeCenter interface {
	Online(context *RpcContext)   //读节点上线
	Offline(context *RpcContext)  //读节点下线
	WorkWith(cfg *RpcCfg)         //根据RpcCfg开始工作
	AddNode(id int64, url string) //添加主节点
}

//link
type RpcLink interface {
	SendBytes(n int64, cmd int32, msg []byte)
	SendPack(n int64, cmd int32, pack tcp.TcpPack)
}

type Rpc interface {
	AddWhite(ip string)
	DelWhite(ip string)
	Run()
}

type RpcWhiteFilter struct {
	whiteDic sync.Map
}

//添加白名单
func (this *RpcWhiteFilter) AddWhite(ip string) {
	this.whiteDic.Store(ip, 1)
}

//删除白名单
func (this *RpcWhiteFilter) DelWhite(ip string) {
	this.whiteDic.Delete(ip)
}

//是否再白名单内
//@return false
func (this *RpcWhiteFilter) InWhiteList(ip string) bool {
	_, ok := this.whiteDic.Load(ip)
	return ok
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

//rpc连接上下文
type RpcContext struct {
	Ip     string   //连接到
	Group  int32    //所属组,0代表属于一个master节点
	Node   int64    //节点编号,在一个rpc关系网中的id
	Url    string   //节点地址
	Conn   net.Conn //连接
	Online bool     //当前是否在线
	// Bind   bool                       //是否以绑定
	Handle map[int32]func(n int64, msg []byte) //处理函数
}

//为上下文添加响应函数
func (this *RpcContext) AddHandle(cmd int32, handle func(n int64, msg []byte)) {
	this.Handle[cmd] = handle
}

func (this *RpcContext) Callback(cmd int32, msg []byte) {
	if this.Handle[cmd] != nil {
		this.Handle[cmd](this.Node, msg)
	} else {
		log.Admin.Debug("未注册的Rpc响应", cmd)
	}
}

//主动断开连接
func (this *RpcContext) Close() {
	if this.Online {
		this.Online = false
		this.Conn.Close()
	}
}

//发送pack
//@return true
func (this *RpcContext) SendPack(cmd int32, pack tcp.TcpPack) bool {
	if pack != nil {
		return this.SendBytes(cmd, pack.Marshal())
	} else {
		return this.SendBytes(cmd, nil)
	}
}

//发送消息
//@return true
func (this *RpcContext) SendBytes(cmd int32, msg []byte) bool {
	if !this.Online {
		return false
	}
	var err error
	if msg != nil {
		bf := make([]byte, 0, 10+len(msg))
		bf = append(bf, tran.Int32ToBytes_B(int32(10+len(msg)))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		bf = append(bf, msg...)
		_, err = this.Conn.Write(bf)
	} else {
		bf := make([]byte, 0, 10)
		bf = append(bf, tran.Int32ToBytes_B(int32(10))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		_, err = this.Conn.Write(bf)
	}
	return err == nil
}

//封包接收
type socketData struct {
	head    bool
	Len     int
	bodyLen int32
	cmd     int32
	flag    int16
	msg     []byte
}

//清除
func (this *socketData) clear() {
	this.Len = 0
	this.bodyLen = 0
	this.cmd = 0
	this.flag = 0
	this.msg = this.msg[:0]
	this.head = false
}

//解析封包.解析后的byte以及是否一个完整的包
//@return nil,true
func (this *socketData) fromBytes(buff []byte) ([]byte, bool) {
	b_buf := bytes.NewBuffer(buff)
	if !this.head {
		binary.Read(b_buf, binary.BigEndian, &this.bodyLen)
		this.Len = int(this.bodyLen)
	}
	if len(buff) < this.Len {
		this.head = true
		return buff, false //非完整的封包
	}
	if this.head {
		binary.Read(b_buf, binary.BigEndian, &this.bodyLen)
	}
	binary.Read(b_buf, binary.BigEndian, &this.cmd)
	binary.Read(b_buf, binary.BigEndian, &this.flag)
	this.msg = buff[10:this.Len]
	this.head = true
	return buff[this.Len:], true
}
