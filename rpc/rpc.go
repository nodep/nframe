package rpc
import (
	"encoding/binary"
	"gitee.com/nodep/nframe/tran"
)

const (
	//请求连接
	CMD_Join int32 = -1001
	//心跳包
	CMD_Heart int32 = -1002
	//身份识别
	CMD_Link int32 = -1003
)
type Msg_1001 struct {
	//所属组
	Group	int32
	//节点编号
	Node	int64
	//节点地址
	Url	string
}
func (this *Msg_1001) Marshal () []byte {
	rst := make([]byte , 0 , 16)
	rst = append(rst, tran.Int32ToBytes_B(this.Group)...)
	rst = append(rst, tran.Int64ToBytes_B(this.Node)...)
	rst = append(rst, tran.Int16ToBytes_B(int16(len(this.Url)))...)
	rst = append(rst, []byte(this.Url)...)
	return rst
}
func Unmarshal_1001(rst []byte) *Msg_1001 {
	this := &Msg_1001{}
	pos := 0
	this.Group = int32(binary.BigEndian.Uint32(rst[pos:pos+4]))
	pos += 4
	this.Node = int64(binary.BigEndian.Uint64(rst[pos:pos+8]))
	pos += 8
	var lenInt int
	lenInt = int(binary.BigEndian.Uint16(rst[pos:pos+2]))
	pos += 2
	this.Url = string(rst[pos:pos+lenInt])
	pos += lenInt
	return this
}
type Msg_1003 struct {
	//节点
	Node	int64
}
func (this *Msg_1003) Marshal () []byte {
	rst := make([]byte , 0 , 16)
	rst = append(rst, tran.Int64ToBytes_B(this.Node)...)
	return rst
}
func Unmarshal_1003(rst []byte) *Msg_1003 {
	this := &Msg_1003{}
	pos := 0
	this.Node = int64(binary.BigEndian.Uint64(rst[pos:pos+8]))
	pos += 8
	return this
}
