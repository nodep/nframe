package rpc

import (
	"net"
	"runtime/debug"
	"time"

	"gitee.com/nodep/nframe/log"
	"gitee.com/nodep/nframe/util"
)

//通过config创建一个Rpc
//@return nil
func NewMaster(cfg *RpcCfg, onLine func(context *RpcContext), offLine func(context *RpcContext)) Rpc {
	m := &Master{
		cfg:             cfg,
		Deadline:        time.Second * time.Duration(cfg.Timeout),
		privateHandle:   make(map[int32]func(msg []byte, context *RpcContext), 2),
		OnlineCallback:  onLine,
		OfflineCallback: offLine,
		firstLink:       true,
	}
	m.privateHandle[CMD_Join] = m.askJoin
	m.privateHandle[CMD_Heart] = m.heart
	return m
}

type Master struct {
	cfg *RpcCfg
	RpcWhiteFilter
	Deadline        time.Duration                                   //读数据过期时间,过期则认为连接断开
	OnlineCallback  func(context *RpcContext)                       //节点接入
	OfflineCallback func(context *RpcContext)                       //节点关闭
	privateHandle   map[int32]func(msg []byte, context *RpcContext) //内部响应函数
	firstLink       bool
}

//请求加入节点
func (this *Master) askJoin(msg []byte, context *RpcContext) {
	context.SendBytes(CMD_Heart, nil)
	join := Unmarshal_1001(msg)
	context.Node = join.Node
	context.Group = join.Group
	context.Url = join.Url
	this.OnlineCallback(context)
}

//心跳包
func (this *Master) heart(msg []byte, context *RpcContext) {
}

//启动master节点
func (this *Master) Run() {
	log.Admin.Sys("启动rpcMaster节点", this.cfg.Port)
	go this.startLis()
}

func (this *Master) startLis() {
	defer func() {
		if r := recover(); r != nil {
			if this.firstLink {
				log.Admin.Error("rpcMaster go->recover by ", r)
				panic(r) //启动失败,必须抛出错误并结束进程
			} else {
				time.Sleep(time.Second * 3)
				go this.startLis()
			}
		}
	}()
	tcpAddr, err := net.ResolveTCPAddr("tcp4", ":"+this.cfg.Port)
	checkErr(err)
	listener, err := net.ListenTCP("tcp4", tcpAddr)
	checkErr(err)
	for {
		conn, err := listener.Accept()
		if err != nil {
			if conn != nil {
				conn.Close()
			}
			continue
		}
		go this.lis(conn)
	}
	this.firstLink = false
}

//创建一个来至其他节点的连接
func (this *Master) lis(conn net.Conn) {
	log.Admin.Debug("建立节点连接")
	ip := util.IPv4(conn.RemoteAddr().String())
	log.Admin.Debug("子节点连接请求", ip)
	if !this.InWhiteList(ip) { //非白名单内的
		log.Admin.Debug("收到非白名单内的rpc连接请求->", ip)
		conn.Close()
		return
	}
	context := &RpcContext{
		Ip:     ip,
		Conn:   conn,
		Online: true,
		Handle: make(map[int32]func(n int64, msg []byte)),
	}
	defer func() {
		if context != nil {
			this.OfflineCallback(context)
		}
		if r := recover(); r != nil {
			log.Admin.Error("master node break ", context.Node, context.Ip, context.Group, " ->", r)
			if log.Admin.Lv >= log.Lv_Debug {
				debug.PrintStack()
			}
		}
	}()
	full := false
	var buf []byte = make([]byte, 64, 64)
	var newBuff []byte
	var pack *socketData = &socketData{
		msg: make([]byte, 0, 518),
	}
	pack.clear()
	for {
		conn.SetReadDeadline(time.Now().Add(this.Deadline))
		n, err := conn.Read(buf) //从conn中读取字符
		// log.Admin.Debug(n, "->", buf)
		if err != nil {
			// log.Admin.Debug("masterConn>Read", err, n)
			// log.Admin.Debug(buf)
			break
		} else {
			buf = buf[:n]
		}
		newBuff = append(newBuff, buf...)
		if len(newBuff) < 4 {
			continue
		}
		newBuff, full = pack.fromBytes(newBuff)
		if !full { //非完整包
			continue
		}
		if this.privateHandle[pack.cmd] != nil { //属于内部通信
			this.privateHandle[pack.cmd](pack.msg, context)
		} else { //属于外部通信
			context.Callback(pack.cmd, pack.msg)
		}
		pack.clear()
	}
	context.Close()
}
