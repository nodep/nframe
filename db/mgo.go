package db

/*
mongodb://admin:123456@localhost/
mongodb://127.0.0.1:27017/hunter?
mongodb://admin:123456@localhost/legend?
*/

import (
	"sync/atomic"
	"time"

	"gitee.com/nodep/nframe/log"

	"gitee.com/nodep/nframe/cfg"
	"gitee.com/nodep/nframe/tran"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

//@return nil,nil
func CreateMgo(cf *cfg.MgoConfig) (*MgoDb, error) {
	if cf.MinPool <= 0 || cf.MaxPool <= cf.MinPool {
		return nil, Err_DbPool
	}
	d := &MgoDb{}
	d.config = cf
	// log.Admin.Sys("linkdb = ", cf.Url+"maxPoolSize="+tran.IntToString(cf.MaxPool)+"&minPoolSize="+tran.IntToString(cf.MinPool))
	info, err := mgo.ParseURL(cf.Url + "maxPoolSize=" + tran.IntToString(cf.MaxPool) + "&minPoolSize=" + tran.IntToString(cf.MinPool))
	if err != nil {
		return nil, err
	}
	info.Timeout = time.Second * 3
	s, err := mgo.DialWithInfo(info)
	if err != nil {
		return nil, err
	}
	d.session = s
	d.database = info.Database
	return d, nil
}

type MgoDb struct {
	database string
	config   *cfg.MgoConfig
	session  *mgo.Session
	cc       int32
}

//释放一个链接
func (this *MgoDb) RelaseCollection(c *mgo.Collection) {
	c.Database.Session.Close()
	atomic.AddInt32(&this.cc, -1)
}

//@return nil
//获取某个表
func (this *MgoDb) GetCollection(cname string) *mgo.Collection {
	atomic.AddInt32(&this.cc, 1)
	return this.session.Clone().DB(this.database).C(cname)
}

//释放一个Database
func (this *MgoDb) RelaseDatabase(d *mgo.Database) {
	d.Session.Close()
	atomic.AddInt32(&this.cc, -1)
}

//@return nil
//获取数据库的连接，用于同时需要操作多个表时，所有表的操作完成后再释放连接
func (this *MgoDb) GetDatabase() *mgo.Database {
	atomic.AddInt32(&this.cc, 1)
	return this.session.Clone().DB(this.database)
}

//当前服务器链接状态
func (this *MgoDb) State() {
	log.Admin.Sys("mgo countter ", this.database)
	log.Admin.Sys("cc = ", atomic.LoadInt32(&this.cc))
}

//关闭数据库
func (this *MgoDb) Close() {
	this.session.Close()
}

//@return ""
//返回一个ID
func (this *MgoDb) GetObjectId() bson.ObjectId {
	return bson.NewObjectId()
}

//@return ""
//根据str创建一个
func (this *MgoDb) CreateObjectId(str string) bson.ObjectId {
	return bson.ObjectIdHex(str)
}

//@return nil
//创建一个唯一索引
func (this *MgoDb) CreateIndexOnlyMgo(c *mgo.Collection, key string) error {
	index := mgo.Index{
		Key:        []string{key},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	return c.EnsureIndex(index)
}
