package db

import (
	"runtime/debug"
	"time"

	"github.com/globalsign/mgo"
)

//@return nil
//构造一个数据库读取器,可选择性添加二级缓存
func NewMgoReader(d *MgoDb, chanNum int, scache *RedisDb) *MgoReader {
	reader := &MgoReader{
		d:      d,
		num:    chanNum,
		scache: scache,
	}
	reader.work()
	return reader
}

//服务器数据加载服务
type MgoReader struct {
	d      *MgoDb
	num    int
	c      chan *MgoChunk
	scache *RedisDb
}

//数据库加载
func (this *MgoReader) Load(ck *MgoChunk) {
	if ck != nil {
		this.c <- ck
	}
}

//开始工作
func (this *MgoReader) work() {
	this.c = make(chan *MgoChunk)
	for i := 0; i < this.num; i++ {
		go this.lis()
	}
}

//监听
func (this *MgoReader) lis() {
	database := this.d.GetDatabase()
	defer func() {
		this.d.RelaseDatabase(database)
		r := recover()
		if r != nil {
			debug.PrintStack()
			time.Sleep(time.Second * 3)
			go this.lis()
		}
	}()
	var ck *MgoChunk
	var preC string = ""
	var cl *mgo.Collection
	var err error
	fromScacheFirst := this.scache != nil
	fromScache := false
	for {
		ck = <-this.c
		fromScache = false
		ck.Lock()
		if preC != ck.C {
			cl = database.C(ck.C)
			preC = ck.C
		}
		if ck.Uuid != "" {
			if fromScacheFirst { //优先redis
				err = this.scache.GetInterface(ck.C+"_"+ck.Uuid, ck.Target)
				if err != nil {
					err = cl.FindId(ck.Uuid).One(ck.Target)
				} else {
					fromScache = true
				}
			} else {
				err = cl.FindId(ck.Uuid).One(ck.Target)
			}
		} else if ck.M != nil {
			err = cl.Find(ck.M).One(ck.Target)
		}
		if err != nil {
			if err != mgo.ErrNotFound {
				ck.State = CState_Error
				ck.Target = nil
			} else {
				if ck.AutoCreate {
					ck.Target.New(ck.Uuid)
					if ck.Uuid == "" {
						ck.Uuid = ck.Target.Uuid()
					}
					ck.Target.Bind(ck)
					ck.Insert()
					ck.State = CState_Idle
				} else {
					ck.Target = nil
					ck.State = CState_Notfound
				}
			}
		} else {
			ck.Target.Bind(ck)
			if fromScache {
				ck.InsertFromScache()
			}
			ck.State = CState_Idle
		}
		ck.Unlock()
		ck.wg.Done()
	}
}
