package db

import (
	"runtime/debug"
	"sync"
	"time"

	"gitee.com/nodep/nframe/log"
	"gitee.com/nodep/nframe/tran"
	"github.com/globalsign/mgo"
)

//@return nil
//获取一个手动控制的Cache
func NewMgoCache(cname string, d *MgoDb, create func() MgoChunker, reader *MgoReader, closeChan chan bool, mod int, scache bool) *MgoCache {
	cache := &MgoCache{
		cname:     cname,
		d:         d,
		create:    create,
		reader:    reader,
		closeChan: closeChan,
		mod:       int8(mod),
		mod64:     int64(mod),
		mmap:      make(map[int8]*MgoCacheMod, mod),
		scache:    scache,
	}
	for i := 0; i < mod; i++ {
		mm := &MgoCacheMod{store: make(map[string]*MgoChunk, 256)}
		cache.mmap[int8(i)] = mm
	}
	go cache.loopWrite()
	return cache
}

type MgoCacheMod struct {
	sync.RWMutex
	store map[string]*MgoChunk
}

/*
手动控制缓存
*/
type MgoCache struct {
	cname       string                //表格名字
	d           *MgoDb                //数据库
	create      func() MgoChunker     //构造器
	reader      *MgoReader            //公共加载器
	closeChan   chan bool             //关闭器
	mod         int8                  //取模值
	mod64       int64                 //64位取模值
	closed      bool                  //是否已关闭
	closeSended bool                  //关闭消息是否已发送
	scache      bool                  //二级缓存
	mmap        map[int8]*MgoCacheMod //mmap
	dbTable     *mgo.Collection
}

//安全关闭
func (this *MgoCache) SafeClose() {
	this.closed = true
}

//测试输出
func (this *MgoCache) DebugLog() {
	log.Admin.Debug("mgoCache<")
	count := 0
	online := 0
	hasErr := false
	for _, v := range this.mmap {
		v.RLock()
		count += len(v.store)
		for _, mc := range v.store {
			if mc.C != this.cname {
				hasErr = true
			}
			if mc.Tag > 0 {
				online++
			}
		}
		v.RUnlock()
	}
	log.Admin.Debug("mgoCache->", this.cname, "truelen->", count, "hasErr", hasErr, "tagTotal", online)
}

/*
新增、更新、删除
只有标记为Destroy的才可能被回收
*/
func (this *MgoCache) loopWrite() {
	this.dbTable = this.d.GetCollection(this.cname)
	c := this.dbTable
	defer func() {
		this.d.RelaseCollection(this.dbTable)
		r := recover()
		if r != nil && !this.closeSended {
			debug.PrintStack()
			time.Sleep(time.Second * 3)
			go this.loopWrite()
		}
	}()
	var closeCheck bool = false
	var i int8 = 0
	var ck *MgoChunk
	var err error
	gced := false
	updated := 0
	for {
		if this.closed {
			closeCheck = true
		} else {
			time.Sleep(time.Second * 5)
		}
		for i = 0; i < this.mod; i++ {
			cm := this.mmap[i]
			cm.RLock()
			clen := len(cm.store)
			values := make([]*MgoChunk, clen, clen)
			idx := 0
			for _, v := range cm.store {
				values[idx] = v
				idx++
			}
			cm.RUnlock()
			updated = 5
			for idx = 0; idx < clen; idx++ {
				ck = values[idx]
				gced = false
				ck.Lock()
				if ck.Option != "" && ck.Target != nil { //检测更新
					switch ck.Option {
					case Option_Update:
						// log.Game.Debug("update", this.cname, ck.Uuid)
						ck.Target.RLock()
						err = c.UpdateId(ck.Uuid, ck.Target)
						ck.Target.RUnlock()
					case Option_Insert:
						// log.Game.Debug("insert", this.cname, ck.Uuid)
						ck.Target.RLock()
						_, err = c.UpsertId(ck.Uuid, ck.Target)
						ck.Target.RUnlock()
						// c.Insert(ck.Target)
					case Option_Delete:
						// log.Game.Debug("delete", this.cname, ck.Uuid)
						err = c.RemoveId(ck.Uuid)
					}
					ck.Option = ""
					if err == nil {
						ck.ClearScache()
					}
					updated--
				} //else if
				if ck.GcTag { //检测回收
					cm.Lock()
					if ck.GcTag {
						if ck.Target != nil {
							ck.Target.Destroy()
							ck.Target = nil
						}
						delete(cm.store, ck.Uuid)
						gced = true
					}
					cm.Unlock()
				}
				ck.Unlock()
				if !this.closed {
					if gced {
						ck.Clear()
						mgoChunkPool.Put(ck)
					}
					if updated <= 0 {
						updated = 5
						time.Sleep(time.Millisecond * 500)
					}
				}
			}
			if !this.closed {
				time.Sleep(time.Millisecond * 1000)
			}
		}
		if closeCheck {
			this.closeSended = true
			this.closeChan <- true
			break
		}
	}
}

func (this *MgoCache) getMod(uuid string) int8 {
	return int8(tran.StringToInt64(uuid) % this.mod64)
}

//清空
func (this *MgoCache) Clear(uuid string, write bool) {
	md := this.getMod(uuid)
	cm := this.mmap[md]
	cm.Lock()
	defer cm.Unlock()
	ck := cm.store[uuid]
	if ck == nil {
		return
	}
	ck.Lock()
	if write {
		if ck.Option != "" && ck.Target != nil { //检测更新
			var err error = nil
			switch ck.Option {
			case Option_Update:
				// log.Game.Debug("update", this.cname, ck.Uuid)
				ck.Target.RLock()
				err = this.dbTable.UpdateId(ck.Uuid, ck.Target)
				ck.Target.RUnlock()
			case Option_Insert:
				// log.Game.Debug("insert", this.cname, ck.Uuid)
				ck.Target.RLock()
				_, err = this.dbTable.UpsertId(ck.Uuid, ck.Target)
				ck.Target.RUnlock()
				// c.Insert(ck.Target)
			case Option_Delete:
				// log.Game.Debug("delete", this.cname, ck.Uuid)
				err = this.dbTable.RemoveId(ck.Uuid)
			}
			ck.Option = ""
			if err == nil {
				ck.ClearScache()
			}
		}
	}
	if ck.Target != nil {
		ck.Target.Destroy()
		// ck.Target = nil
	}
	delete(cm.store, ck.Uuid)
	ck.Clear()
	ck.Unlock()
	// mgoChunkPool.Put(ck)
}

/*
根据uuid查询或创建,若查无数据则创建
*/
//@return nil,nil
func (this *MgoCache) LoadOrStoreById(uuid string) (*MgoChunk, error) {
	md := this.getMod(uuid)
	cm := this.mmap[md]
	cm.RLock()
	ck := cm.store[uuid]
	if ck != nil {
		ck.Lock()
		ck.Tag++
		ck.GcTag = false
		ck.Unlock()
	}
	cm.RUnlock()
	if ck == nil {
		cm.Lock()
		if cm.store[uuid] == nil {
			ck = mgoChunkPool.Get().(*MgoChunk)
			ck.UseScache = this.scache
			cm.store[uuid] = ck
			ck.C = this.cname
			ck.Uuid = uuid
			ck.Target = this.create()
			ck.AutoCreate = true
			ck.Tag++
			ck.wg.Add(1)
			cm.Unlock()
			this.reader.Load(ck)
		} else {
			ck = cm.store[uuid]
			if ck.State == CState_Error || ck.State == CState_Notfound {
				ck.State = CState_Find
				ck.Target = this.create()
				ck.AutoCreate = true
				ck.wg.Add(1)
				cm.Unlock()
				this.reader.Load(ck)
				ck.wg.Wait()
			} else {
				cm.Unlock()
			}
		}
	} else {
		if ck.State == CState_Error || ck.State == CState_Notfound {
			ck.State = CState_Find
			ck.Target = this.create()
			ck.AutoCreate = true
			ck.wg.Add(1)
			this.reader.Load(ck)
			ck.wg.Wait()
		}
	}
	if ck.State == CState_Error {
		ck.Destroy()
		return nil, Err_DbError
	}
	if ck.State == CState_Find {
		ck.wg.Wait()
	}
	return ck, nil
}

/*
根据uuid查询,若查无数据,chunk.target = nil
*/
//@return nil,nil
func (this *MgoCache) LoadById(uuid string) (*MgoChunk, error) {
	md := this.getMod(uuid)
	cm := this.mmap[md]
	cm.RLock()
	ck := cm.store[uuid]
	if ck != nil {
		ck.Lock()
		ck.Tag++
		ck.GcTag = false
		ck.Unlock()
	}
	cm.RUnlock()
	if ck == nil {
		cm.Lock()
		if cm.store[uuid] == nil {
			ck = mgoChunkPool.Get().(*MgoChunk)
			ck.UseScache = this.scache
			cm.store[uuid] = ck
			ck.C = this.cname
			ck.Uuid = uuid
			ck.Target = this.create()
			ck.AutoCreate = false
			ck.Tag++
			ck.wg.Add(1)
			this.reader.Load(ck)
		} else {
			ck = cm.store[uuid]
			if ck.State == CState_Error {
				ck.State = CState_Find
				ck.Target = this.create()
				ck.wg.Add(1)
				this.reader.Load(ck)
				ck.wg.Wait()
			}
		}
		cm.Unlock()
	} else {
		if ck.State == CState_Error {
			ck.State = CState_Find
			ck.Target = this.create()
			ck.wg.Add(1)
			this.reader.Load(ck)
			ck.wg.Wait()
		}
	}
	if ck.State == CState_Error {
		ck.Destroy()
		return nil, Err_DbError
	}
	if ck.State == CState_Find {
		ck.wg.Wait()
	}
	return ck, nil
}
