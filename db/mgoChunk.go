package db

import (
	"sync"
	"time"

	"gitee.com/nodep/nframe/log"
	"github.com/globalsign/mgo/bson"
)

var ScacheForChunk *RedisDb = nil //二级缓存
var ScacheExtTime time.Duration   //二级缓存有效时间

type DbOption string

const (
	Option_Insert DbOption = "insert"
	Option_Update DbOption = "update"
	Option_Delete DbOption = "delete"
)

const (
	CState_Find     int32 = 0 //查询
	CState_Idle     int32 = 1 //空闲
	CState_Change   int32 = 2 //修改
	CState_Notfound int32 = 8 //未查询到
	CState_Error    int32 = 9 //错误的
)

type MgoChunker interface {
	Lock()
	Unlock()
	RLock()
	RUnlock()
	Uuid() string
	New(uuid string)
	Bind(c *MgoChunk)
	Destroy()
}

var mgoChunkPool = sync.Pool{
	New: func() interface{} {
		return &MgoChunk{}
	},
}

type MgoChunk struct {
	C          string         //表
	Uuid       string         //Key
	M          bson.M         //其他查询条件(当没有uuid时有效)
	Target     MgoChunker     //存储目标
	State      int32          //当前状态
	Option     DbOption       //操作类型
	wg         sync.WaitGroup //同步加载锁
	AutoCreate bool           //自动创建
	GcTag      bool           //回收标记
	Tag        int32          //计数器
	UseScache  bool           //是否使用二级缓存
	sync.Mutex                //锁
}

func (this *MgoChunk) Clear() {
	this.C = ""
	this.Target = nil
	this.Uuid = ""
	this.M = nil
	this.State = CState_Find
	this.Option = ""
	this.GcTag = false
	this.Tag = 0
	this.AutoCreate = false
	this.UseScache = false
}

//删除二级缓存
func (this *MgoChunk) ClearScache() {
	if this.UseScache {
		ScacheForChunk.DelKey(this.C + "_" + this.Uuid)
	}
}

//从二级缓存新增
func (this *MgoChunk) InsertFromScache() {
	this.Option = Option_Insert
	this.State = CState_Change
}

//新增
func (this *MgoChunk) Insert() {
	this.Option = Option_Insert
	this.State = CState_Change
	if this.UseScache {
		ScacheForChunk.SetInterface(this.C+"_"+this.Uuid, this.Target, ScacheExtTime)
	}
}

//更新
func (this *MgoChunk) Update() {
	this.Lock()
	switch this.Option {
	case Option_Insert:
		break
	default:
		this.Option = Option_Update
	}
	this.State = CState_Change
	if this.UseScache {
		ScacheForChunk.SetInterface(this.C+"_"+this.Uuid, this.Target, ScacheExtTime)
	}
	this.Unlock()

}

//删除并销毁缓存
func (this *MgoChunk) Delete() {
	this.Lock()
	this.Option = Option_Delete
	if this.UseScache {
		ScacheForChunk.DelKey(this.C + "_" + this.Uuid)
	}
	this.Unlock()
}

//销毁缓存
func (this *MgoChunk) Destroy() {
	this.Lock()
	this.Tag--
	// log.Admin.Debug("Destroy MgoChunk", this.C, this.Tag)
	if this.Tag < 0 {
		log.Admin.Error("destroyed MgoChunk name =" + this.C)
		panic("destroyed MgoChunk name =" + this.C)
	} else if this.Tag == 0 {
		this.GcTag = true
	}
	this.Unlock()
}
