package db

import (
	"time"

	"gitee.com/nodep/nframe/log"
	"gitee.com/nodep/nframe/util"
	"github.com/go-redis/redis"
)

//redis配置
type RedisConfig struct {
	Name string
	Addr string
	Pwd  string
	Db   int
}

//redis数据库
type RedisDb struct {
	Name string
	Opts *redis.Options
	cli  *redis.Client
}

//删除
func (this *RedisDb) DelKey(key string) error {
	return this.cli.Del(key).Err()
}

//读对象
func (this *RedisDb) GetInterface(key string, v interface{}) error {
	str, err := this.cli.Get(key).Result()
	if err == nil && str != "" {
		err = util.JSONStruct(str, v)
	}
	return err
}

//写对象
func (this *RedisDb) SetInterface(key string, v interface{}, ext time.Duration) error {
	return this.cli.Set(key, util.JSONString(v), ext).Err()
}

//设置字符串
func (this *RedisDb) Set(key string, value string, ext time.Duration) error {
	return this.cli.Set(key, value, ext).Err()
}

//读字符串
func (this *RedisDb) Get(key string) (string, error) {
	return this.cli.Get(key).Result()
}

//TODO 需要测试因为网络原因导致的redis掉线是否会自动重新连接
func (this *RedisDb) connect() {
	cli := redis.NewClient(this.Opts)
	rst, err := cli.Ping().Result()
	if err == nil {
		this.cli = cli
		log.Admin.Sys("redis,", this.Name, this.Opts.Addr, rst)
	} else {
		panic(err)
	}
}

func CreateRedis(cfg *RedisConfig) *RedisDb {
	opts := &redis.Options{
		Addr:     cfg.Addr,
		Password: cfg.Pwd,     // no password set
		DB:       int(cfg.Db), // use default DB
	}
	rdb := &RedisDb{
		Name: cfg.Name,
		Opts: opts,
	}
	rdb.connect()
	return rdb
}
