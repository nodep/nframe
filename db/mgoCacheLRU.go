package db

import (
	"runtime/debug"
	"sync"
	"time"

	"gitee.com/nodep/nframe/log"
	"github.com/globalsign/mgo"
)

//@return nil
func NewMgoCacheLRU(cname string, d *MgoDb, create func() MgoChunker, reader *MgoReader, closeChan chan bool, max int, scache bool) *MgoCacheLRU {
	cache := &MgoCacheLRU{
		cname:     cname,
		d:         d,
		create:    create,
		reader:    reader,
		closeChan: closeChan,
		max:       max,
		del:       max,
		cmap:      make(map[string]*MclNode),
		scache:    scache,
	}
	cache.startNode = &MclNode{}
	cache.endNode = &MclNode{}
	cache.startNode.next = cache.endNode
	cache.endNode.pre = cache.startNode
	go cache.loopWrite()
	return cache
}

//LRU节点
type MclNode struct {
	uuid  string
	pre   *MclNode
	next  *MclNode
	value *MgoChunk
}

/*
LRU缓存组
*/
type MgoCacheLRU struct {
	cname       string
	d           *MgoDb
	closed      bool
	closeSended bool //关闭消息是否已发送
	create      func() MgoChunker
	reader      *MgoReader
	closeChan   chan bool //关闭器
	max         int       //计数器
	del         int       //需要删除的数量
	count       int       //当前数量
	startNode   *MclNode  //头部节点
	endNode     *MclNode  //尾部节点
	nearNode    *MclNode  //用于错误检查的node
	errCount    int       //计数错误次数
	scache      bool      //二级缓存
	cmap        map[string]*MclNode
	dbTable     *mgo.Collection
	sync.Mutex
	LastLockBy int //最终锁定位置
}

//移除一个节点
func (this *MgoCacheLRU) removeNode(node *MclNode) {
	this.count--
	pre := node.pre
	next := node.next
	pre.next = next
	next.pre = pre
	node.pre = nil
	node.next = nil
}

//插入头部
func (this *MgoCacheLRU) insertNode(node *MclNode) {
	this.count++
	old := this.startNode.next
	old.pre = node
	node.next = old
	node.pre = this.startNode
	this.startNode.next = node
}

//将node插入到toNode的后面
func (this *MgoCacheLRU) swapNode(node *MclNode, toNode *MclNode) {
	if node == toNode {
		return
	}
	oldPre := node.pre
	oldNext := node.next
	oldPre.next = oldNext
	oldNext.pre = oldPre
	old := this.startNode.next
	old.pre = node
	node.next = old
	node.pre = this.startNode
	this.startNode.next = node
}

// func (this *MgoCacheLRU) gc() {
// 	var node *MclNode
// 	var ck *MgoChunk
// 	log.Admin.Debug("尝试回收", this.count, this.del)
// 	if this.count > this.del {
// 		needDel := this.count - this.max
// 		log.Admin.Debug("需要回收的数量", needDel)
// 		this.Lock()
// 		node = this.endNode.pre
// 		if this.nearNode == node {
// 			this.errCount++
// 		} else {
// 			this.errCount = 0
// 		}
// 		this.nearNode = node
// 		if this.errCount > 5 {
// 			// log.Admin.Error("mgoCacheLRU , gcError Catched =" + this.cname)
// 			this.errCount = 0
// 		}
// 		jumpMax := needDel / 2
// 		jumpCount := 0
// 		for idx := 0; idx < needDel; idx++ {
// 			if node.value == nil {
// 				continue
// 			}
// 			ck = node.value
// 			ck.Lock()
// 			if ck.Option == "" && ck.GcTag {
// 				if ck.Target != nil {
// 					ck.Target.Destroy()
// 				}
// 				delete(this.cmap, ck.Uuid)
// 				this.removeNode(node)
// 				ck.Unlock()
// 				ck.Clear()
// 				mgoChunkPool.Put(ck)
// 			} else {
// 				ck.Unlock()
// 				jumpCount++
// 				if jumpCount >= jumpMax {
// 					break
// 				}
// 			}
// 			node = node.pre
// 		}
// 		this.Unlock()
// 	}
// }

// //循环写入与安全关闭
// func (this *MgoCacheLRU) loopWrite() {
// 	c := this.d.GetCollection(this.cname)
// 	defer func() {
// 		this.d.RelaseCollection(c)
// 		r := recover()
// 		if r != nil && !this.closeSended {
// 			time.Sleep(time.Second * 3)
// 			go this.loopWrite()
// 		}
// 	}()
// 	//循环写入,如果超过存储上线,反向删除多余的(同时要记录最后一个节点,如果连续多次不变且状态GcTag=false。则说明有业务bug)
// 	var closeCheck bool = false
// 	var ck *MgoChunk
// 	var node *MclNode
// 	var values []*MclNode = make([]*MclNode, 0, this.max*2)
// 	idx := 0
// 	for {
// 		if this.closed {
// 			closeCheck = true
// 		} else {
// 			time.Sleep(time.Second * 5)
// 		}
// 		values = values[:0]
// 		this.Lock()
// 		for _, v := range this.cmap {
// 			values = append(values, v)
// 		}
// 		this.Unlock()
// 		clen := len(values)
// 		waitTime := time.Duration(int64(clen) * 10)
// 		if waitTime > 100 {
// 			waitTime = 100
// 		}
// 		checkCount := 0
// 		for idx = 0; idx < clen; idx++ {
// 			node = values[idx]
// 			ck = node.value
// 			ck.Lock()
// 			if ck.Option != "" && ck.Target != nil { //检测更新
// 				switch ck.Option {
// 				case Option_Update:
// 					log.Game.Debug("update", this.cname, ck.Uuid)
// 					ck.Target.RLock()
// 					c.UpdateId(ck.Uuid, ck.Target)
// 					ck.Target.RUnlock()
// 				case Option_Insert:
// 					log.Game.Debug("insert", this.cname, ck.Uuid)
// 					ck.Target.RLock()
// 					c.UpsertId(ck.Uuid, ck.Target)
// 					ck.Target.RUnlock()
// 					// c.Insert(ck.Target)
// 				case Option_Delete:
// 					log.Game.Debug("delete", this.cname, ck.Uuid)
// 					c.RemoveId(ck.Uuid)
// 				}
// 				ck.Option = ""
// 			}
// 			ck.Unlock()
// 			if !this.closed {
// 				checkCount++
// 				if checkCount > 20 {
// 					checkCount = 0
// 					this.gc()
// 				}
// 				time.Sleep(time.Millisecond * waitTime)
// 			}
// 		}
// 		if closeCheck {
// 			this.closeSended = true
// 			this.closeChan <- true
// 			break
// 		} else {
// 			this.gc()
// 		}
// 	}
// }

func (this *MgoCacheLRU) gc() {
	if this.count <= this.del {
		return
	}
	node := this.endNode.pre
	if this.nearNode == node {
		this.errCount++
	} else {
		this.errCount = 0
	}
	debugFlag := false
	this.nearNode = node
	if this.errCount > 10 {
		if node.value != nil {
			debugFlag = true
			log.Admin.Error(this.cname, "->", node.value.Option, node.value.GcTag, node.value.Tag)
		}
		this.errCount = 0
	}
	if node.value == nil {
		return
	}
	ck := node.value
	if debugFlag {
		log.Admin.Debug("debugFlag->", ck.C, ck.Option, ck.GcTag, ck.Tag)
	}
	ck.Lock()
	if ck.Option == "" && ck.GcTag {
		if ck.Target != nil {
			ck.Target.Destroy()
			ck.Target = nil
		}
		delete(this.cmap, ck.Uuid)
		this.removeNode(node)
		ck.Unlock()
		ck.Clear()
		mgoChunkPool.Put(ck)
	} else {
		ck.Unlock()
	}
}

func (this *MgoCacheLRU) writeValue(ck *MgoChunk, c *mgo.Collection, lk bool) {
	if lk {
		ck.Lock()
		defer ck.Unlock()
	}
	if ck.Option != "" && ck.Target != nil { //检测更新
		var err error = nil
		switch ck.Option {
		case Option_Update:
			// log.Game.Debug("update", this.cname, ck.Uuid)
			ck.Target.RLock()
			err = c.UpdateId(ck.Uuid, ck.Target)
			ck.Target.RUnlock()
		case Option_Insert:
			// log.Game.Debug("insert", this.cname, ck.Uuid)
			ck.Target.RLock()
			_, err = c.UpsertId(ck.Uuid, ck.Target)
			ck.Target.RUnlock()
			// c.Insert(ck.Target)
		case Option_Delete:
			// log.Game.Debug("delete", this.cname, ck.Uuid)
			err = c.RemoveId(ck.Uuid)
		}
		ck.Option = ""
		if err == nil { //操作成功,清除缓存
			ck.ClearScache()
		}
	}
}

//从尾部开始写入
func (this *MgoCacheLRU) loopWrite() {
	this.dbTable = this.d.GetCollection(this.cname)
	defer func() {
		log.Admin.Debug("LRU loopWrite end,restart")
		this.d.RelaseCollection(this.dbTable)
		r := recover()
		if r != nil && !this.closeSended {
			debug.PrintStack()
			time.Sleep(time.Second * 3)
			go this.loopWrite()
		}
	}()
	c := this.dbTable
	var node *MclNode = this.endNode
	for {
		if this.closed {
			this.Lock()
			node = this.startNode
			for node != this.endNode {
				node = node.next
				if node.value != nil {
					this.writeValue(node.value, c, true)
				}
			}
			this.closeSended = true
			this.closeChan <- true
			this.Unlock()
			return
		}
		this.Lock()
		this.LastLockBy = 1
		cc := 1
		if this.count > this.del {
			cc += (this.count - this.del) / 10
		}
		trueCount := 0
		if this.count > this.del { //如果超过了容器允许的最大值
			node = this.endNode.pre
		} else {
			node = node.pre
			if node == this.startNode || node == nil {
				node = this.endNode.pre
			}
		}
		for cc > 0 {
			cc--
			if node.value == nil {
				break
			}
			this.writeValue(node.value, c, true)
			trueCount++
			node = node.pre
		}
		for trueCount > 0 {
			this.gc()
			trueCount--
		}
		this.LastLockBy = 0
		this.Unlock()
		time.Sleep(time.Millisecond * 500)
	}
	log.Admin.Debug("LRU loopWrite out")
}

//安全关闭
func (this *MgoCacheLRU) SafeClose() {
	this.closed = true
}

//测试输出
func (this *MgoCacheLRU) DebugLog() {
	log.Admin.Debug("mgoCacheLRU<", this.cname, this.LastLockBy)
	this.Lock()
	this.LastLockBy = 3
	log.Admin.Debug("mgoCacheLRU->", this.cname, "count->", this.count, "truelen->", len(this.cmap))
	this.LastLockBy = 0
	this.Unlock()
}

//清空
func (this *MgoCacheLRU) Clear(uuid string, write bool) {
	defer func() {
		if r := recover(); r != nil {
			debug.PrintStack()
		}
	}()
	this.Lock()
	this.LastLockBy = 2
	defer this.Unlock()
	defer func() {
		this.LastLockBy = 0
	}()
	node := this.cmap[uuid]
	if node == nil {
		return
	}
	var ck *MgoChunk = node.value
	if ck != nil {
		ck.Lock()
		this.LastLockBy = 21
		if write {
			this.writeValue(ck, this.dbTable, false)
		}
		if ck.Target != nil {
			ck.Target.Destroy()
			// ck.Target = nil
		}
		delete(this.cmap, ck.Uuid)
		ck.Clear()
		ck.Unlock()
		// mgoChunkPool.Put(ck)
	}
	this.removeNode(node)
}

//@return nil,nil
//通过uuid获取数据,不会自动创建
func (this *MgoCacheLRU) LoadById(uuid string) (*MgoChunk, error) {
	var ck *MgoChunk = nil
	this.Lock()
	node := this.cmap[uuid]
	if node != nil {
		ck = node.value
		ck.Lock()
		ck.Tag++
		ck.GcTag = false
		ck.Unlock()
		this.swapNode(node, this.startNode)
		this.Unlock()
		if ck.State == CState_Error {
			ck.State = CState_Find
			ck.wg.Add(1)
			this.reader.Load(ck)
			ck.wg.Wait()
		}
	} else {
		node = &MclNode{}
		ck = mgoChunkPool.Get().(*MgoChunk)
		node.value = ck
		node.uuid = uuid
		this.cmap[uuid] = node
		this.insertNode(node)
		ck.C = this.cname
		ck.Uuid = uuid
		ck.Target = this.create()
		ck.Tag++
		this.Unlock()
		ck.AutoCreate = false
		ck.wg.Add(1)
		this.reader.Load(ck)
	}
	if ck.State == CState_Error {
		ck.Destroy()
		return nil, Err_DbError
	}
	if ck.State == CState_Find {
		ck.wg.Wait()
	}
	return ck, nil
}

//@return nil,nil
//通过uuid获取数据或创建数据
func (this *MgoCacheLRU) LoadOrStoreById(uuid string) (*MgoChunk, error) {
	var ck *MgoChunk = nil
	this.Lock()
	this.LastLockBy = 4
	node := this.cmap[uuid]
	if node != nil {
		ck = node.value
		ck.Lock()
		ck.Tag++
		ck.GcTag = false
		ck.Unlock()
		this.swapNode(node, this.startNode)
		this.LastLockBy = 0
		this.Unlock()
		if ck.State == CState_Error || ck.State == CState_Notfound {
			ck.AutoCreate = true
			ck.State = CState_Find
			ck.wg.Add(1)
			this.reader.Load(ck)
			ck.wg.Wait()
		}
	} else {
		node = &MclNode{}
		ck = mgoChunkPool.Get().(*MgoChunk)
		node.value = ck
		node.uuid = uuid
		this.cmap[uuid] = node
		this.insertNode(node)
		ck.C = this.cname
		ck.Uuid = uuid
		ck.Target = this.create()
		ck.Tag++
		this.LastLockBy = 0
		this.Unlock()
		ck.AutoCreate = true
		ck.wg.Add(1)
		this.reader.Load(ck)
	}
	if ck.State == CState_Error {
		ck.Destroy()
		return nil, Err_DbError
	}
	if ck.State == CState_Find {
		ck.wg.Wait()
	}
	return ck, nil
}
