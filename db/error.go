package db

import (
	"errors"
)

var (
	Err_DbPool  = errors.New("db pool set error")
	Err_DbError = errors.New("db error")

	Err_RedisNil = errors.New("redis: nil")
)
