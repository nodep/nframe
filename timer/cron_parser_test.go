package timer

import (
	"fmt"
	"testing"
	"time"
)

func TestCronParse(t *testing.T) {
	args := []string{"@daily", "daily", "@hourly", "hourly", "@weekly", "weekly", "@monthly", "monthly", "@yearly", "yearly", "* * * * * *", "秒", "0 0 0 * * *", "每天0点", "0/30 * * * * *", "每 30 秒", "0,30 * * * * *", "每 30 秒", "0 43 21 * * *", "21:43 执行每天", "0 15 05 * * *", "05:15 执行", "0 0 17 * * *", "17:00 执行", "0 0 17 * * 0", "每周天的 17:00 执行", "0 0,10 17 * * 0,2,3", "每周日,周二,周三的 17:00和 17:10 执行", "0 0-10 17 1 * *", "毎月1日从 17:00 到 7:10 毎隔 1 分钟 执行", "0 0 0 1,15 * 1", "毎月1日和 15 日和 一日的 0:00 执行", "0 42 4 1 * *", "毎月1日的 4:42 分 执行", "0 0 21 * * 1-6", "周一到周六 21:00 执行", "0 0,10,20,30,40,50 * * * *", "每隔 10 分 执行", "0 */10 * * * *", "每隔 10 分 执行", "0 * 1 * * *", "从 1:0 到 1:59 每隔 1 分钟 执行", "0 0 1 * * *", "1:00 执行", "0 0 */1 * * *", "毎时 0 分 每隔 1 小时 执行", "0 0 * * * *", "毎时 0 分 每隔 1 小时 执行", "0 2 8-20/3 * * *", "8:02,11:02,14:02,17:02,20:02 执行", "0 30 5 1,15 * *", "1 日 和 15 日的 5:30 执行", "0 30 5 1,15 1 *", "", "0 30 5 1,15 1 2", ""}
	ttt := time.Now()
	fmt.Println("当前时间！！！！！！！！！！！！", ttt.String())
	for i := 0; i < len(args); i += 2 {
		key := args[i]
		value := args[i+1]
		fmt.Println(key, value)
		cron, _ := Parse(key)
		newT := cron.Next(ttt)
		fmt.Println(newT.String())
		newT = cron.Next(newT)
		fmt.Println(newT.String())
	}

	fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

	cron1, _ := ParseStandard("@daily")
	newT := cron1.Next(ttt)
	fmt.Println(newT.String())
	newT = cron1.Next(newT)
	fmt.Println(newT.String())

	delay := Every(time.Second * 10)
	newT = delay.Next(ttt)
	fmt.Println(newT.String())
	newT = delay.Next(newT)
	fmt.Println(newT.String())
	newT = delay.Next(newT)
	fmt.Println(newT.String())

	delay = Every(time.Millisecond * 500)
	newT = delay.Next(ttt)
	fmt.Println(newT.String())
	newT = delay.Next(newT)
	fmt.Println(newT.String())
	newT = delay.Next(newT)
	fmt.Println(newT.String())
}
