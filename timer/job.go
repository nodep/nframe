package timer

import (
	"sync"
	"sync/atomic"
	"time"
)

//https://github.com/rfyiamcool/cronlib
type JobTimer struct {
	Id     uint64        //自己的id
	Times  int           //次数 <=0无限循环
	target TimeJob       //任务
	st     int64         //执行时的服务器时间
	pre    *JobTimer     //上一个
	next   *JobTimer     //下一个
	group  *jobTimerLink //所属定时器
	remain int           //剩余次数
	runner TimeRunner    //时序
}

/*
时刻任务队列
辅助一系列时刻任务
每一个link有自己所属的自增id
*/
type jobTimerLink struct {
	idInc      uint64
	stopChan   chan bool //关闭用的定时器
	sync.Mutex           //同步锁
	Head       *JobTimer //头
	Tail       *JobTimer //尾
}

//@return nil
func new_jobTimerLink() *jobTimerLink {
	link := &jobTimerLink{}
	link.stopChan = make(chan bool)
	link.run()
	return link
}

//添加一个任务
//@return nil
func (this *jobTimerLink) Add(timeParams string, job TimeJob, ts int) *JobTimer {
	cron, err := Parse(timeParams)
	if err != nil {
		return nil
	}
	tick := &JobTimer{
		Id:     atomic.AddUint64(&this.idInc, 1),
		Times:  ts,
		target: job,
		group:  this,
		runner: cron,
	}
	if tick.Times <= 0 {
		tick.remain = -1
	} else {
		tick.remain = tick.Times
	}
	tick.st = tick.runner.Next(time.Now()).UnixNano()
	this.Lock()
	if this.insert(tick) {
		this.Unlock()
		this.restart()
		return tick
	}
	this.Unlock()
	return tick
}

//添加一个定时器
//@return false
func (this *jobTimerLink) insert(t *JobTimer) bool {
	t.pre, t.next = nil, nil
	if this.Tail == nil { //空队列
		this.Tail = t
		this.Head = t
		return true
	}
	now := this.Tail
	for now != nil && now.st > t.st {
		now = now.pre
	}
	if now == nil { //插入到表头
		this.Head.pre = t
		t.next = this.Head
		this.Head = t
		return true
	} else {
		if now == this.Tail { //插入到尾
			t.pre = now
			now.next = t
			this.Tail = t
		} else { //插入到中间
			next := now.next
			t.pre = now
			t.next = next
			next.pre = t
			now.next = t
		}
	}
	return false
}

//删除一个定时器
func (this *jobTimerLink) remove(t *JobTimer) {
	//TODO
}

//移除一个头
//@return nil
func (this *jobTimerLink) shift() *JobTimer {
	if this.Head == nil {
		return nil
	}
	h := this.Head
	this.Head = this.Head.next
	if this.Head != nil {
		this.Head.pre = nil
	} else {
		this.Tail = nil
	}
	return h
}

//停止当前得
func (this *jobTimerLink) restart() {
	this.stopChan <- false
}

//重启定时器
func (this *jobTimerLink) run() {
	go func() {
		for {
			now := time.Now()
			nowTime := now.UnixNano()
			watiTime := nowTime
			this.Lock()
			if this.Head != nil {
				watiTime = this.Head.st - nowTime
				for watiTime <= 0 { //如果已经超时
					tc := this.shift()
					if tc.remain < 0 {
						tc.st = tc.runner.Next(now).UnixNano()
						this.insert(tc)
					} else {
						tc.remain--
						if tc.remain > 0 {
							tc.st = tc.runner.Next(now).UnixNano()
							this.insert(tc)
						}
					}
					go tc.target.RunTimeJob()
					if this.Head != nil {
						watiTime = this.Head.st - nowTime
					} else {
						break
					}
				}
			}
			if this.Head == nil {
				watiTime = nowTime
			}
			this.Unlock()
			ticker := time.NewTicker(time.Duration(watiTime))
			select {
			case <-ticker.C:
				ticker.Stop()
				ticker = nil
			case flag := <-this.stopChan:
				ticker.Stop()
				ticker = nil
				if flag {
					return
				}
			}
		}
	}()
}

//前6个字段分别表示：
//       秒钟：0-59
//       分钟：0-59
//       小时：1-23
//       日期：1-31
//       月份：1-12
//       星期：0-6（0 表示周日）

//还可以用一些特殊符号：
//       *： 表示任何时刻
//       ,： 表示分割，如第三段里：2,4，表示 2 点和 4 点执行
//       －：表示一个段，如第三端里： 1-5，就表示 1 到 5 点
//       /n : 表示每个n的单位执行一次，如第三段里，*/1, 就表示每隔 1 个小时执行一次命令。也可以写成1-23/1.
/////////////////////////////////////////////////////////
//  0/30 * * * * *                        每 30 秒
//  0,30 * * * * *                        每 30 秒
//  0 43 21 * * *                         21:43 执行每天
//  0 15 05 * * *                         05:15 执行
//  0 0 17 * * *                          17:00 执行
//  0 0 17 * * 1                          每周一的 17:00 执行
//  0 0,10 17 * * 0,2,3                   每周日,周二,周三的 17:00和 17:10 执行
//  0 0-10 17 1 * *                       毎月1日从 17:00 到 7:10 毎隔 1 分钟 执行
//  0 0 0 1,15 * 1                        毎月1日和 15 日和 一日的 0:00 执行
//  0 42 4 1 * *                          毎月1日的 4:42 分 执行
//  0 0 21 * * 1-6                        周一到周六 21:00 执行
//  0 0,10,20,30,40,50 * * * *            每隔 10 分 执行
//  0 */10 * * * *                        每隔 10 分 执行
//  0 * 1 * * *                           从 1:0 到 1:59 每隔 1 分钟 执行
//  0 0 1 * * *                           1:00 执行
//  0 0 */1 * * *                         毎时 0 分 每隔 1 小时 执行
//  0 0 * * * *                           毎时 0 分 每隔 1 小时 执行
//  0 2 8-20/3 * * *                      8:02,11:02,14:02,17:02,20:02 执行
//  0 30 5 1,15 * *                       1 日 和 15 日的 5:30 执行
