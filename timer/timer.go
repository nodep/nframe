package timer

import (
	"runtime"
	"sync/atomic"
	"time"
)

//定时任务接口,适合用于游戏中的时刻任务与tick任务
type TimeJob interface {
	RunTimeJob() //执行任务
}

//分组类型
type type_job string

const (
	JOB_SEC  type_job = "JOB_SEC"  //待执行时间<=1000ms
	JOB_MIN  type_job = "JOB_MIN"  //待执行时间<=60000ms
	JOB_HOUR type_job = "JOB_HOUR" //待执行时间<=3600000ms
	JOB_DAY  type_job = "JOB_DAY"  //待执行时间<=86400000ms
	JOB_MORE type_job = "JOB_MORE" //带执行时间<=其他任何时间
)

var (
	jobTimerDic       map[type_job]*jobTimerLink  = make(map[type_job]*jobTimerLink, 5)
	tickTimerDic      map[type_job]*tickTimerLink = make(map[type_job]*tickTimerLink, 5)
	tickTimerSecGroup []*tickTimerLink
	cupCount          int
	stroke            int32
)

//初始化定时器
func init() {
	cupCount = runtime.NumCPU()
	jobTimerDic[JOB_SEC] = new_jobTimerLink()
	jobTimerDic[JOB_MIN] = new_jobTimerLink()
	jobTimerDic[JOB_HOUR] = new_jobTimerLink()
	jobTimerDic[JOB_DAY] = new_jobTimerLink()
	jobTimerDic[JOB_MORE] = new_jobTimerLink()
	tickTimerDic[JOB_MIN] = New_tickTimerLink()
	tickTimerDic[JOB_HOUR] = New_tickTimerLink()
	tickTimerDic[JOB_DAY] = New_tickTimerLink()
	tickTimerDic[JOB_MORE] = New_tickTimerLink()
	tickTimerSecGroup = make([]*tickTimerLink, cupCount, cupCount)
	for i := 0; i < cupCount; i++ {
		tickTimerSecGroup[i] = New_tickTimerLink()
	}
}

//前6个字段分别表示：
//       秒钟：0-59
//       分钟：0-59
//       小时：1-23
//       日期：1-31
//       月份：1-12
//       星期：0-6（0 表示周日）

//还可以用一些特殊符号：
//       *： 表示任何时刻
//       ,： 表示分割，如第三段里：2,4，表示 2 点和 4 点执行
//       －：表示一个段，如第三端里： 1-5，就表示 1 到 5 点
//       /n : 表示每个n的单位执行一次，如第三段里，*/1, 就表示每隔 1 个小时执行一次命令。也可以写成1-23/1.
/////////////////////////////////////////////////////////
//  0/30 * * * * *                        每 30 秒
//  0,30 * * * * *                        每 30 秒
//  0 43 21 * * *                         21:43 执行每天
//  0 15 05 * * *                         05:15 执行
//  0 0 17 * * *                          17:00 执行
//  0 0 17 * * 1                          每周一的 17:00 执行
//  0 0,10 17 * * 0,2,3                   每周日,周二,周三的 17:00和 17:10 执行
//  0 0-10 17 1 * *                       毎月1日从 17:00 到 7:10 毎隔 1 分钟 执行
//  0 0 0 1,15 * 1                        毎月1日和 15 日和 一日的 0:00 执行
//  0 42 4 1 * *                          毎月1日的 4:42 分 执行
//  0 0 21 * * 1-6                        周一到周六 21:00 执行
//  0 0,10,20,30,40,50 * * * *            每隔 10 分 执行
//  0 */10 * * * *                        每隔 10 分 执行
//  0 * 1 * * *                           从 1:0 到 1:59 每隔 1 分钟 执行
//  0 0 1 * * *                           1:00 执行
//  0 0 */1 * * *                         毎时 0 分 每隔 1 小时 执行
//  0 0 * * * *                           毎时 0 分 每隔 1 小时 执行
//  0 2 8-20/3 * * *                      8:02,11:02,14:02,17:02,20:02 执行
//  0 30 5 1,15 * *                       1 日 和 15 日的 5:30 执行

/*
创建一个时刻任务触发器
秒	 分时日月星期
0/30 * * * * *
@param jt * * * * * *格式
@param job 实现TimeJob接口RunTimeJob() //执行任务
@param JOB_SEC...
@return 任务触发器
*/
//@return nil
func CreateJob(jt string, job TimeJob, group type_job) *JobTimer {
	ter := jobTimerDic[group]
	if ter != nil {
		return ter.Add(jt, job, 0)
	}
	return nil
}

//单次任务
//@return nil
func CreateJobOnce(jt string, job TimeJob, group type_job) *JobTimer {
	ter := jobTimerDic[group]
	if ter != nil {
		return ter.Add(jt, job, 1)
	}
	return nil
}

/*
创建一个延迟触发器
@param t 间隔时间
@param times 次数
@param job 实现TimeJob接口
@return 延迟触发器
*/
//@return nil
func CreateTimer(t time.Duration, times int, job TimeJob) *TickTimer {
	var tm *TickTimer = nil
	if t <= time.Second {
		sid := int(atomic.AddInt32(&stroke, 1))
		if sid >= cupCount {
			atomic.StoreInt32(&stroke, 0)
			sid = 0
		}
		tm = tickTimerSecGroup[sid].Add(t, times, job)
	} else if t <= time.Minute {
		tm = tickTimerDic[JOB_MIN].Add(t, times, job)
	} else if t <= time.Hour {
		tm = tickTimerDic[JOB_HOUR].Add(t, times, job)
	} else if t <= time.Hour*24 {
		tm = tickTimerDic[JOB_DAY].Add(t, times, job)
	} else {
		tm = tickTimerDic[JOB_MORE].Add(t, times, job)
	}
	return tm
}
