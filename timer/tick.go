package timer

import (
	"sync"
	"sync/atomic"
	"time"
)

type TickTimer struct {
	Id     uint64         //自己的id
	Durt   time.Duration  //间隔时间
	Times  int            //次数 <=0无限循环
	target TimeJob        //任务
	st     int64          //执行时的服务器时间
	pre    *TickTimer     //上一个
	next   *TickTimer     //下一个
	group  *tickTimerLink //所属定时器
	remain int            //剩余次数
}

//触发器定时器,可自行构建与释放
//推荐每个地图至少拥有一个自己的TickTimerLink已减少过多定时器之间的竞争
//技能等相关不要使用定时器(非公共部分，用时间做验证即可)
/*
一组排序的tick
每个tickTimerLink拥有自己的id自增
*/
type tickTimerLink struct {
	idInc      uint64     //自增组id
	stopChan   chan bool  //关闭用的定时器
	sync.Mutex            //同步锁
	Head       *TickTimer //头
	Tail       *TickTimer //尾
}

//@return nil
func New_tickTimerLink() *tickTimerLink {
	link := &tickTimerLink{}
	link.stopChan = make(chan bool)
	link.run()
	return link
}

//添加一个定时任务
//@return nil
func (this *tickTimerLink) Add(t time.Duration, times int, job TimeJob) *TickTimer {
	tid := atomic.AddUint64(&this.idInc, 1)
	stime := time.Now().UnixNano()
	tick := &TickTimer{
		Id:     tid,
		Durt:   t,
		Times:  times,
		target: job,
		st:     stime + int64(t),
		group:  this,
	}
	if tick.Times <= 0 {
		tick.remain = -1
	} else {
		tick.remain = tick.Times
	}
	this.Lock()
	if this.insert(tick) {
		this.Unlock()
		this.restart()
		return tick
	}
	this.Unlock()
	return tick
}

//添加一个定时器
//@return false
func (this *tickTimerLink) insert(t *TickTimer) bool {
	t.pre, t.next = nil, nil
	if this.Tail == nil { //空队列
		this.Tail = t
		this.Head = t
		return true
	}
	now := this.Tail
	for now != nil && now.st > t.st {
		now = now.pre
	}
	if now == nil { //插入到表头
		this.Head.pre = t
		t.next = this.Head
		this.Head = t
		return true
	} else {
		if now == this.Tail { //插入到尾
			t.pre = now
			now.next = t
			this.Tail = t
		} else { //插入到中间
			next := now.next
			t.pre = now
			t.next = next
			next.pre = t
			now.next = t
		}
	}
	return false
}

//删除一个定时器
func (this *tickTimerLink) remove(t *TickTimer) {
	//TODO
}

//移除一个头
//@return nil
func (this *tickTimerLink) shift() *TickTimer {
	if this.Head == nil {
		return nil
	}
	h := this.Head
	this.Head = this.Head.next
	if this.Head != nil {
		this.Head.pre = nil
	} else {
		this.Tail = nil
	}
	return h
}

//停止当前得
func (this *tickTimerLink) restart() {
	this.stopChan <- false
}

//重启定时器
func (this *tickTimerLink) run() {
	go func() {
		for {
			nowTime := time.Now().UnixNano()
			watiTime := nowTime
			this.Lock()
			if this.Head != nil {
				watiTime = this.Head.st - nowTime
				for watiTime <= 0 { //如果已经超时
					tc := this.shift()
					if tc.remain < 0 {
						tc.st = nowTime + int64(tc.Durt)
						this.insert(tc)
					} else {
						tc.remain--
						if tc.remain > 0 {
							tc.st = nowTime + int64(tc.Durt)
							this.insert(tc)
						}
					}
					go tc.target.RunTimeJob()
					if this.Head != nil {
						watiTime = this.Head.st - nowTime
					} else {
						break
					}
				}
			}
			if this.Head == nil {
				watiTime = nowTime
			}
			this.Unlock()
			ticker := time.NewTicker(time.Duration(watiTime))
			select {
			case <-ticker.C:
				ticker.Stop()
				ticker = nil
			case flag := <-this.stopChan:
				ticker.Stop()
				ticker = nil
				if flag {
					return
				}
			}
		}
	}()
}
