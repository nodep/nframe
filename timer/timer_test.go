package timer

import (
	"fmt"
	"sync/atomic"
	"testing"
	// "time"
)

var c uint32 = 0
var loged bool = false

type TickTestObj struct {
	Id int
}

func (this *TickTestObj) RunTimeJob() {
}

// 性能测试
func BenchmarkTran(b *testing.B) {
	// b.N会根据函数的运行时间取一个合适的值
	var config atomic.Value
	v := &TickTestObj{}
	config.Store(v)
	for i := 0; i < b.N; i++ {
		v = config.Load().(*TickTestObj)
	}
}

// 并发性能测试
func BenchmarkTranParallel(b *testing.B) {
	var config atomic.Value
	v := make(map[int64]*TickTestObj, 0)
	v[1] = &TickTestObj{123}
	config.Store(v)
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			v = config.Load().(map[int64]*TickTestObj)
			if !loged {
				loged = true
				fmt.Println(v[1])
				xv := make(map[int64]*TickTestObj, 0)
				config.Store(xv)
				fmt.Println(v[1])
				fmt.Println(config.Load().(map[int64]*TickTestObj)[1])
			}
		}
	})
}
