package cfg

type MgoConfig struct {
	Url     string //mongodb://admin:123456@localhost/legend?|mongodb://localhost/legend?
	MaxPool int    //最大连接池
	MinPool int    //最小连接池
}
