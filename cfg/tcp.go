package cfg

/*
	"port":"8804"			//端口
	"url":"/echo",			//路径
	"packLenMax":2048,		//请求封包最大长度
	"safePackSec":5,		//多少秒检查一次累计封包
	"safePackLimit":50000,	//规定时间范围内不能超过最大封包值
	"safeAddrSec":1,		//IP地址链接检测单位时间
	"safeAddrCount":10,		//IP地址单位时间允许最大链接数
	"safeAddrTotal":50,		//同一个IP地址允许保持的最大链接数
	"tls_crt": "",			//TLS证书 hunter.huamco.com.crt
	"tls_key": ""			//TLS密钥 hunter.huamco.com.key
*/
type TcpServiceConfig struct {
	Addrs         string `json:"addrs"`         //对外地址
	Port          string `json:"port"`          //端口
	Url           string `json:"url"`           //路径
	PackLenMax    int    `json:"packLenMax"`    //请求封包最大长度
	SafePackSec   int64  `json:"safePackSec"`   //多少秒检查一次累计封包
	SafePackLimit int    `json:"safepackLimit"` //规定时间范围内不能超过最大封包值
	SafeAddrSec   int64  `json:"safeAddrSec"`   //IP地址链接检测单位时间
	SafeAddrCount int    `json:"safeAddrCount"` //IP地址单位时间允许最大链接数
	SafeAddrTotal int    `json:"safeAddrTotal"` //同一个IP地址允许保持的最大链接数
	TlsCrt        string `json:"tlsCrt"`        //TLS证书 hunter.huamco.com.crt
	TlsKey        string `json:"tlsKey"`        //TLS密钥 hunter.huamco.com.key
}
