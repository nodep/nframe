package cfg

type LogLv int

//配置文件
type LogConfig struct {
	Lv          LogLv  //日志等级
	Name        string //日志的key
	LogFilePath string //日志存储地址,若不填,则不进行存储
	SingleBytes int64  //单个日志文件的大小
	MaxBytes    int64  //最多存储的日志大小
}

//服务配置
type ServiceConfig struct {
	Name string //服务的名称
	Node int64  //所在节点
}
