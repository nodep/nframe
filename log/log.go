package log

import (
	"fmt"
	"os"
	"strings"
	"sync"

	"gitee.com/nodep/nframe/tran"

	"gitee.com/nodep/nframe/util"

	"gitee.com/nodep/nframe/cfg"
)

//日志错误等级
var (
	Lv_Core  cfg.LogLv = -2
	Lv_Error cfg.LogLv = -1
	Lv_Sys   cfg.LogLv = 0
	Lv_Info  cfg.LogLv = 1
	Lv_Debug cfg.LogLv = 2
	Game     *Log      = &Log{}
	Admin    *Log      = &Log{}
	Quiet    bool      = false
	_logDic  map[string]*Log
)

func init() {
	_logDic = make(map[string]*Log, 2)
	_logDic["game"] = Game
	_logDic["admin"] = Admin
	Game.Lv = Lv_Debug
	Admin.Lv = Lv_Debug
}

//创建一个新的日志管理
//@return nil
func New(cf *cfg.LogConfig) *Log {
	key := cf.Name
	if _logDic[key] == nil {
		_logDic[key] = &Log{}
	}
	l := _logDic[key]
	if cf != nil {
		l.Lv = cf.Lv
		l.Name = cf.Name
		l.Cfg = cf
		if l.Cfg.LogFilePath != "" { //有日志地址,进行连接
			cfgPs := l.Cfg.LogFilePath + "log.cfg"
			_, err := os.Stat(cfgPs)
			if err != nil { //如果该文件夹没有写过任何日志
				util.FileWriteString("0_0", cfgPs) //写入最老版本日志与最新的日志
			}
			str := util.FileStringGet(cfgPs)
			tags := strings.Split(str, "_")
			l.fileTagOld = tran.StringToInt(tags[0])
			l.fileTagNow = tran.StringToInt(tags[1])
			l.ReloadFile()
		}
	}
	return l
}

//@return nil
//获取一个日志管理器
func Get(key string) *Log {
	lg := _logDic[key]
	if lg == nil {
		Admin.Error("log.Get key=", key, "Non-existent")
	}
	return lg
}

type Log struct {
	Lv               cfg.LogLv //日志等级
	Name             string
	Cfg              *cfg.LogConfig
	LogFile          *os.File //当前日志文件
	fileTagOld       int      //循环头文件
	fileTagNow       int      //循环尾文件
	totalBytesBefore int64    //前几个文件的总大小
	sync.Mutex                //先用这种简单的方法实现
}

//重新装载log日志文件目标
func (this *Log) ReloadFile() {
	this.Lock()
	defer this.Unlock()
	if this.LogFile != nil {
		this.LogFile.Close()
		this.LogFile = nil
	}
	fpath := this.Cfg.LogFilePath + this.Name + "_" + tran.IntToString(this.fileTagNow) + ".log"
	_, err := os.Stat(fpath)
	if err != nil { //如果文件不存在,创建文件
		util.FileWriteString(util.TimeNowSec1()+"\r日志创建\r", fpath)
	}
	this.LogFile, err = os.OpenFile(fpath, os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
}

//写日志到本地
func (this *Log) Write(arg ...interface{}) {
	if this.LogFile == nil {
		return
	}
	str := util.TimeNowSec1() + "\r" + fmt.Sprint(arg...) + "\r"
	if this.appendLogs(str) {
		this.ReloadFile()
	}
}

//是否超出总大小
func (this *Log) appendLogs(str string) bool {
	this.Lock()
	defer this.Unlock()
	this.LogFile.WriteString(str)
	info, _ := this.LogFile.Stat()
	sSize := info.Size()
	maxOutRange := false
	if sSize > this.Cfg.SingleBytes { //超过单个最大值
		this.fileTagNow++
		if this.totalSize() > this.Cfg.MaxBytes {
			delPath := this.Cfg.LogFilePath + this.Name + "_" + tran.IntToString(this.fileTagOld) + ".log"
			err := os.Remove(delPath)
			if err != nil {
				//日志删除失败无视
			}
			this.fileTagOld++
		}
		fpath := this.Cfg.LogFilePath + "log.cfg"
		util.FileWriteString(tran.IntToString(this.fileTagOld)+"_"+tran.IntToString(this.fileTagNow), fpath)
		maxOutRange = true
	}
	return maxOutRange
}

//当前总值
func (this *Log) totalSize() int64 {
	var totalSize int64 = 0
	for i := this.fileTagOld; i <= this.fileTagNow; i++ {
		filePath := this.Cfg.LogFilePath + this.Name + "_" + tran.IntToString(i) + ".log"
		info, err := os.Stat(filePath)
		if err == nil && info != nil {
			sz := info.Size()
			if sz > this.Cfg.SingleBytes {
				sz = this.Cfg.SingleBytes
			}
			totalSize += sz
		}
	}
	return totalSize
}

func (this *Log) Error(arg ...interface{}) {
	if this.Lv < Lv_Error {
		return
	}
	this.Write("[error]", arg)
	if !Quiet {
		fmt.Println(arg...)
	}
}

func (this *Log) Sys(arg ...interface{}) {
	if this.Lv < Lv_Sys {
		return
	}
	this.Write("[sys]", arg)
	if !Quiet {
		fmt.Println(arg...)
	}
}

//普通信息输出
func (this *Log) Info(arg ...interface{}) {
	if this.Lv < Lv_Info {
		return
	}
	if !Quiet {
		fmt.Println(arg...)
	}
}

//测试输出
func (this *Log) Debug(arg ...interface{}) {
	if this.Lv < Lv_Debug {
		return
	}
	if !Quiet {
		fmt.Println(arg...)
	}
}
