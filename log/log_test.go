package log

import (
	"testing"

	"gitee.com/nodep/nframe/cfg"
	"gitee.com/nodep/nframe/log"
)

func TestLog(t *testing.T) {
	logCfg := &cfg.LogConfig{
		Lv:          2,            //输出等级
		Name:        "admin",      //日志的key
		Port:        "8989",       //端口
		LogFilePath: "./gameLog/", //日志存储地址
		SingleBytes: 8388608,      //单个日志文件最大尺寸字节
		MaxBytes:    838860800,    //总的日志大小字节
	}
	log.New(logCfg)
	log.Game.Error("测试输出")
}
