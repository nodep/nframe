package tran

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"strings"
)

//由于excel中存在浮点数偏差(可能是因为当前的excel库导致的,所以这里需要检查是否有.xxxx)。
//如果存在小数点，则需要将小数点后面干掉
func fixStringWithInt(str string) string {
	idx := strings.Index(str, ".")
	if idx > 0 {
		str = str[:idx]
	}
	return str
}

//@return ""
func IntToString(i int) string {
	return strconv.Itoa(i)
}

func Int8ToString(i int8) string {
	return strconv.FormatInt(int64(i), 10)
}

func Int16ToString(i int16) string {
	return strconv.FormatInt(int64(i), 10)
}

func Int32ToString(i int32) string {
	return strconv.FormatInt(int64(i), 10)
}

func Int64ToString(i int64) string {
	return strconv.FormatInt(i, 10)
}

func StringToInt8(str string) int8 {
	str = fixStringWithInt(str)
	i, _ := strconv.ParseInt(str, 10, 8)
	return int8(i)
}

func StringToUint8(str string) uint8 {
	str = fixStringWithInt(str)
	i, _ := strconv.ParseUint(str, 10, 8)
	return uint8(i)
}

func StringToBool(str string) bool {
	if str == "" {
		return false
	}
	lower := strings.ToLower(str)
	if lower == "true" {
		return true
	} else if lower == "false" {
		return false
	}
	ti := StringToInt(str)
	return ti > 0
}

func StringToUint16(str string) uint16 {
	str = fixStringWithInt(str)
	i, _ := strconv.ParseUint(str, 10, 16)
	return uint16(i)
}

func StringToInt16(str string) int16 {
	str = fixStringWithInt(str)
	i, _ := strconv.ParseInt(str, 10, 16)
	return int16(i)
}

//@return 0
func StringToInt(str string) int {
	str = fixStringWithInt(str)
	i, _ := strconv.Atoi(str)
	return i
}

//字符串塞到数组里
func StringToIntArray(str string, splitStr string, arr *[]int) {
	strs := strings.Split(str, splitStr)
	arrV := *arr
	arrV = arrV[0:0]
	//需要保证顺序
	for i := 0; i < len(strs); i++ {
		arrV = append(arrV, StringToInt(strs[i]))
	}
	*arr = arrV
}

//@return 0
func StringToInt32(str string) int32 {
	str = fixStringWithInt(str)
	i, _ := strconv.ParseInt(str, 10, 32)
	return int32(i)
}

//@return 0
func StringToInt64(str string) int64 {
	str = fixStringWithInt(str)
	i, _ := strconv.ParseInt(str, 10, 64)
	return i
}

//@return 0
func StringToFloat32(str string) float32 {
	i, _ := strconv.ParseFloat(str, 32)
	return float32(i)
}

//@return 0
func StringToFloat64(str string) float64 {
	i, _ := strconv.ParseFloat(str, 64)
	return i
}

//@return ""
func Float64ToString(f float64) string {
	return strconv.FormatFloat(f, 'E', -1, 64)
}

//@return ""
func Float32ToString(f float32) string {
	return strconv.FormatFloat(float64(f), 'E', -1, 64)
}

//取num最近并且>=num的power幂后值
//@return 0
func UpperPowerBy(num int, power int) int {
	c := power
	for c < num {
		c = c * power
	}
	return c
}

//@return nil
func Int16ToBytes_S(v2 int16) []byte {
	var b2 []byte = make([]byte, 2, 2)
	b2[0] = uint8(v2)
	b2[1] = uint8(v2 >> 8)
	return b2
}

//@return nil
func Int16ToBytes_B(v2 int16) []byte {
	var b2 []byte = make([]byte, 2, 2)
	b2[1] = uint8(v2)
	b2[0] = uint8(v2 >> 8)
	return b2
}

//@return nil
func Int32ToBytes_S(v4 int32) []byte {
	var b4 []byte = make([]byte, 4, 4)
	b4[0] = uint8(v4)
	b4[1] = uint8(v4 >> 8)
	b4[2] = uint8(v4 >> 16)
	b4[3] = uint8(v4 >> 24)
	return b4
}

func Uint32ToBytes_S(v4 uint32) []byte {
	var b4 []byte = make([]byte, 4, 4)
	b4[0] = uint8(v4)
	b4[1] = uint8(v4 >> 8)
	b4[2] = uint8(v4 >> 16)
	b4[3] = uint8(v4 >> 24)
	return b4
}

func Uint16ToBytes_S(v2 uint16) []byte {
	var b4 []byte = make([]byte, 2, 2)
	b4[0] = uint8(v2)
	b4[1] = uint8(v2 >> 8)
	return b4
}

//@return nil
func Int32ToBytes_B(v4 int32) []byte {
	var b4 []byte = make([]byte, 4, 4)
	b4[3] = uint8(v4)
	b4[2] = uint8(v4 >> 8)
	b4[1] = uint8(v4 >> 16)
	b4[0] = uint8(v4 >> 24)
	return b4
}

//@return nil
func IntToBytes_S(v4 int) []byte {
	var b4 []byte = make([]byte, 4, 4)
	b4[0] = uint8(v4)
	b4[1] = uint8(v4 >> 8)
	b4[2] = uint8(v4 >> 16)
	b4[3] = uint8(v4 >> 24)
	return b4
}

//@return nil
func IntToBytes_B(v4 int) []byte {
	var b4 []byte = make([]byte, 4, 4)
	b4[3] = uint8(v4)
	b4[2] = uint8(v4 >> 8)
	b4[1] = uint8(v4 >> 16)
	b4[0] = uint8(v4 >> 24)
	return b4
}

//@return nil
func Int64ToBytes_S(v8 int64) []byte {
	var b8 []byte = make([]byte, 8, 8)
	b8[0] = uint8(v8)
	b8[1] = uint8(v8 >> 8)
	b8[2] = uint8(v8 >> 16)
	b8[3] = uint8(v8 >> 24)
	b8[4] = uint8(v8 >> 32)
	b8[5] = uint8(v8 >> 40)
	b8[6] = uint8(v8 >> 48)
	b8[7] = uint8(v8 >> 56)
	return b8
}

//@return nil
func Int64ToBytes_B(v8 int64) []byte {
	var b8 []byte = make([]byte, 8, 8)
	b8[7] = uint8(v8)
	b8[6] = uint8(v8 >> 8)
	b8[5] = uint8(v8 >> 16)
	b8[4] = uint8(v8 >> 24)
	b8[3] = uint8(v8 >> 32)
	b8[2] = uint8(v8 >> 40)
	b8[1] = uint8(v8 >> 48)
	b8[0] = uint8(v8 >> 56)
	return b8
}

//@return 0
func BytesToInt64_B(buf []byte) int64 {
	return int64(binary.BigEndian.Uint64(buf))
}

//@return 0
func BytesToInt64_S(buf []byte) int64 {
	return int64(binary.LittleEndian.Uint64(buf))
}

//@return false
func IsJudge2(n int) bool {
	if n <= 0 {
		return false
	}
	return n&(n-1) == 0
}

func ToString(v interface{}) string {
	rst := ""
	switch v.(type) {
	case byte:
		rst = Int8ToString(int8(v.(byte)))
	case int8:
		rst = Int8ToString(v.(int8))
	case int16:
		rst = Int16ToString(v.(int16))
	case int32:
		rst = Int32ToString(v.(int32))
	case int:
		rst = IntToString(v.(int))
	case int64:
		rst = Int64ToString(v.(int64))
	case float32:
		rst = Float32ToString(v.(float32))
	case float64:
		rst = Float64ToString(v.(float64))
	case bool:
		if v.(bool) {
			rst = "true"
		} else {
			rst = "false"
		}
	case string:
		rst = v.(string)
	}
	return rst
}

func ToInt(v interface{}) int {
	rst := 0
	switch v.(type) {
	case string:
		rst = StringToInt(v.(string))
	case float64:
		rst = int(v.(float64))
	case float32:
		rst = int(v.(float32))
	case int:
		rst = v.(int)
	case int8:
		rst = int(v.(int8))
	case int16:
		rst = int(v.(int8))
	case int32:
		rst = int(v.(int32))
	case int64:
		rst = int(v.(int64))
	}
	return rst
}

func ToInt64(v interface{}) (rst int64) {
	switch v.(type) {
	case string:
		rst = StringToInt64(v.(string))
	case float32:
		rst = int64(v.(float32))
	case float64:
		rst = int64(v.(float64))
	case int:
		rst = int64(v.(int))
	case int8:
		rst = int64(v.(int8))
	case int16:
		rst = int64(v.(int8))
	case int32:
		rst = int64(v.(int32))
	case int64:
		rst = v.(int64)
	}
	return
}

//保留小数位数
func Float64Round(value float64, c int) float64 {
	str := "%." + IntToString(c) + "f"
	value, _ = strconv.ParseFloat(fmt.Sprintf(str, value), 64)
	return value
}

//将字符串存储为虚幻存储格式的字符串
/*
017b000000fe64e443-04000000-61-61-61-00   aaa
017b000000fe64e443-05000000-61-62-63-64-00 abcd
017b000000fe64e443-00000000 空字符串
017b000000fe64e443-feffffff-1162-0000 我
017b000000fe64e443-fdffffff-1162-1162-0000 我我
017b000000fe64e443-fcffffff-1162-1162-1162-0000 我我我
017b000000fe64e443-fcffffff-1162-1162-6100-0000 我我a
017b000000fe64e443-fcffffff-1162-6100-1162-0000 我a我
017b000000fe64e443-fcffffff-1162-6100-6100-1162-0000 我aa我
017b000000fe64e443-fbffffff-1162-6100-1162-6100-0000 我a我a
017b000000fe64e443-02000000-6100      a

经过上面的观察可以发现,如果全是英文，则01000000 每个字母+1
如果有中文，则是ffffffff每一个字母-1 好像就这么简单
另外遇到连续英文的结束需要+00 如果句子中有中文,则需要在结尾加0000

Unicode编码 我对应0x6211，其实这就是一种Unicode编码格式
https://blog.csdn.net/weixin_29888579/article/details/112710289
*/
func StringToUeBytes(str string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if len(str) == 0 { //空字符串直接返回
		zeroBytes := Int32ToBytes_S(0)
		return zeroBytes
	}
	runes := []rune(str)
	hasCn := false
	for i := 0; i < len(runes); i++ {
		a := runes[i]
		if a > 255 {
			hasCn = true
			break
		}
	}
	var totalLen uint32 = uint32(len(runes))
	for i := 0; i < len(runes); i++ {
		a := runes[i]
		if a > 255 { //中文
			bts = append(bts, Int16ToBytes_S(int16(a))...)
		} else { //英文
			bts = append(bts, byte(a))
			if hasCn {
				bts = append(bts, 0)
			}
		}
	}
	totals := make([]byte, 0, 4)
	if hasCn {
		bts = append(bts, 0)
		bts = append(bts, 0)
		totals = append(totals, Uint32ToBytes_S(4294967295-totalLen)...)
	} else {
		bts = append(bts, 0)
		totals = append(totals, Uint32ToBytes_S(totalLen+1)...)
	}
	totals = append(totals, bts...)
	return totals
}

//https://blog.csdn.net/digdugbomb/article/details/119838f975
func FloatToUeBytes(f float32) []byte {
	bits := math.Float32bits(f)
	bytes := make([]byte, 4)
	binary.LittleEndian.PutUint32(bytes, bits)
	return bytes
}

func Float64ToUeBytes(f float64) []byte {
	bits := math.Float64bits(f)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)
	return bytes
}

func Uint8ToUeBytes(u uint8) []byte {
	var bts []byte = make([]byte, 1, 1)
	bts[0] = u
	return bts
}

func BoolToUeBytes(b bool) []byte {
	if b {
		return Int32ToBytes_S(1)
	} else {
		return Int32ToBytes_S(0)
	}
}

func BoolToUeBytesForArray(b bool) []byte {
	if b {
		return Uint8ToUeBytes(1)
	} else {
		return Uint8ToUeBytes(0)
	}
}

//x,y,z格式的float转换为Vector
func VectorToUeBytes(v string) []byte {
	if v == "" {
		v = "0,0,0"
	}
	vstrs := strings.Split(v, ",")
	var x float64 = StringToFloat64(vstrs[0])
	var y float64 = StringToFloat64(vstrs[1])
	var z float64 = StringToFloat64(vstrs[2])
	var bts []byte = make([]byte, 0, 24)
	bts = append(bts, Float64ToUeBytes(x)...)
	bts = append(bts, Float64ToUeBytes(y)...)
	bts = append(bts, Float64ToUeBytes(z)...)
	return bts
}

func VectorToUeBytes32(v string) []byte {
	if v == "" {
		v = "0,0,0"
	}
	vstrs := strings.Split(v, ",")
	var x float32 = StringToFloat32(vstrs[0])
	var y float32 = StringToFloat32(vstrs[1])
	var z float32 = StringToFloat32(vstrs[2])
	var bts []byte = make([]byte, 0, 12)
	bts = append(bts, FloatToUeBytes(x)...)
	bts = append(bts, FloatToUeBytes(y)...)
	bts = append(bts, FloatToUeBytes(z)...)
	return bts
}

func Vector2DToUeBytes(v string) []byte {
	if v == "" {
		v = "0,0"
	}
	vstrs := strings.Split(v, ",")
	var x float64 = StringToFloat64(vstrs[0])
	var y float64 = StringToFloat64(vstrs[1])
	var bts []byte = make([]byte, 0, 2)
	bts = append(bts, Float64ToUeBytes(x)...)
	bts = append(bts, Float64ToUeBytes(y)...)
	return bts
}

func Vector2DToUeBytes32(v string) []byte {
	if v == "" {
		v = "0,0"
	}
	vstrs := strings.Split(v, ",")
	var x float32 = StringToFloat32(vstrs[0])
	var y float32 = StringToFloat32(vstrs[1])
	var bts []byte = make([]byte, 0, 2)
	bts = append(bts, FloatToUeBytes(x)...)
	bts = append(bts, FloatToUeBytes(y)...)
	return bts
}

//p,y,r格式的float转换为rotator
func RotatorToUeBytes(v string) []byte {
	if v == "" {
		v = "0,0,0"
	}
	vstrs := strings.Split(v, ",")
	var x float64 = StringToFloat64(vstrs[0])
	var y float64 = StringToFloat64(vstrs[1])
	var z float64 = StringToFloat64(vstrs[2])
	var bts []byte = make([]byte, 0, 24)
	bts = append(bts, Float64ToUeBytes(x)...)
	bts = append(bts, Float64ToUeBytes(y)...)
	bts = append(bts, Float64ToUeBytes(z)...)
	return bts
}

//p,y,r格式的float转换为rotator
func RotatorToUeBytes32(v string) []byte {
	if v == "" {
		v = "0,0,0"
	}
	vstrs := strings.Split(v, ",")
	var x float32 = StringToFloat32(vstrs[0])
	var y float32 = StringToFloat32(vstrs[1])
	var z float32 = StringToFloat32(vstrs[2])
	var bts []byte = make([]byte, 0, 12)
	bts = append(bts, FloatToUeBytes(x)...)
	bts = append(bts, FloatToUeBytes(y)...)
	bts = append(bts, FloatToUeBytes(z)...)
	return bts
}

//i,i,i,i格式转换为TArray<int32>
//[i,i,i,i]格式转换为TArray<int32>
func TArrayInt32ToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	if v[0:1] == "[" {
		v = v[1 : len(v)-1]
	}
	strs := strings.Split(v, ",")
	bts = append(bts, Int32ToBytes_S(int32(len(strs)))...)
	for i := 0; i < len(strs); i++ {
		if strs[i] == "" {
			bts = append(bts, Int32ToBytes_S(0)...)
		} else {
			v := StringToInt32(strs[i])
			bts = append(bts, Int32ToBytes_S(v)...)
		}
	}
	return bts
}

func TArrayBoolToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	if v[0:1] == "[" {
		v = v[1 : len(v)-1]
	}
	strs := strings.Split(v, ",")
	bts = append(bts, Int32ToBytes_S(int32(len(strs)))...)
	for i := 0; i < len(strs); i++ {
		if strs[i] == "" {
			bts = append(bts, BoolToUeBytesForArray(StringToBool("0"))...)
		} else {
			bts = append(bts, BoolToUeBytesForArray(StringToBool(strs[i]))...)
		}
	}
	return bts
}

func TArrayUint8ToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	if v[0:1] == "[" {
		v = v[1 : len(v)-1]
	}
	strs := strings.Split(v, ",")
	bts = append(bts, Int32ToBytes_S(int32(len(strs)))...)
	for i := 0; i < len(strs); i++ {
		if strs[i] == "" {
			bts = append(bts, Uint8ToUeBytes(0)...)
		} else {
			bts = append(bts, Uint8ToUeBytes(StringToUint8(strs[i]))...)
		}
	}
	return bts
}

func TArrayInt64ToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	if v[0:1] == "[" {
		v = v[1 : len(v)-1]
	}
	strs := strings.Split(v, ",")
	bts = append(bts, Int32ToBytes_S(int32(len(strs)))...)
	for i := 0; i < len(strs); i++ {
		if strs[i] == "" {
			bts = append(bts, Int64ToBytes_S(0)...)
		} else {
			bts = append(bts, Int64ToBytes_S(StringToInt64(strs[i]))...)
		}
	}
	return bts
}

func TArrayStringToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	strList := make([]string, 0, 2)
	json.Unmarshal([]byte(v), &strList)
	bts = append(bts, Int32ToBytes_S(int32(len(strList)))...)
	for i := 0; i < len(strList); i++ {
		bts = append(bts, StringToUeBytes(strList[i])...)
	}
	return bts
}

//["0,0,0","0,0,0"]
func TArrayVectorToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	strList := make([]string, 0, 2)
	json.Unmarshal([]byte(v), &strList)
	bts = append(bts, Int32ToBytes_S(int32(len(strList)))...)
	for i := 0; i < len(strList); i++ {
		bts = append(bts, VectorToUeBytes(strList[i])...)
	}
	return bts
}

//["0,0,0","0,0,0"]
func TArrayVectorToUeBytes32(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	strList := make([]string, 0, 2)
	json.Unmarshal([]byte(v), &strList)
	bts = append(bts, Int32ToBytes_S(int32(len(strList)))...)
	for i := 0; i < len(strList); i++ {
		bts = append(bts, VectorToUeBytes32(strList[i])...)
	}
	return bts
}

//["0,0","0,0"]
func TArrayVector2DToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	strList := make([]string, 0, 2)
	json.Unmarshal([]byte(v), &strList)
	bts = append(bts, Int32ToBytes_S(int32(len(strList)))...)
	for i := 0; i < len(strList); i++ {
		bts = append(bts, Vector2DToUeBytes(strList[i])...)
	}
	return bts
}

//["0,0","0,0"]
func TArrayVector2DToUeBytes32(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	strList := make([]string, 0, 2)
	json.Unmarshal([]byte(v), &strList)
	bts = append(bts, Int32ToBytes_S(int32(len(strList)))...)
	for i := 0; i < len(strList); i++ {
		bts = append(bts, Vector2DToUeBytes32(strList[i])...)
	}
	return bts
}

func TArrayRotatorToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	strList := make([]string, 0, 2)
	json.Unmarshal([]byte(v), &strList)
	bts = append(bts, Int32ToBytes_S(int32(len(strList)))...)
	for i := 0; i < len(strList); i++ {
		bts = append(bts, VectorToUeBytes(strList[i])...)
	}
	return bts
}

func TArrayRotatorToUeBytes32(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	strList := make([]string, 0, 2)
	json.Unmarshal([]byte(v), &strList)
	bts = append(bts, Int32ToBytes_S(int32(len(strList)))...)
	for i := 0; i < len(strList); i++ {
		bts = append(bts, VectorToUeBytes32(strList[i])...)
	}
	return bts
}

func TArrayFloatToUeBytes(v string) []byte {
	var bts []byte = make([]byte, 0, 4)
	if v == "" || v == "[]" {
		bts = append(bts, Int32ToBytes_S(0)...)
		return bts
	}
	if v[0:1] == "[" {
		v = v[1 : len(v)-1]
	}
	strs := strings.Split(v, ",")
	bts = append(bts, Int32ToBytes_S(int32(len(strs)))...)
	for i := 0; i < len(strs); i++ {
		if strs[i] == "" {
			bts = append(bts, FloatToUeBytes(0)...)
		} else {
			bts = append(bts, FloatToUeBytes(StringToFloat32(strs[i]))...)
		}
	}
	return bts
}
