package tran

import (
	"encoding/binary"
	"fmt"
	"testing"
)

// 单元测试
// 测试全局函数，以TestFunction命名
// 测试类成员函数，以TestClass_Function命名
func TestTran(t *testing.T) {
	// for i := 0; i < 10; i++ {
	// 	IntToString(123123)
	// 	StringToInt("123123")
	// }
	// //-------------移位测试-----------------
	// fmt.Println("移位测试")

	var i int = -100
	bts := IntToBytes_B(i)
	v := int(int32(binary.BigEndian.Uint32(bts)))
	fmt.Println(v)
}

// 性能测试
func BenchmarkTran(b *testing.B) {
	// b.N会根据函数的运行时间取一个合适的值
	for i := 0; i < b.N; i++ {
		IntToString(i * 100)
		StringToInt("123123")
	}
}

// 并发性能测试
func BenchmarkTranParallel(b *testing.B) {
	// 测试一个对象或者函数在多线程的场景下面是否安全
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			IntToString(123123)
			StringToInt("123123")
		}
	})
}
