package util

import (
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"os"
	"strings"

	"github.com/nfnt/resize"
)

//将一个文件夹下的png全部打包到指定的位置
//根据path生成一个atlas(这里的path是不带扩展名的文件名字))
//这个打包是为了兼容Laya3.0
func PackImageForLaya2(urls []string, exportUrl string, mapping map[string]string, basePath string) {
	fmt.Println("package2...")
	fmt.Println(urls)
	fmt.Println(exportUrl)
	fmt.Println(basePath)
	fmt.Println("-----------------------")
	if mapping == nil {
		mapping = make(map[string]string, 0)
	}
	sizeList := []int{128, 256, 512, 1024, 2048} //128, 512, 1024, 2048, 2048 * 2
	var img image.Image
	var rgba *image.RGBA
	var cutx int
	var cuty int
	var gridList []*RgbaGrid = make([]*RgbaGrid, 0, 2)
	box := &GridBox{}
	nameMapping := make(map[string]string, 0)
	for _, url := range urls {
		mappinged := false
		for k, v := range mapping {
			if strings.Index(url, k) >= 0 { //如果在映射表中
				mappinged = true
				kf := k[strings.LastIndex(k, "/")+1:]
				kf = kf[:strings.LastIndex(kf, ".")]
				kv := v[strings.LastIndex(v, "/")+1:]
				kv = kv[:strings.LastIndex(kv, ".")]
				nameMapping[kf] = kv
				break
			}
		}
		if mappinged {
			continue
		}
		// f := url[strings.LastIndex(url, "/")+1:]
		f := url[strings.LastIndex(basePath, "/")+1:]
		// f = f[:strings.LastIndex(f, ".")]
		img, _ = GetImageByUrl(url)
		rgba, cutx, cuty = cutImgSize(img)
		gridList = append(gridList, &RgbaGrid{
			Name: f,
			Rgba: rgba,
			CutX: cutx,
			Cuty: cuty,
		})
	}
	//---------------生成一个新的名称mapping-----------------
	isOk := false
	for i := 0; i < len(sizeList); i++ {
		size := box.sortGrid(sizeList[i], gridList)
		if size > 0 {
			isOk = true
			break
		}
	}
	if !isOk {
		panic("2048打包失败,请检查" + exportUrl)
	}
	//--------输出新的Img----------
	box.buildTestImg(exportUrl + ".png")
	fname := exportUrl[strings.LastIndex(exportUrl, "/")+1:]
	dirName := exportUrl[:strings.LastIndex(exportUrl, "/")]
	dirName = dirName[strings.LastIndex(dirName, "/")+1:]
	//--------输出动画配置----------//
	aCfg := &Atlas{}
	aCfg.Meta = &AtlasMeta{}
	aCfg.Meta.Image = fname + ".png"
	aCfg.Meta.Prefix = dirName + "/" + fname + "/"
	aCfg.Frames = make(map[string]*FrameInfo, 0)
	for _, gg := range box.sorted {
		finfo := &FrameInfo{}
		finfo.Frame = &Frame{
			H:   gg.Rgba.Bounds().Size().Y,
			W:   gg.Rgba.Bounds().Size().X,
			Idx: 0,
			X:   gg.PosX,
			Y:   gg.PosY,
		}
		finfo.SourceSize = &SourceSize{
			H: gg.Rgba.Bounds().Size().Y,
			W: gg.Rgba.Bounds().Size().X,
		}
		finfo.SpriteSourceSize = &SpriteSourceSize{
			X: 0,
			Y: 0,
		}
		aCfg.Frames[gg.Name] = finfo
		for k, v := range nameMapping {
			if v == gg.Name {
				aCfg.Frames[k] = finfo
			}
		}
	}
	FileWriteString(JSONString(aCfg), exportUrl+".atlas")
}

//将一个文件夹下的png全部打包到指定的位置
//根据path生成一个atlas(这里的path是不带扩展名的文件名字))
func PackImageForLaya(urls []string, exportUrl string, mapping map[string]string) {
	if mapping == nil {
		mapping = make(map[string]string, 0)
	}
	sizeList := []int{128, 256, 512, 1024, 2048} //128, 512, 1024, 2048, 2048 * 2
	var img image.Image
	var rgba *image.RGBA
	var cutx int
	var cuty int
	var gridList []*RgbaGrid = make([]*RgbaGrid, 0, 2)
	box := &GridBox{}
	nameMapping := make(map[string]string, 0)
	for _, url := range urls {
		mappinged := false
		for k, v := range mapping {
			if strings.Index(url, k) >= 0 { //如果在映射表中
				mappinged = true
				kf := k[strings.LastIndex(k, "/")+1:]
				kf = kf[:strings.LastIndex(kf, ".")]
				kv := v[strings.LastIndex(v, "/")+1:]
				kv = kv[:strings.LastIndex(kv, ".")]
				nameMapping[kf] = kv
				break
			}
		}
		if mappinged {
			continue
		}
		f := url[strings.LastIndex(url, "/")+1:]
		f = f[:strings.LastIndex(f, ".")]
		img, _ = GetImageByUrl(url)
		rgba, cutx, cuty = cutImgSize(img)
		gridList = append(gridList, &RgbaGrid{
			Name: f,
			Rgba: rgba,
			CutX: cutx,
			Cuty: cuty,
		})
	}
	//---------------生成一个新的名称mapping-----------------
	isOk := false
	for i := 0; i < len(sizeList); i++ {
		size := box.sortGrid(sizeList[i], gridList)
		if size > 0 {
			isOk = true
			break
		}
	}
	if !isOk {
		panic("2048打包失败,请检查" + exportUrl)
	}
	//--------输出新的Img----------
	box.buildTestImg(exportUrl + ".png")
	fname := exportUrl[strings.LastIndex(exportUrl, "/")+1:]
	dirName := exportUrl[:strings.LastIndex(exportUrl, "/")]
	dirName = dirName[strings.LastIndex(dirName, "/")+1:]
	//--------输出动画配置----------//
	aCfg := &Atlas{}
	aCfg.Meta = &AtlasMeta{}
	aCfg.Meta.Image = fname + ".png"
	aCfg.Meta.Prefix = dirName + "/" + fname
	aCfg.Frames = make(map[string]*FrameInfo, 0)
	for _, gg := range box.sorted {
		finfo := &FrameInfo{}
		finfo.Frame = &Frame{
			H:   gg.Rgba.Bounds().Size().Y,
			W:   gg.Rgba.Bounds().Size().X,
			Idx: 0,
			X:   gg.PosX,
			Y:   gg.PosY,
		}
		finfo.SourceSize = &SourceSize{
			H: gg.Rgba.Bounds().Size().Y,
			W: gg.Rgba.Bounds().Size().X,
		}
		finfo.SpriteSourceSize = &SpriteSourceSize{
			X: 0,
			Y: 0,
		}
		aCfg.Frames[gg.Name] = finfo
		for k, v := range nameMapping {
			if v == gg.Name {
				aCfg.Frames[k] = finfo
			}
		}
	}
	FileWriteString(JSONString(aCfg), exportUrl+".atlas")
}

type Atlas struct {
	Rate   int                   `json:"rate,omitempty"`
	Frames map[string]*FrameInfo `json:"frames"`
	Meta   *AtlasMeta            `json:"meta"`
	Mc     map[string]([]string) `json:"mc,omitempty"`
}

type FrameInfo struct {
	Frame            *Frame            `json:"frame"`
	SourceSize       *SourceSize       `json:"sourceSize"`
	SpriteSourceSize *SpriteSourceSize `json:"spriteSourceSize"`
}

type Frame struct {
	H   int `json:"h"`
	Idx int `json:"idx"`
	W   int `json:"w"`
	X   int `json:"x"`
	Y   int `json:"y"`
	Ox  int `json:"ox,omitempty"`
	Oy  int `json:"oy,omitempty"`
}

type SourceSize struct {
	H int `json:"h"`
	W int `json:"w"`
}

type SpriteSourceSize struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type AtlasMeta struct {
	Image  string `json:"image"`
	Prefix string `json:"prefix"`
}

type GridEmpty struct {
	PosX  int
	PosY  int
	SizeX int
	SizeY int
}

type GridBox struct {
	sorted     []*RgbaGrid
	unsorted   []*RgbaGrid
	emptyGrids []*GridEmpty
	maxSize    int
}

//设置到一个空的格子中,并且生成两个空格子
func (this *GridBox) pushToEmptyGrid(g *RgbaGrid) bool {
	//寻找一个能放进去的空格子,放进去(选当前面积最大的一个)
	var maxSize int = 100000000
	var target *GridEmpty = nil
	// fmt.Println("-----", g.Rgba.Bounds().Size().Y)
	// if len(this.emptyGrids) == 3 {
	// 	for _, v := range this.emptyGrids {
	// 		fmt.Println(v.PosX, v.PosY, v.SizeX, v.SizeY)
	// 	}
	// }
	//获取放的下的格子,很重要
	for _, v := range this.emptyGrids {
		if v.SizeX >= g.Rgba.Bounds().Size().X && v.SizeY >= g.Rgba.Bounds().Size().Y { //首先格子必须能放下东西
			if v.SizeX*v.SizeY < maxSize {
				maxSize = v.SizeX * v.SizeY
				target = v
			}
		}
	}
	if target == nil {
		return false
	}
	for i := 0; i < len(this.emptyGrids); i++ {
		if this.emptyGrids[i] == target { //移除空格子
			this.emptyGrids = append(this.emptyGrids[:i], this.emptyGrids[i+1:]...)
			break
		}
	}
	//设置g的位置到target的位置
	g.PosX = target.PosX
	g.PosY = target.PosY
	this.sorted = append(this.sorted, g)
	//拆分成两个GridEmpty
	topGrid := &GridEmpty{
		PosX:  g.PosX + g.Rgba.Bounds().Size().X,
		PosY:  g.PosY,
		SizeX: this.maxSize - g.PosX - g.Rgba.Bounds().Size().X,
		SizeY: g.Rgba.Bounds().Size().Y,
	}
	this.emptyGrids = append(this.emptyGrids, topGrid)
	bottomGrid := &GridEmpty{
		PosX:  g.PosX,
		PosY:  g.PosY + g.Rgba.Bounds().Size().Y,
		SizeX: this.maxSize - g.PosX,
		SizeY: target.SizeY - g.Rgba.Bounds().Size().Y,
	}
	this.emptyGrids = append(this.emptyGrids, bottomGrid)
	return true
}

//过去高度最高的一个盒子
func (this *GridBox) popHeightMaxFromUnsorted() *RgbaGrid {
	maxH := 0
	var tar *RgbaGrid = nil
	var maxIndex int = -1
	for i := 0; i < len(this.unsorted); i++ {
		s := this.unsorted[i]
		if s.Rgba.Bounds().Size().Y > maxH {
			maxH = s.Rgba.Bounds().Size().Y
			tar = s
			maxIndex = i
		}
	}
	// fmt.Println("maxH->", tar.Rgba.Bounds().Size().Y)
	this.unsorted = append(this.unsorted[:maxIndex], this.unsorted[maxIndex+1:]...)
	return tar
}

//对list进行填充,填充成功返回面积。否则返回-1
func (this *GridBox) sortGrid(maxSize int, list []*RgbaGrid) int {
	this.maxSize = maxSize
	this.sorted = make([]*RgbaGrid, 0, 2)
	this.unsorted = make([]*RgbaGrid, 0, 2)
	this.unsorted = append(this.unsorted, list...)
	this.emptyGrids = make([]*GridEmpty, 1, 2)
	this.emptyGrids[0] = &GridEmpty{
		PosX:  0,
		PosY:  0,
		SizeX: maxSize,
		SizeY: maxSize,
	}
	canSet := true
	i := 0
	for canSet && len(this.unsorted) > 0 {
		tar := this.popHeightMaxFromUnsorted()
		canSet = this.pushToEmptyGrid(tar)
		// this.buildTestImg("./test/" + tran.IntToString(i) + ".png")
		i++
	}
	if canSet { //全部放置成功
		return 1
	}
	return -1
}

//输出测试用的图片
func (this *GridBox) buildTestImg(url string) {
	if len(this.sorted) <= 0 {
		return
	}
	//构造路径
	path := url[:strings.LastIndex(url, "/")+1]
	CreatePath(path)
	os.Remove(url)
	file, err := os.Create(url)
	defer file.Close()
	if err != nil {
		fmt.Println(err)
	}
	maxX := 0
	maxY := 0
	for _, v := range this.sorted {
		if v.PosX+v.Rgba.Bounds().Size().X > maxX {
			maxX = v.PosX + v.Rgba.Bounds().Size().X
		}
		if v.PosY+v.Rgba.Bounds().Size().Y > maxY {
			maxY = v.PosY + v.Rgba.Bounds().Size().Y
		}
	}
	rectangle := image.Rect(0, 0, maxX, maxY)
	rgba := image.NewRGBA(rectangle)
	for _, v := range this.sorted {
		for x := 0; x < v.Rgba.Bounds().Size().X; x++ {
			for y := 0; y < v.Rgba.Bounds().Size().Y; y++ {
				rgba.Set(v.PosX+x, v.PosY+y, v.Rgba.At(x, y))
			}
		}
	}
	png.Encode(file, rgba)

}

//创建Png
func CreateJpgFromU32(imgW int, imgH int, list []uint32, url string) {
	fmt.Println("创建一个检测图,w=", imgW, "h=", imgH, "len=", len(list), "url=", url)
	rectangle := image.Rect(0, 0, imgW, imgH)
	rgba := image.NewRGBA(rectangle)
	for y := 0; y < imgH; y++ {
		for x := 0; x < imgW; x++ {
			idx := y*imgW + x
			if idx >= len(list) {
				fmt.Println("坐标越界", x, y, imgW, imgH, idx)
			}
			c := list[idx]
			rgba.SetRGBA(x, y, color.RGBA{
				uint8((c & 0xFF000000) >> 24),
				uint8((c & 0x00FF0000) >> 16),
				uint8((c & 0x0000FF00) >> 8),
				uint8(c & 0x000000FF),
			})
		}
	}
	path := url[:strings.LastIndex(url, "/")+1]
	CreatePath(path)
	url += ".png"
	os.Remove(url)
	file, err := os.Create(url)
	defer file.Close()
	if err != nil {
		fmt.Println(err)
	}
	png.Encode(file, rgba)
}

type RgbaGrid struct {
	Name string
	Rgba *image.RGBA
	CutX int
	Cuty int
	PosX int
	PosY int
}

//构造一个新的img
func cutImgSize(img image.Image) (*image.RGBA, int, int) {
	allA0 := false
	xleft := 0
	for x := 0; x < img.Bounds().Size().X; x++ { //计算左边界
		allA0 = true
		for y := 0; y < img.Bounds().Size().Y; y++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			xleft = x
			break
		}
	}
	xright := -1
	for x := img.Bounds().Size().X - 1; x >= 0; x-- { //计算右边界
		allA0 = true
		for y := 0; y < img.Bounds().Size().Y; y++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			xright = x
			break
		}
	}
	ytop := 0
	for y := 0; y < img.Bounds().Size().Y; y++ { //计算上边界
		allA0 = true
		for x := 0; x < img.Bounds().Size().X; x++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			ytop = y
			break
		}
	}
	ybottom := -1
	for y := img.Bounds().Size().Y - 1; y >= 0; y-- { //计算下边界
		allA0 = true
		for x := 0; x < img.Bounds().Size().X; x++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			ybottom = y
			break
		}
	}
	rectangle := image.Rect(0, 0, xright-xleft+1, ybottom-ytop+1)
	rgba := image.NewRGBA(rectangle)
	for x := xleft; x <= xright; x++ {
		for y := ytop; y <= ybottom; y++ {
			rgba.Set(x-xleft, y-ytop, img.At(x, y))
		}
	}
	return rgba, xleft, ytop
}

//通过url获取一个图片
func GetImageByUrl(url string) (image.Image, error) {
	file1, _ := os.Open(url)
	defer file1.Close()
	var (
		img1 image.Image
		err  error
	)
	isJpg := strings.Index(strings.ToUpper(url), ".JPG") > 0
	if isJpg {
		img1, err = jpeg.Decode(file1)
	} else {
		img1, err = png.Decode(file1)
	}
	return img1, err
}

//获取一个图片的透明区包裹
func GetAlphaRectange(img image.Image) (int, int, int, int) {
	allA0 := false
	xleft := 0
	for x := 0; x < img.Bounds().Size().X; x++ { //计算左边界
		allA0 = true
		for y := 0; y < img.Bounds().Size().Y; y++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			xleft = x
			break
		}
	}
	xright := -1
	for x := img.Bounds().Size().X - 1; x >= 0; x-- { //计算右边界
		allA0 = true
		for y := 0; y < img.Bounds().Size().Y; y++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			xright = x
			break
		}
	}
	ytop := 0
	for y := 0; y < img.Bounds().Size().Y; y++ { //计算上边界
		allA0 = true
		for x := 0; x < img.Bounds().Size().X; x++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			ytop = y
			break
		}
	}
	ybottom := -1
	for y := img.Bounds().Size().Y - 1; y >= 0; y-- { //计算下边界
		allA0 = true
		for x := 0; x < img.Bounds().Size().X; x++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a > 0 {
				allA0 = false
				break
			}
		}
		if !allA0 {
			ybottom = y
			break
		}
	}
	return xleft, xright, ytop, ybottom
}

//获取一个RGBA
func GetSubRGBA(img image.Image, xleft, xright, ytop, ybottom int) *image.RGBA {
	rgba := image.NewRGBA(image.Rect(0, 0, xright-xleft+1, ybottom-ytop+1))
	for x := xleft; x <= xright; x++ {
		for y := ytop; y <= ybottom; y++ {
			rgba.Set(x-xleft, y-ytop, img.At(x, y))
		}
	}
	return rgba
}

//将img缩略到一定尺寸后发布到指定地址
func ResizeTo(img image.Image, mw int16, mh int16, path string) {
	var maxW float32 = 0
	var maxH float32 = 0
	if mw <= 0 {
		mw = 512
	}
	if mh <= 0 {
		mh = 512
	}
	maxW = float32(mw)
	maxH = float32(mh)
	fmt.Println(maxW, maxH)
	imgW := float32(img.Bounds().Size().X)
	imgH := float32(img.Bounds().Size().Y)
	fmt.Println(imgW, imgH)
	scaleX := 0
	scaleY := 0
	if imgW <= maxW && imgH <= maxH {
		scaleX = int(maxW)
	} else {
		if imgW/maxW >= imgH/maxH { //宽度更宽,以宽为准
			scaleX = int(maxW)
		} else { //以高为准
			scaleY = int(maxH)
		}
	}
	min := resize.Resize(uint(scaleX), uint(scaleY), img, resize.Lanczos3)
	os.Remove(path)
	CreatePath(path[:strings.LastIndex(path, "/")])
	file, err := os.Create(path)
	defer file.Close()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("输出中位图", scaleX, scaleY, path)
	png.Encode(file, min)
}
