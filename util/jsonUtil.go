/**
JSON支持
omitempty 如果为空就忽略
- 直接忽略
*/
package util

import (
	"github.com/json-iterator/go"
)

//@import go.
var json = jsoniter.ConfigCompatibleWithStandardLibrary

/**
将一个结构体转换成json字符串
@param v 结构体
//@return ""
*/
func JSONString(v interface{}) string {
	b, err := json.Marshal(v)
	if err != nil {
		return ""
	}
	return string(b[:])
}

//@return []byte{}
func JSONBytes(v interface{}) []byte {
	b, err := json.Marshal(v)
	if err != nil {
		return nil
	}
	return b
}

/**
将一个结构体转换成json字符串
@param bodyStr json字符串
@param v 结构体的指针
*/
//@return nil
func JSONStruct(bodyStr string, v interface{}) error {
	return json.Unmarshal([]byte(bodyStr), v)
}

//@return nil
func JSONStructByte(bytes []byte, v interface{}) error {
	return json.Unmarshal(bytes, v)
}

//获取一个泛型对象某个字段
func JSONGetProp(obj interface{}, propName string) interface{} {
	if jsonObj, ok := obj.(map[string]interface{}); ok {
		if _, exists := jsonObj[propName]; exists {
			return jsonObj[propName]
		} else {
			return nil
		}
	}
	return nil
}
