package util

import (
	"math"
)

type Quaternion struct {
	X, Y, Z, W float64
}

func EulerToQuaternion(x, y, z float64) *Quaternion {
	// 将欧拉角转换为弧度
	xRad := x * math.Pi / 180.0
	yRad := y * math.Pi / 180.0
	zRad := z * math.Pi / 180.0

	cosX := math.Cos(xRad / 2)
	cosY := math.Cos(yRad / 2)
	cosZ := math.Cos(zRad / 2)

	sinX := math.Sin(xRad / 2)
	sinY := math.Sin(yRad / 2)
	sinZ := math.Sin(zRad / 2)

	// 计算四元数的各个分量
	xPrime := sinX*cosY*cosZ - cosX*sinY*sinZ
	yPrime := cosX*sinY*cosZ + sinX*cosY*sinZ
	zPrime := cosX*cosY*sinZ + sinX*sinY*cosZ
	wPrime := cosX*cosY*cosZ - sinX*sinY*sinZ

	return &Quaternion{xPrime, yPrime, zPrime, wPrime}
}
