package util

var ShortNameSeed string = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"

type ShortNamer struct {
	Idx []int
	L   int
}

func (this *ShortNamer) Next() string {
	if this.L == 0 {
		this.L = len(ShortNameSeed)
		this.Idx = []int{0}
	}
	key := ""
	for i := 0; i < len(this.Idx); i++ {
		key += ShortNameSeed[this.Idx[i] : this.Idx[i]+1]
	}
	key += "_"
	//进位
	jw := true
	//增加一位
	addb := false
	for i := len(this.Idx) - 1; i >= 0; i-- {
		if jw {
			this.Idx[i] = this.Idx[i] + 1
		}
		if this.Idx[i] >= this.L {
			this.Idx[i] = 0
			jw = true
			if i == 0 {
				addb = true
			}
		} else {
			jw = false
		}
	}
	if addb {
		this.Idx = append(this.Idx, 0)
	}
	return key
}
