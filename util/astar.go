package util

var (
	eigthArr [][]int = [][]int{{0, -1, 10}, {1, -1, 14}, {1, 0, 10}, {1, 1, 14}, {0, 1, 10}, {-1, 1, 14}, {-1, 0, 10}, {-1, -1, 14}}
)

//寻路接口
type IMapData interface {
	/**行数 */
	RowCount() int
	/**列数*/
	ColCount() int
	/**某个行列是否可通行(默认) */
	IsDefWalkable(x int, y int) bool
	/**某个点是否可叠加通行 */
	IsWalkable(x int, y int, cross bool) bool
	/**是否超出边界 */
	CheckBlock(x int, y int) bool
}

//导航节点
type AStarNode struct {
	x          int
	y          int
	state      int
	g          int
	h          int
	f          int
	nodeIndex  int
	nears      []*AStarNode
	parentNode *AStarNode
}

func (t *AStarNode) destructor() {
	t.x = 0
	t.y = 0
	t.g = 0
	t.h = 0
	t.f = 0
	t.parentNode = nil
	t.state = 0
	t.nodeIndex = 0
	t.nears = nil
}

type AStar struct {
	inited               bool
	openlist             []*AStarNode
	closelist            []*AStarNode
	_nodepPool           []*AStarNode
	_index               int
	lastFindPathUsedNode []*AStarNode
	_tox                 int
	_toy                 int
	_fromx               int
	_fromy               int
	_cross               bool
	cacheObjects         [][]*AStarNode
	_nodeListPool        [][]*AStarNode
	_mapData             IMapData
	_standon             int
}

//初始化导航
func (t *AStar) Init(mapdata IMapData) {
	if !t.inited {
		t.closelist = make([]*AStarNode, 0, 2)
		t.openlist = make([]*AStarNode, 0, 2)
		t.lastFindPathUsedNode = make([]*AStarNode, 0, 2)
		t._nodeListPool = make([][]*AStarNode, 0, 2)
		t.inited = true
		t._nodepPool = make([]*AStarNode, 100000, 100000)
		for i := 0; i < 100000; i++ {
			t._nodepPool[i] = &AStarNode{}
		}
		t._index = len(t._nodepPool) - 1
	}
	t._mapData = mapdata
	nRow := mapdata.RowCount()
	nCol := mapdata.ColCount()
	t.cacheObjects = make([][]*AStarNode, nRow, nRow)
	var nlist []*AStarNode
	for y := 0; y < nRow; y++ {
		if len(t._nodeListPool) > 0 {
			nlist = t._nodeListPool[len(t._nodeListPool)-1]
			t._nodeListPool = t._nodeListPool[:len(t._nodeListPool)-1]
		} else {
			nlist = make([]*AStarNode, nCol, nCol)
		}
		nlist = nlist[:nCol]
		t.cacheObjects[y] = nlist
	}
	var node *AStarNode
	for x := 0; x < nCol; x++ {
		for y := 0; y < nRow; y++ {
			//必须初始默认是可通行的,这里才有初始化的必要。不可通行的地形不可逆转
			if mapdata.IsDefWalkable(x, y) {
				if t._index >= 0 {
					node = t._nodepPool[t._index]
					t._index--
				} else {
					node = &AStarNode{}
					t._nodepPool = append(t._nodepPool, node)
				}
				node.x = x
				node.y = y
				t.cacheObjects[y][x] = node
			} else {
				t.cacheObjects[y][x] = nil
			}
		}
	}
	//为所有的节点添加near初始化
	for x := 0; x < nCol; x++ {
		for y := 0; y < nRow; y++ {
			t.initEightBlocks(t.cacheObjects[y][x])
		}
	}
}

//初始化8方向
func (t *AStar) initEightBlocks(blockobj *AStarNode) {
	if blockobj == nil {
		return
	}
	fx := blockobj.x
	fy := blockobj.y
	var px, py int = 0, 0
	tempnears := make([]*AStarNode, 8, 8)
	var tempobj *AStarNode
	for i := 0; i < 8; i++ {
		var p = eigthArr[i]
		px = p[0] + fx
		py = p[1] + fy
		if (px < 0 || py < 0) || (py >= len(t.cacheObjects) || px >= len(t.cacheObjects[py])) {
			tempobj = nil
		} else {
			tempobj = t.getObject(px, py)
		}
		tempnears[i] = tempobj
	}
	blockobj.nears = tempnears
}

func (t *AStar) addEightBlocks(blockobj *AStarNode, tx int, ty int) {
	var fx = blockobj.x
	var fy = blockobj.y
	var px, py int = 0, 0
	var tempobj *AStarNode
	for i := 0; i < 8; i++ {
		var p = eigthArr[i]
		tempobj = blockobj.nears[i]
		if tempobj == nil {
			continue
		}
		px = p[0] + fx
		py = p[1] + fy
		if tempobj.state > 1 {
			continue
		}
		if !t._mapData.IsWalkable(px, py, t._cross) {
			tempobj.state = 2
			t.lastFindPathUsedNode = append(t.lastFindPathUsedNode, tempobj)
			continue
		}
		if tempobj.state == 1 {
			if blockobj.g+p[2] < tempobj.g {
				tempobj.g = blockobj.g + p[2]
				tempobj.h = (AbsInt(tx-px) + AbsInt(ty-py)) * 10
				tempobj.f = tempobj.g + tempobj.h
				tempobj.parentNode = blockobj
				t.findAndSort(tempobj)
			}
		} else {
			tempobj.g = blockobj.g + p[2]
			tempobj.h = (AbsInt(tx-px) + AbsInt(ty-py)) * 10
			tempobj.f = tempobj.g + tempobj.h
			tempobj.parentNode = blockobj
			tempobj.state = 1
			t.lastFindPathUsedNode = append(t.lastFindPathUsedNode, tempobj)
			t.insertAndSort(tempobj)
		}
	}
}

func (t *AStar) getObject(x int, y int) *AStarNode {
	return t.cacheObjects[y][x]
}

//重置所有的MapNode节点
func (t *AStar) resetAllNode() {
	var n *AStarNode
	for i := 0; i < len(t.lastFindPathUsedNode); i++ {
		n = t.lastFindPathUsedNode[i]
		n.parentNode = nil
		n.nodeIndex = 0
		n.state = 0
		n.g = 0
		n.f = 0
		n.h = 0
	}
	t.lastFindPathUsedNode = make([]*AStarNode, 0, 2)
}

func (t *AStar) clear() {
	t.resetObject()
	var list []*AStarNode
	for len(t.cacheObjects) > 0 {
		list = t.cacheObjects[len(t.cacheObjects)-1]
		t.cacheObjects = t.cacheObjects[:len(t.cacheObjects)-1]
		list = list[:1]
		t._nodeListPool = append(t._nodeListPool, list)
	}
}

func (t *AStar) resetObject() {
	if t.cacheObjects == nil {
		return
	}
	t.lastFindPathUsedNode = make([]*AStarNode, 0, 2)
	for i := 0; i < len(t.cacheObjects); i++ {
		count := 0
		for _, element := range t.cacheObjects[i] {
			if element != nil {
				count++
				element.destructor()
			}
		}
		t._index += count
	}
}

func (t *AStar) calu() [][]int {
	var curblockobj *AStarNode
	var fx = t._fromx
	var fy = t._fromy
	var tox = t._tox
	var toy = t._toy
	var x1 = 0
	var y1 = 0
	for len(t.openlist) > 0 {
		curblockobj = t.openlist[0]
		x1 = curblockobj.x
		y1 = curblockobj.y
		t.delAndSort()
		curblockobj.state = 2
		t.closelist = append(t.closelist, curblockobj)
		if AbsInt(x1-tox) <= 1 && AbsInt(y1-toy) <= 1 {
			var endp = t.getObject(tox, toy)
			endp.g = 0
			endp.h = 0
			endp.f = 0
			endp.parentNode = curblockobj
			curblockobj = endp
			x1 = curblockobj.x
			y1 = curblockobj.y
			t.lastFindPathUsedNode = append(t.lastFindPathUsedNode, endp)
			break
		}
		if x1 == tox && y1 == toy {
			break
		}
		t.addEightBlocks(curblockobj, tox, toy)
	}
	if x1 == tox && y1 == toy {
		var ret = make([][]int, 0, 2)
		for curblockobj != nil && !(x1 == fx && y1 == fy) {
			ret = append(ret, []int{x1, y1})
			curblockobj = curblockobj.parentNode
			x1 = curblockobj.x
			y1 = curblockobj.y
		}
		return ret
	} else {
		return make([][]int, 0, 2)
	}
}

/**
 * 删除根node后排序
 */
func (t *AStar) delAndSort() {
	var listLength = len(t.openlist)
	if listLength <= 1 {
		t.openlist = t.openlist[1:]
	} else {
		t.openlist[0] = t.openlist[listLength-1]
		t.openlist[0].nodeIndex = 0
		t.openlist = t.openlist[:listLength-1]
		listLength--
		var nodeIndex = 0
		var tmpNode *AStarNode = nil
		for (nodeIndex<<1)+1 < listLength {
			var smallNodeIndex = nodeIndex
			if t.openlist[nodeIndex].f > t.openlist[(nodeIndex<<1)+1].f {
				smallNodeIndex = (nodeIndex << 1) + 1
			}
			if (nodeIndex<<1)+2 < listLength {
				if t.openlist[smallNodeIndex].f > t.openlist[(nodeIndex<<1)+2].f {
					smallNodeIndex = (nodeIndex << 1) + 2
				}
			}
			if smallNodeIndex == nodeIndex {
				break
			} else {
				tmpNode = t.openlist[smallNodeIndex]
				t.openlist[smallNodeIndex] = t.openlist[nodeIndex]
				t.openlist[nodeIndex] = tmpNode
				t.openlist[nodeIndex].nodeIndex = nodeIndex
				t.openlist[smallNodeIndex].nodeIndex = smallNodeIndex
				nodeIndex = smallNodeIndex
			}
		}
	}
}

/**
 * 将node放到正确顺序
 */
func (t *AStar) findAndSort(node *AStarNode) {
	var listLength = len(t.openlist)
	if listLength < 1 {
		return
	}
	t.openlist[node.nodeIndex] = node
	var tmpNode *AStarNode = nil
	var nodeIndex = node.nodeIndex
	for nodeIndex > 0 {
		if t.openlist[nodeIndex].f <= t.openlist[((nodeIndex-1)>>1)].f {
			tmpNode = t.openlist[nodeIndex]
			t.openlist[nodeIndex] = t.openlist[((nodeIndex - 1) >> 1)]
			t.openlist[((nodeIndex - 1) >> 1)] = tmpNode
			t.openlist[nodeIndex].nodeIndex = nodeIndex
			nodeIndex = ((nodeIndex - 1) >> 1)
			t.openlist[nodeIndex].nodeIndex = nodeIndex
		} else {
			break
		}
	}
}

/**
 * 按由小到大顺序将节点插入到列表
 */
func (t *AStar) insertAndSort(node *AStarNode) {
	node.state = 1
	var listLength = len(t.openlist)
	if listLength == 0 {
		t.openlist = append(t.openlist, node)
		node.nodeIndex = listLength
	} else {
		t.openlist = append(t.openlist, node)
		node.nodeIndex = listLength
		var tmpNode *AStarNode = nil
		var nodeIndex = listLength
		for nodeIndex > 0 {
			if t.openlist[nodeIndex].f <= t.openlist[((nodeIndex-1)>>1)].f {
				tmpNode = t.openlist[nodeIndex]
				t.openlist[nodeIndex] = t.openlist[((nodeIndex - 1) >> 1)]
				t.openlist[((nodeIndex - 1) >> 1)] = tmpNode
				t.openlist[nodeIndex].nodeIndex = nodeIndex
				nodeIndex = ((nodeIndex - 1) >> 1)
				t.openlist[nodeIndex].nodeIndex = nodeIndex
			} else {
				break
			}
		}
	}
}

//导航 起点xy,终点xy
func (t *AStar) FindPath(fx int, fy int, tox int, toy int) [][]int {
	t._cross = false
	t._fromx = fx
	t._fromy = fy
	t.resetAllNode()
	t._tox = tox
	t._toy = toy
	t._standon = 0
	arr := t.startSearch()
	if arr == nil {
		arr = t.calu()
		if arr != nil && len(arr) > 0 && arr[0][0] == t._tox && t._toy == arr[0][1] {
			if !t._mapData.IsWalkable(t._tox, t._toy, t._cross) {
				arr = arr[1:]
			}
		}
	}
	if t._standon > 0 && arr != nil && len(arr) > 0 && arr[0][0] == tox && arr[0][1] == toy {
		arr = arr[1:]
	}
	return arr
}

func (t *AStar) startSearch() [][]int {
	if t._fromx == t._tox && t._fromy == t._toy {
		return [][]int{{t._tox, t._toy}}
	}
	var px int
	var py int
	var p []int
	if t._standon == 0 {
		if !t._mapData.CheckBlock(t._tox, t._toy) || !t._mapData.IsWalkable(t._tox, t._toy, t._cross) {
			return [][]int{}
		}
	} else {
		if t._standon > 0 || !t._mapData.IsWalkable(t._tox, t._toy, t._cross) {
			canWalk := false
			for _i := 0; _i < len(eigthArr); _i++ {
				p = eigthArr[_i]
				px = p[0] + t._tox
				py = p[1] + t._toy
				if t._mapData.CheckBlock(px, py) && t._mapData.IsWalkable(px, py, t._cross) {
					canWalk = true
				}
			}
			if !canWalk {
				return [][]int{}
			}
		}
	}
	t.openlist = make([]*AStarNode, 0, 2)
	t.closelist = make([]*AStarNode, 0, 2)
	var obj = t.getObject(t._tox, t._toy)
	obj.g = 0
	obj.h = 0
	obj.f = 0
	obj.parentNode = nil
	obj.state = 0
	t.lastFindPathUsedNode = append(t.lastFindPathUsedNode, obj)
	obj = t.getObject(t._fromx, t._fromy)
	if obj == nil {
		return nil
	}
	obj.g = 0
	obj.h = 0
	obj.f = 0
	obj.parentNode = nil
	obj.state = 1
	obj.nodeIndex = 0
	t.openlist = append(t.openlist, obj)
	t.lastFindPathUsedNode = append(t.lastFindPathUsedNode, obj)
	return nil
}
