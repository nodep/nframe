package util

import (
	"strings"
)

type CsvFile struct {
	Name  string
	Path  string
	Datas [][]string
}

//获取一个csv文件
//@return nil
func LoadCsv(path string) *CsvFile {
	str := FileStringGet(path)
	lines := strings.Split(str, "\r\n")
	f := &CsvFile{}
	args := strings.Split(path, "/")
	argStr := args[len(args)-1]
	argStr = argStr[:strings.LastIndex(argStr, ".")]
	f.Name = argStr
	f.Path = path
	f.Datas = make([][]string, 0, len(lines))
	for i := 0; i < len(lines); i++ {
		info := lines[i]
		if info == "" {
			continue
		}
		f.Datas = append(f.Datas, getCsvArgs(info))
	}
	return f
}

//解析获得一个csv行数据
//@return []string{}
func getCsvArgs(str string) []string {
	args := strings.Split(str, ",")
	args1 := make([]string, 0, 8)
	msg := ""
	needBack := false
	for i := 0; i < len(args); i++ {
		aa := args[i]
		if strings.Index(aa, "\"") < 0 {
			if msg == "" {
				args1 = append(args1, aa)
			} else {
				msg = msg + aa
				if !needBack {
					args1 = append(args1, msg)
					msg = ""
				}
			}
		} else {
			msg = msg + aa
			if strings.Count(msg, "\"")%2 == 0 {
				if needBack {
					msg = msg[1 : len(msg)-1]
				}
				msg = strings.ReplaceAll(msg, "\"\"", "\"")
				args1 = append(args1, msg)
				msg = ""
				needBack = false
			} else {
				needBack = true
			}
		}
	}
	return args1
}
