package util

import (
	"sync"
)

//key计数器
type KCountter struct {
	kc map[string]int
	sync.RWMutex
}

//@return 0
func (this *KCountter) Get(key string) (n int) {
	this.RLock()
	n = this.kc[key]
	this.RUnlock()
	return
}

func (this *KCountter) Clear(key string) {
	this.Lock()
	delete(this.kc, key)
	this.Unlock()
}

func (this *KCountter) Set(key string, value int) {
	this.Lock()
	this.kc[key] = value
	this.Unlock()
}

//@return 0
func (this *KCountter) Inc(key string) int {
	this.Lock()
	count := this.kc[key] + 1
	this.kc[key] = count
	this.Unlock()
	return count
}

//@return 0
func (this *KCountter) Dec(key string) int {
	this.Lock()
	count := this.kc[key] - 1
	this.kc[key] = count
	if count <= 0 {
		delete(this.kc, key)
	}
	this.Unlock()
	return count
}

//获取一个key countter
//@return nil
func NewKCountter() *KCountter {
	kc := &KCountter{}
	kc.kc = make(map[string]int)
	return kc
}
