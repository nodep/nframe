package util

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

var win_trim_slash = regexp.MustCompile(`^\/[c-zC-Z]:`)

/**
通过一个文件路径设置到一个结构体指针中
@param url 文件地址
@param v 结构体指针
//@return nil
*/
func FileGetInterface(url string, v interface{}) error {
	str := FileStringGet(url)
	startIndex := strings.Index(str, " //")
	endIndex := 0
	for startIndex >= 0 {
		endIndex = strings.Index(str[startIndex:], "\n")
		str = str[:startIndex] + str[startIndex+endIndex:]
		startIndex = strings.Index(str, " //")
	}
	startIndex = strings.Index(str, "\t//")
	for startIndex >= 0 {
		endIndex = strings.Index(str[startIndex:], "\n")
		str = str[:startIndex] + str[startIndex+endIndex:]
		startIndex = strings.Index(str, "\t//")
	}
	var bts = []byte(str)
	str = ""
	err := json.Unmarshal(bts, v)
	v = nil
	if err != nil {
		return err
	}
	return nil
}

/**
获取文件的字符串格式
@param url 通过该文件获取文件内的字符串
//@return ""
*/
func FileStringGet(url string) string {
	buff, err := ioutil.ReadFile(url)
	if err != nil {
		panic(err)
	}
	bodyStr := string(buff)
	buff = nil
	return bodyStr
}

//获取指定地址下的文本，如果文件不存在则直接返回""
func FileStringGetUnCheck(url string) string {
	if FileExist(url) {
		return FileStringGet(url)
	}
	return ""
}

//获取文件二进制
//@return []byte{}
func FileBytesGet(url string) []byte {
	buff, err := ioutil.ReadFile(url)
	if err != nil {
		fmt.Println(url, "not found")
		panic(err)
	}
	return buff
}

func FileWriteInterface(value interface{}, url string) {
	FileWriteString(JSONString(value), url)
}

func FileExist(url string) bool {
	finfo, err := os.Stat(url)
	return finfo != nil && err == nil
}

/**
字符串写文件
*/
func FileWriteString(value string, url string) {
	lidx := strings.LastIndex(url, "/")
	ps := url[:lidx]
	CreatePath(ps)
	f, err := os.OpenFile(url, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, os.ModePerm) //打开文件
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.Write([]byte(value)) //写入文件(字节数组)
}

func FileWriteBytes(value []byte, url string) {
	lidx := strings.LastIndex(url, "/")
	ps := url[:lidx]
	CreatePath(ps)
	f, err := os.OpenFile(url, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, os.ModePerm) //打开文件
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.Write(value) //写入文件(字节数组)
}

//@return []string{}
func FileListGetChildren(path string, filters []string) []string {
	return walkDirOneFileFilters(path, filters)
}

//是否有这个文件
func FileHas(path string) bool {
	f, e := os.Stat(path)
	return f != nil && e == nil
}

//如果没有文件则创建
func FileHasCreate(path string, fillStr string) {
	if !FileHas(path) {
		FileWriteString(fillStr, path)
	}
}

/*
注意：该方法消耗很大,建议在业务中进行缓存
获取一个文件或文件夹的最后变更时间
如果是一个文件则直接返回文件的最后变更时间
如果是一个文件夹,则返回该文件夹下所有文件最后变更时间中最大的那个
需要注意的是,文件夹要避免递归
*/
func FileGetModTime(path string) int64 {
	f, e := os.Stat(path)
	if f == nil || e != nil {
		return -1
	}
	if f.IsDir() {
		lastModTime := f.ModTime().Unix()
		allurls := FilelistGet(path, []string{})
		for _, url := range allurls {
			subf, e := os.Stat(url)
			if subf != nil && e == nil {
				subLastModTime := subf.ModTime().Unix()
				if subLastModTime > lastModTime {
					lastModTime = subLastModTime
				}
			}
		}
		return lastModTime
	} else {
		return f.ModTime().Unix()
	}
	//winFileAttr := f.Sys().(*syscall.Win32FileAttributeData)
	//return winFileAttr.LastAccessTime.Nanoseconds() / 1e6
	//return f.ModTime().Unix()
}

//获取一个文件的路径
func FileGetPath(url string) string {
	idx := strings.LastIndex(url, "/")
	return url[:idx+1]
}

/**
获取文件列表,根目录、需要文件过滤[.csv,.CSV]
@param path 根目录
@param filters [".csv",".CSV"]
//@return []string{}
*/
func FilelistGet(path string, filters []string) []string {
	files := walkDir(path, filters)
	return files
}

//@return []string{}
func walkDir(dirpath string, filters []string) []string {
	files, err := ioutil.ReadDir(dirpath) //读取目录下文件
	fps := []string{}
	if err != nil {
		return fps
	}
	flen := len(filters)
	for _, file := range files {
		if file.IsDir() {
			others := walkDir(dirpath+file.Name()+"/", filters)
			fps = append(fps, others...)
			continue
		} else {
			if flen == 0 {
				fps = append(fps, dirpath+file.Name())
			} else {
				for i := 0; i < flen; i++ {
					if strings.HasSuffix(file.Name(), filters[i]) {
						fps = append(fps, dirpath+file.Name())
						break
					}
				}
			}
		}
	}
	return fps
}

//下一级路径中所有文件
//@return []string{}
func FileChildrenFiles(path string) []string {
	files := walkDirOneFile(path)
	return files
}

/**
获取路径下的一级子路径
*/
//@return []string{}
func FileChildrenPath(path string) []string {
	files := walkDirOnePath(path)
	return files
}

//@return []string{}
func walkDirOneFile(dirpath string) []string {
	files, err := ioutil.ReadDir(dirpath) //读取目录下文件fps := []string{}
	fps := []string{}
	if err != nil {
		return fps
	}
	for _, file := range files {
		if !file.IsDir() {
			fps = append(fps, dirpath+file.Name())
		}
	}
	return fps
}

//@return []string{}
func walkDirOneFileFilters(dirpath string, filters []string) []string {
	files, err := ioutil.ReadDir(dirpath) //读取目录下文件fps := []string{}
	fps := []string{}
	flen := len(filters)
	if err != nil {
		return fps
	}
	for _, file := range files {
		if flen == 0 {
			fps = append(fps, dirpath+file.Name())
		} else {
			for i := 0; i < flen; i++ {
				if strings.HasSuffix(file.Name(), filters[i]) {
					fps = append(fps, dirpath+file.Name())
					break
				}
			}
		}
	}
	return fps
}

//@return []string{}
func walkDirOnePath(dirpath string) []string {
	files, err := ioutil.ReadDir(dirpath) //读取目录下文件fps := []string{}
	fps := []string{}
	if err != nil {
		return fps
	}
	for _, file := range files {
		if file.IsDir() {
			fps = append(fps, dirpath+file.Name()+"/")
		}
	}
	return fps
}

//获取文件名称
func GetFileName(path string) string {
	idx := strings.LastIndex(path, "/")
	return path[idx+1:]
}

/**
创建路径
*/
func CreatePath(path string) {
	exist, err := PathExists(path)
	if err != nil {
		return
	}
	if exist {
	} else {
		if path[len(path)-1:len(path)] == "/" {
			path = path[:len(path)-1]
		}
		lidx := strings.LastIndex(path, "/")
		ps := path[:lidx]
		CreatePath(ps)
		os.Mkdir(path, os.ModePerm)
	}
}

//@return false,nil
func PathExists(path string) (bool, error) {
	if win_trim_slash.Match([]byte(path)) {
		path = path[1:]
	}
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

//@return false
func FileIsUtf8(path string) bool {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return ValidUTF8(b)
}

//@return false
func ValidUTF8(buf []byte) bool {
	nBytes := 0
	for i := 0; i < len(buf); i++ {
		if nBytes == 0 {
			if (buf[i] & 0x80) != 0 { //与操作之后不为0，说明首位为1
				for (buf[i] & 0x80) != 0 {
					buf[i] <<= 1 //左移一位
					nBytes++     //记录字符共占几个字节
				}

				if nBytes < 2 || nBytes > 6 { //因为UTF8编码单字符最多不超过6个字节
					return false
				}

				nBytes-- //减掉首字节的一个计数
			}
		} else { //处理多字节字符
			if buf[i]&0xc0 != 0x80 { //判断多字节后面的字节是否是10开头
				return false
			}
			nBytes--
		}
	}
	return nBytes == 0
}

//拷贝文件夹
func FileCopyPath(from string, to string, p bool) {
	urls := FilelistGet(from, nil)
	flen := len(from)
	for _, v := range urls {
		toPath := to + v[flen:]
		if p {
			fmt.Println(toPath)
		}
		FileCopy(v, toPath)
	}
}

//拷贝文件夹包含一个排除字典,一般针对比较大的文件夹拷贝,所以采用递归的方式更合理
func FileCopyPathExcludes(from string, to string, p bool, exludes map[string]bool, basePath string) {
	urls := FileListGetChildren(from, nil)
	flen := len(basePath)
	for _, path := range urls {
		//--------排查过滤-------
		eindex := strings.Index(from, basePath) + len(basePath)
		subPath := from[eindex:]
		//判断是否进行了排除
		if exludes[subPath] {
			if p {
				fmt.Println("排除备份文件：", subPath)
			}
			continue
		}
		f, e := os.Stat(path)
		if e != nil || f == nil {
			continue
		}
		if f.IsDir() { //如果是目录
			FileCopyPathExcludes(path+"/", to, p, exludes, basePath)
			if p {
				fmt.Println(path, basePath)
			}
		} else { //拷贝具体一个文件
			toPath := to + path[flen:]
			if p {
				fmt.Println("备份文件", path)
			}
			FileCopy(path, toPath)
		}
	}

	// urls := FilelistGet(from, nil)
	// flen := len(from)
	// for _, v := range urls {
	// 	toPath := to + v[flen:]
	// 	if p {
	// 		fmt.Println(toPath)
	// 	}
	// 	FileCopy(v, toPath)
	// }
}

//拷贝文件
func FileCopy(from string, to string) {
	FileWriteBytes(FileBytesGet(from), to)
}

//文件移动
func FileMove(from string, to string, removeFrom bool) {
	if FileExist(from) {
		FileCopy(from, to)
		os.Remove(from)
	}
}

//获取一个文件的Md5值
func FileMd5(fileName string) string {
	h := md5.New()
	h.Write(FileBytesGet(fileName))
	return hex.EncodeToString(h.Sum(nil))
}

//获取一个文件最后的变更时间
func FileModTime(fileName string) string {
	f, e := os.Stat(fileName)
	if f == nil || e != nil {
		return "-1"
	}
	return strconv.FormatInt(f.ModTime().Unix(), 10)
}

func GetFileNameAndType(url string) (string, string) {
	fname := GetFileName(url)
	idx := strings.LastIndex(fname, ".")
	fileName := fname[:idx]
	fileType := fname[idx+1:]
	return fileName, fileType
}

//修复链接地址成/格式,并检查最后是否有/
func FixFileDivPath(url string) string {
	if url == "" {
		return ""
	}
	url = strings.ReplaceAll(url, "\\", "/")
	if url[len(url)-1:] != "/" {
		url = url + "/"
	}
	return url
}

//获取相对目录,开头不带/结束带/
//basePath为基础目录(根文件夹目录目录),fullPath为文件完整目录，获取fullPath-basePath
//用新的目录+获取到的子目录，即可获得一个全新的目录。且保持相对路径结构不变
func FilePathGetSub(basePath string, fullPath string) string {
	subPath := fullPath[len(basePath):]
	lastIdx := strings.LastIndex(subPath, "/")
	if lastIdx < 0 {
		return ""
	} else {
		return subPath[:lastIdx+1]
	}
}

//获取相对路径
func GetRelativePath(a, b string) (string, error) {
	relativePath, err := filepath.Rel(a, b)
	if err != nil {
		return "", err
	}
	if relativePath == "." {
		relativePath = "./"
	} else {
		relativePath = "./" + relativePath + "/"
	}
	return relativePath, nil
}

// package main

// import (
// 	"encoding/json"
// 	"fmt"
// )

// func main() {
// 	// 定义一个JSON对象的字节切片
// 	jsonData := []byte(`{
// 		"name": "John Doe",
// 		"age": 30,
// 		"email": "johndoe@example.com"
// 	}`)

// 	// 解析JSON对象
// 	var obj interface{}
// 	err := json.Unmarshal(jsonData, &obj)
// 	if err != nil {
// 		fmt.Println("JSON解析错误:", err)
// 		return
// 	}

// 	// 将interface{}类型的对象转换为map[string]interface{}类型
// 	if jsonObj, ok := obj.(map[string]interface{}); ok {
// 		// 动态读取字段
// 		if value, exists := jsonObj["name"]; exists {
// 			fmt.Println("name字段的值:", value)
// 		} else {
// 			fmt.Println("name字段不存在")
// 		}

// 		if value, exists := jsonObj["age"]; exists {
// 			fmt.Println("age字段的值:", value)
// 		} else {
// 			fmt.Println("age字段不存在")
// 		}

// 		if value, exists := jsonObj["email"]; exists {
// 			fmt.Println("email字段的值:", value)
// 		} else {
// 			fmt.Println("email字段不存在")
// 		}

// 		if value, exists := jsonObj["invalidField"]; exists {
// 			fmt.Println("invalidField字段的值:", value)
// 		} else {
// 			fmt.Println("invalidField字段不存在")
// 		}
// 	} else {
// 		fmt.Println("解析后的对象不是map类型")
// 	}
// }
