package util

//将字符串数组设置到map中,并计数量
func SetofStrsToStrsTag(from *([]string), out *(map[string]int)) {
	var fromArr []string
	var outMap map[string]int
	fromArr = *from
	outMap = *out
	for _, v := range fromArr {
		outMap[v] = outMap[v] + 1
	}
}
