package util

import (
	"encoding/binary"
)

//@return nil
func NewBytesBufferBigEndian(bts []byte) *BytesBufferBigEndian {
	re := &BytesBufferBigEndian{
		Pos:   0,
		bytes: bts,
	}
	return re
}

//@return nil
func NewBytesBufferLittleEndian(bts []byte) *BytesBufferLittleEndian {
	re := &BytesBufferLittleEndian{
		Pos:   0,
		bytes: bts,
	}
	return re
}

type BytesBufferLittleEndian struct {
	Pos   int
	bytes []byte
}

//@return 0
func (this *BytesBufferLittleEndian) ReadInt32() int32 {
	rst := int32(binary.LittleEndian.Uint32(this.bytes[this.Pos : this.Pos+4]))
	this.Pos += 4
	return rst
}

type BytesBufferBigEndian struct {
	Pos   int
	bytes []byte
}

//@return 0
func (this *BytesBufferBigEndian) ReadInt16() int16 {
	rst := int16(binary.BigEndian.Uint16(this.bytes[this.Pos : this.Pos+2]))
	this.Pos += 2
	return rst
}

//@return 0
func (this *BytesBufferBigEndian) ReadInt32() int32 {
	rst := int32(binary.BigEndian.Uint32(this.bytes[this.Pos : this.Pos+4]))
	this.Pos += 4
	return rst
}

//@return []byte{}
func (this *BytesBufferBigEndian) ReadBytes(l int) []byte {
	rst := this.bytes[this.Pos : this.Pos+l]
	this.Pos += l
	return rst
}
