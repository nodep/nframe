package util

import (
	"fmt"
	"testing"
)

var ct = NewKCountter()

func TestCountter(t *testing.T) {
	fmt.Println(ct)
}

func BenchmarkCountter(b *testing.B) {
	// b.N会根据函数的运行时间取一个合适的值
	for i := 0; i < b.N; i++ {
		ct.Inc("1")
	}
}

func BenchmarkCountterParallel(b *testing.B) {
	// 测试一个对象或者函数在多线程的场景下面是否安全
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			ct.Dec("1")
		}
	})
}
