package util

import (
	"io/ioutil"
	"net/http"
)

//Http请求
func HttpGet(url string) (error, string) {
	resp, err := http.Get(url)
	if err != nil {
		return err, ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	rst := string(body)
	return err, rst
}
