package util

import (
	"flag"
	"fmt"
	"os"
	"runtime/debug"
)

func FlagUsage() {
	fmt.Fprintf(os.Stderr, `Usage: protocolForXml [-output] [-xml]
Options:
`)
	flag.PrintDefaults()
}

//捕获异常
func CatchError() {
	if r := recover(); r != nil {
		FlagUsage()
		fmt.Println(r)
		debug.PrintStack()
	}
}

//输入阻断
func InputWait(str string) string {
	fmt.Println(str)
	str = ""
	fmt.Scanln(&str)
	return str
}
