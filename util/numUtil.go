package util

//返回比v大的最小2的幂
//@return 2
func Power2RoundupInt(v int) int {
	v--
	v |= v >> 1
	v |= v >> 2
	v |= v >> 4
	v |= v >> 8
	v |= v >> 16
	v++
	return int(v)
}

//取根
func QrtFloat64(base float64, times int64) float64 {
	var i int64
	var rst float64 = 1
	for i = 0; i < times; i++ {
		rst = rst * base
	}
	return rst
}

func AbsInt(a int) int {
	if a >= 0 {
		return a
	} else {
		return -a
	}
}
