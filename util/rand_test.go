package util

import (
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"testing"
)

var _wg sync.WaitGroup
var _bmmap map[int]*int32 = make(map[int]*int32)
var _countter int32

func TestRand(t *testing.T) {
	// RandInt31n(100)
	// for i := 0; i < 10; i++ {
	// 	var c int32 = 0
	// 	_bmmap[i] = &c
	// }
	// var ii int32 = 0
	// ss := &ii
	// atomic.AddInt32(ss, 1)
	// fmt.Println("ss->", *ss)
	fmt.Println("!!!!!!!!!!!!!!")
	fmt.Println((1 << 63) - 1)
	fmt.Println(uint64(1 << 63))
	rand.Seed(1000)
	fmt.Println(rand.Int63n(10000))
	fmt.Println(rand.Int63n(10000))
	fmt.Println(rand.Int63n(10000))
	fmt.Println(rand.Int63n(10000))
	fmt.Println(rand.Int63n(10000))
}

func BenchmarkRand(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RandInt31n(100)
	}
}

func BenchmarkRandParallel(b *testing.B) {
	defer func() {
		_wg.Wait()
		var totalCount int32
		fmt.Println("/////////////////////////////////")
		for i := 0; i < 10; i++ {
			totalCount += int32(*_bmmap[i])
			fmt.Printf("%d->%d\n", i, *_bmmap[i])
		}
		fmt.Println("总随机次数", totalCount, _countter)
	}()
	b.RunParallel(func(pb *testing.PB) {
		_wg.Add(1)
		for pb.Next() {
			v := RandIntn(100)
			atomic.AddInt32(_bmmap[v/10], 1)
			atomic.AddInt32(&_countter, 1)
		}
		_wg.Done()
	})
}
