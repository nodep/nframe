package util

import (
	"fmt"
	"time"

	"gitee.com/nodep/nframe/tran"
)

const (
	F1 string = "2006-01-02 15:04:05"
)

// 	  in1 := "2017/06/23"
//    in2 := "2017-07-24"
//    in3 := "2017/08/25 17:04:55"
//    in4 := "2017-08-26 15:04:55"
//    in5 := "16:08:59"

//当前时间格式字符串->2006-01-02 15:04:05
//@return ""
func TimeNowSec1() string {
	return time.Now().Format(F1)
}

//@return ""
func TimeFormat1(t time.Time) string {
	return t.Format(F1)
}

//毫秒获取time对象
//@return time.Now()
func GetTimeByMillisecond(msec int64) time.Time {
	t := time.Unix(0, msec*1e6)
	return t
}

//"2006-01-02 15:04:05"格式获取time对象
//@return time.Now()
func GetTimeByFormat1(str string) time.Time {
	tm, _ := time.ParseInLocation(F1, str, time.Local)
	return tm
}

//根据偏移获取 YYYYMMDD 格式字符串
//@return ""
func GetTodyYYYYMMDD(offsetDay int64) string {
	t := time.Unix(time.Now().Unix()+offsetDay*86400, 0)
	y, m, d := t.Date()
	return fmt.Sprintf("%d%02d%02d", y, m, d)
}

func GetTodayYYMMDDInt() int {
	str := GetTodyYYYYMMDD(0)
	str = str[2:]
	return tran.StringToInt(str)
}

//获取当前是第几周
func GetWeekNum(t time.Time) int {
	yearDay := t.YearDay()
	yearFirstDay := t.AddDate(0, 0, -yearDay+1)
	firstDayInWeek := int(yearFirstDay.Weekday())
	firstWeekDays := 1
	if firstDayInWeek != 0 {
		firstWeekDays = 7 - firstDayInWeek + 1
	}
	var week int
	if yearDay <= firstWeekDays {
		week = 1
	} else {
		week = (yearDay-firstWeekDays)/7 + 2
	}
	return week
}

//获取一个绝对的周数值
func SameWeek(t1 time.Time, t2 time.Time) bool {
	startWeekT := GetTimeByFormat1("1970-01-05 00:00:00")
	var weekDur int64 = 7 * 24 * 3600
	week1 := (t1.Unix() - startWeekT.Unix()) / weekDur
	week2 := (t2.Unix() - startWeekT.Unix()) / weekDur
	return week1 == week2
}

//是否是同一天
func IsSameDay(ts1, ts2 int64) bool {
	t1 := time.Unix(ts1/1000, 0)
	t2 := time.Unix(ts2/1000, 0)

	return t1.Year() == t2.Year() &&
		t1.Month() == t2.Month() &&
		t1.Day() == t2.Day()
}

//是否同一月
func IsSameMonth(ts1, ts2 int64) bool {
	t1 := time.Unix(ts1/1000, 0)
	t2 := time.Unix(ts2/1000, 0)

	return t1.Year() == t2.Year() &&
		t1.Month() == t2.Month()
}

//是否同一月
func IsSameYear(ts1, ts2 int64) bool {
	t1 := time.Unix(ts1/1000, 0)
	t2 := time.Unix(ts2/1000, 0)

	return t1.Year() == t2.Year()
}

//当前时间秒
func TimeSec() int64 {
	currentTime := time.Now()
	seconds := currentTime.Unix()
	return seconds
}
