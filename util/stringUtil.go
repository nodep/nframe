package util

import (
	"math"
	"strconv"
	"strings"

	"gitee.com/nodep/nframe/tran"
)

//文本格式化
//@return ""
func TrimRN(str string) string {
	args := strings.Split(str, "\r\n")
	str = ""
	for i := 0; i < len(args); i++ {
		str += args[i]
	}
	return str
}

//删除两侧
//@return ""
func TrimBtween(str string, rep string) string {
	rlen := len(rep)
	for strings.Index(str, rep) == 0 {
		str = str[rlen:]
	}
	for strings.Index(str, rep) == len(str)-rlen {
		str = str[:len(str)-rlen]
	}
	return str
}

//获取ipv4值
//@return ""
func IPv4(addr string) string {
	pos := strings.Index(addr, `:`)
	if pos == -1 {
		return addr
	} else {
		return addr[0:pos]
	}
}

//将字符串拆分成int32
func SplitInt32(str string, i string) []int32 {
	str = strings.TrimSpace(str)
	args := strings.Split(str, i)
	l := len(args)
	rst := make([]int32, 0, l)
	for i := 0; i < l; i++ {
		if args[i] != "" {
			rst = append(rst, tran.StringToInt32(args[i]))
		}
	}
	return rst
}

//将字符串 xxx#xxx&xxx#111拆分成一个map[int32]int64
func SplitMap_Int32_Int64(str, k1, k2 string) map[int32]int64 {
	str = strings.TrimSpace(str)
	args_1 := strings.Split(str, k1)
	l := len(args_1)
	if l == 0 {
		return nil
	}
	rst := make(map[int32]int64, l)
	for _, v := range args_1 {
		if v == "" {
			continue
		}
		vs := strings.Split(v, k2)
		rst[tran.StringToInt32(vs[0])] = tran.StringToInt64(vs[1])
	}
	return rst
}

//一个字符串,从中提取以start开始,end结束的字符串
func PickUpByKey(str string, start string, end string) string {
	startIndex := strings.Index(str, start)
	if startIndex < 0 {
		return ""
	}
	endIndex := strings.Index(str, end)
	if endIndex <= startIndex {
		return ""
	}
	return str[startIndex+len(start) : endIndex]
}

func UpperFirst(str string) string {
	return strings.ToUpper(str[:1]) + str[1:]
}

/*
str中是否包含arr中的片段
*/
func ArrayIndexOfString(str string, arr *[]string) int {
	idx := -1
	strArr := *arr
	for i := 0; i < len(strArr); i++ {
		if strings.Index(str, strArr[i]) >= 0 {
			idx = i
			break
		}
	}
	return idx
}

//arr中是否有完整的str
func ArrayIndexOf(str string, arr *[]string) int {
	idx := -1
	strArr := *arr
	for i := 0; i < len(strArr); i++ {
		if strArr[i] == str {
			idx = i
			break
		}
	}
	return idx
}

//查找并替换标签
func FindTagAndRep(oldString string, newString string, tagBegin string, tagEnd string, rep string) (string, int) {
	foundCount := 0
	beginIndex := strings.Index(oldString, tagBegin+"\n")
	endIndex := strings.Index(oldString, "\n"+tagEnd)
	customStr := ""
	flag := beginIndex >= 0 && endIndex >= 0
	for flag {
		if beginIndex+len(tagBegin+"\n") >= endIndex {
			customStr = ""
		} else {
			customStr = oldString[beginIndex+len(tagBegin+"\n") : endIndex]
			if customStr == "\r" {
				customStr = ""
			}
		}
		newString = strings.Replace(newString, rep, customStr, 1)
		oldString = oldString[:beginIndex+len("\n")] + oldString[endIndex+len(tagEnd):]
		beginIndex = strings.Index(oldString, tagBegin+"\n")
		endIndex = strings.Index(oldString, "\n"+tagEnd)
		flag = beginIndex >= 0 && endIndex >= 0
	}
	newString = strings.ReplaceAll(newString, rep, "")
	return newString, foundCount
}

//删除.之后的部分，如果有的话
func CutPoint(str string) string {
	idx := strings.Index(str, ".")
	if idx >= 0 {
		str = str[:idx] + str[idx+1:]
	}
	return str
}

//浮点数删除小数点5位之后的字符串
func CutFloatB(str string, count int) string {
	idx := strings.Index(str, ".")
	if idx < 0 {
		return str
	}
	backStr := str[idx+1:]
	if len(backStr) > count {
		parsed, _ := strconv.ParseFloat(str, 64)
		var ee6f float64 = 1
		for i := 0; i < count; i++ {
			ee6f *= 10
		}
		rounded := math.Round(parsed*ee6f) / ee6f
		roundedStr := strconv.FormatFloat(rounded, 'f', count, 64)
		return roundedStr
	} else {
		return str
	}
}

//去重
func StringDelRepeat(strs *[]string) {
	strArrr := *strs
	slen := len(strArrr)
	smap := map[string]bool{}
	str := ""
	for i := slen - 1; i >= 0; i-- {
		str = strArrr[i]
		if smap[str] {
			strArrr = append(strArrr[:i], strArrr[i+1:]...)
			continue
		}
		smap[str] = true
	}
}
