package util

import (
	"math/rand"
	"sync/atomic"
	"time"
)

//@ return nil
func NewRand(seed int64) *rand.Rand {
	return rand.New(rand.NewSource(seed))
}

var _seedSec int64 = 0

func seed() {
	s := time.Now().Unix()
	old := atomic.SwapInt64(&_seedSec, s)
	if old != s { //对比先后时间,若超过1秒，重新用纳秒重置种子
		rand.Seed(time.Now().UnixNano())
	}
}

func RandFloat32() float32 {
	seed()
	return rand.Float32()
}

func RandFloat64() float64 {
	seed()
	return rand.Float64()
}

//@return 0
func RandInt() int {
	seed()
	return rand.Int()
}

//@return 0
func RandIntn(n int) int {
	seed()
	return rand.Intn(n)
}

//@return 0
func RandInt31() int32 {
	seed()
	return rand.Int31()
}

//@return 0
func RandInt31n(n int32) int32 {
	seed()
	return rand.Int31n(n)
}

func RandInt63() int64 {
	seed()
	return rand.Int63()
}

//@return 0
func RandInt63n(n int64) int64 {
	seed()
	return rand.Int63n(n)
}
