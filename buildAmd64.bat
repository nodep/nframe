@echo off
set GOOS=windows
set GOARCH=amd64
go install gitee.com/nodep/nframe/cfg
go install gitee.com/nodep/nframe/db
go install gitee.com/nodep/nframe/log
go install gitee.com/nodep/nframe/net/tcp
go install gitee.com/nodep/nframe/profiling
go install gitee.com/nodep/nframe/timer
go install gitee.com/nodep/nframe/tran
go install gitee.com/nodep/nframe/util
go install gitee.com/nodep/nframe/uuid
@pause