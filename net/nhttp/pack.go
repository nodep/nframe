package nhttp

import (
	"sync"

	"gitee.com/nodep/nframe/util"
)

var PackInPool = sync.Pool{
	New: func() interface{} {
		return &PackIn{}
	},
}

var PackOutPool = sync.Pool{
	New: func() interface{} {
		return &PackOut{}
	},
}

func GetPackOut(cmd int32, code int32, rst interface{}) *PackOut {
	p := PackOutPool.Get().(*PackOut)
	p.Cmd = cmd
	p.Code = code
	if rst != nil && code == 0 {
		p.Rst = util.JSONString(rst)
	} else {
		p.Rst = ""
	}
	return p
}

type PackIn struct {
	Cmd   int32  `json:"cmd"`
	Token string `json:"token"`
	Body  string `json:"body"`
	Ipv4  string `json:"-"`
}

type PackOut struct {
	Cmd  int32  `json:"cmd"`
	Code int32  `json:"code"`
	Rst  string `json:"rst"`
}

func (this *PackOut) Bytes() []byte {
	return util.JSONBytes(this)
}
