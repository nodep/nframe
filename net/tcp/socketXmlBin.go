package tcp

import (
	"bytes"
	"encoding/binary"
	"net"

	"runtime/debug"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitee.com/nodep/nframe/cfg"
	"gitee.com/nodep/nframe/log"
	"gitee.com/nodep/nframe/tran"
	"gitee.com/nodep/nframe/util"
)

//通过配置获取一个socket
//@return nil
func NewSocketXmlBin(config *cfg.TcpServiceConfig) *socketXmlBin {
	skt := &socketXmlBin{
		_conf:          config,
		_handler:       make(map[int32]func(context *TcpContext, msg []byte)),
		_uncheckDic:    make(map[int32]bool),
		_idle:          1,
		_ipCountter:    util.NewKCountter(),
		_ipDurCountter: util.NewKCountter(),
		_hackCountter:  util.NewKCountter(),
	}
	return skt
}

type socketXmlBin struct {
	_closed        bool                                            //是否已关闭入口
	_kicked        bool                                            //是否kick所有玩家
	_handler       map[int32]func(context *TcpContext, msg []byte) //接口处理函数
	_conf          *cfg.TcpServiceConfig                           //配置文件
	_idle          int32                                           //当前是否空闲
	_uncheckDic    map[int32]bool                                  //不检查uuid得接口
	_ipCountter    *util.KCountter                                 //计数器
	_ipDurCountter *util.KCountter                                 //同ip地址时间段统计
	_hackCountter  *util.KCountter                                 //风险计数器
	_ipCheckTime   sync.Map                                        //ip地址最后一次检查时间
}

func (this *socketXmlBin) closeConn(slx *socketLink_XmlBin, ip string) {
	if !slx._closed {
		slx._closed = true
		this._ipCountter.Dec(ip)
		// t := this._ipCountter.Dec(ip)
		// if t <= 0 {
		// 	this._ipCheckTime.Delete(ip)
		// 	this._ipDurCountter.Clear(ip)
		// }
		slx.Close()
	}
}

//注册处理函数
//@return
func (this *socketXmlBin) AddHandler(cmd int32, handle func(context *TcpContext, msg []byte)) {
	this._handler[cmd] = handle
}

//添加不需要检测uuid得接口
func (this *socketXmlBin) RegistUncheckCmds(cmds []int32) {
	for _, cmd := range cmds {
		this._uncheckDic[cmd] = true
	}
}

//结束服务
func (this *socketXmlBin) Stop() {
	this._closed = true
}

//Kick所有玩家
func (this *socketXmlBin) KickAll() {
	this._kicked = true
}

//开始监听
func (this *socketXmlBin) Start() {
	defer func() {
		if r := recover(); r != nil {
			log.Admin.Error("socketXmlBin->recover by ", r)
		}
	}()
	idle := atomic.CompareAndSwapInt32(&this._idle, 1, 0)
	if !idle {
		return
	}
	this._kicked = false
	this._closed = false
	// var crt, key string
	// if this._conf.TlsCrt != "" && this._conf.TlsKey != "" {
	// 	crt = this._conf.TlsCrt
	// 	key = this._conf.TlsKey
	// }
	log.Admin.Sys("Start SocketXmlBin", this._conf.Port)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				log.Admin.Error("socketXmlBin go->recover by ", r)
				panic(r) //启动失败,必须抛出错误并结束进程
			}
		}()
		tcpAddr, err := net.ResolveTCPAddr("tcp4", ":"+this._conf.Port)
		checkErr(err)
		listener, err := net.ListenTCP("tcp4", tcpAddr)
		checkErr(err)
		for {
			conn, err := listener.Accept()
			if err != nil {
				if conn != nil {
					conn.Close()
				}
				continue
			}
			go this.lis(conn)
		}
	}()
}

func (this *socketXmlBin) lis(conn net.Conn) {
	var err error
	if this._closed {
		err = conn.Close()
		if err != nil {
			log.Game.Debug("socketXmlBin is closed ", err)
		}
	}
	slx := &socketLink_XmlBin{}
	slx.con = conn
	ip := slx.IPv4()
	if this._hackCountter.Get(ip) > 100 {
		//TODO 若设置了网关,需要告知网关此ip危险并广播给其他服务器一并屏蔽
		this.closeConn(slx, ip)
		return
	}
	//Whether the total number of links with the same ip address exceeds the limit
	if this._ipCountter.Inc(ip) > this._conf.SafeAddrTotal {
		log.Admin.Sys("该ip地址连接数过多->", ip)
		this.closeConn(slx, ip)
		return
	}
	//Same ip frequency
	ut := time.Now().Unix()
	utV, loaded := this._ipCheckTime.LoadOrStore(ip, ut)
	lastUt := utV.(int64)
	if loaded {
		if ut-lastUt > this._conf.SafeAddrSec {
			this._ipCheckTime.Store(ip, ut)
			this._ipDurCountter.Set(ip, 0)
		}
	}
	if this._ipDurCountter.Inc(ip) > this._conf.SafeAddrCount {
		log.Admin.Sys("链接频率过快->", ip)
		this.closeConn(slx, ip)
		return
	}
	//listen
	context := &TcpContext{
		Uuid:   "",
		SafeLv: LINK_unsafe,
		Count:  0,
		Link:   slx,
		User:   nil,
	}
	slx._context = context
	defer func() {
		rer := recover()
		if rer != nil {
			if rer == ERR_Dangerous {
				this._hackCountter.Inc(ip)
			}
			this.closeConn(slx, ip)
			if log.Admin.Lv >= log.Lv_Debug {
				log.Admin.Debug("[socketXmlBin recover]")
				log.Admin.Debug(rer)
				debug.PrintStack()
			}
		}
	}()
	//-------------------------------socket---------------------------------
	maxLen := this._conf.PackLenMax
	var buf []byte = make([]byte, 64, 64)
	var newBuff []byte
	full := false
	var pack *socketData = &socketData{
		msg: make([]byte, 0, this._conf.PackLenMax),
	}
	pack.clear()
	var checkTime int64 = time.Now().Unix()
	msgTotal := 0
	aveTime := this._conf.SafePackSec
	aveLimit := this._conf.SafePackLimit
	errorMsg := 0
	for {
		if this._kicked {
			log.Game.Debug("kicked")
			break
		}
		conn.SetReadDeadline(time.Now().Add(context.SafeLv))
		n, err := conn.Read(buf) //从conn中读取字符
		if err != nil {
			// log.Game.Debug("conn>Read", err)
			break
		} else {
			buf = buf[:n]
		}
		// buf = bytes.TrimRight(buf, "\x00") //去除buf尾部的所有0
		newBuff = append(newBuff, buf...)
		if len(newBuff) < 4 {
			continue
		}
		newBuff, full = pack.fromBytes(newBuff)
		if pack.Len >= maxLen || pack.Len <= 0 { //包的长度过大
			log.Game.Debug("packLen error", pack.Len)
			break
		}
		if !full { //一个完整的包
			continue
		}
		msgTotal += pack.Len
		usec := time.Now().Unix()
		if usec-checkTime > aveTime {
			checkTime = usec
			msgTotal = 0
		}
		if msgTotal > aveLimit {
			log.Game.Debug("msgTotal error", msgTotal, aveLimit)
			break
		}
		if context.Uuid == "" && !this._uncheckDic[pack.cmd] { //错误的封包请求
			errorMsg++
			log.Game.Error("cmd not allow unlogin= ", pack.cmd)
			if errorMsg >= 10 {
				break
			}
		}
		if this._handler[pack.cmd] != nil {
			this._handler[pack.cmd](context, pack.msg)
		} else {
			log.Game.Error("cmd not lis= ", pack.cmd)
			return
		}
		pack.clear()
	}
	this.closeConn(slx, ip)
}

//封包接收
type socketData struct {
	head    bool
	Len     int
	bodyLen int32
	cmd     int32
	flag    int16
	msg     []byte
}

//清除
func (this *socketData) clear() {
	this.Len = 0
	this.bodyLen = 0
	this.cmd = 0
	this.flag = 0
	this.msg = this.msg[:0]
	this.head = false
}

//解析封包.解析后的byte以及是否一个完整的包
//@return nil,true
func (this *socketData) fromBytes(buff []byte) ([]byte, bool) {
	b_buf := bytes.NewBuffer(buff)
	if !this.head {
		binary.Read(b_buf, binary.BigEndian, &this.bodyLen)
		this.Len = int(this.bodyLen)
	}
	if len(buff) < this.Len {
		this.head = true
		return buff, false //非完整的封包
	}
	if this.head {
		binary.Read(b_buf, binary.BigEndian, &this.bodyLen)
	}
	binary.Read(b_buf, binary.BigEndian, &this.cmd)
	binary.Read(b_buf, binary.BigEndian, &this.flag)
	this.msg = buff[10:this.Len]
	this.head = true
	return buff[this.Len:], true
}

//--------------------------------实现LinkVar接口---------------------------------

//协议处理器
type socketLink_XmlBin struct {
	con      net.Conn
	_context *TcpContext
	_closed  bool
}

//发送pack
func (this *socketLink_XmlBin) SendPack(cmd int32, pack TcpPack) {
	if pack == nil {
		this.SendBytes(cmd, nil)
	} else {
		this.SendBytes(cmd, pack.Marshal())
	}
}

//发送消息
func (this *socketLink_XmlBin) SendBytes(cmd int32, msg []byte) {
	if this._closed {
		return
	}
	if msg != nil {
		bf := make([]byte, 0, 10+len(msg))
		bf = append(bf, tran.Int32ToBytes_B(int32(10+len(msg)))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		bf = append(bf, msg...)
		this.con.Write(bf)
	} else {
		bf := make([]byte, 0, 10)
		bf = append(bf, tran.Int32ToBytes_B(int32(10))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		this.con.Write(bf)
	}
}

//IPv4
//@return ""
func (this *socketLink_XmlBin) IPv4() string {
	addr := this.con.RemoteAddr().String()
	pos := strings.Index(addr, `:`)
	if pos == -1 {
		return addr
	} else {
		return addr[0:pos]
	}
}

//关闭链接
func (this *socketLink_XmlBin) Close() {
	_ = this.con.Close()
	if this._context != nil && this._context.User != nil {
		this._context.User.Offline(this._context)
		this._context.User = nil
	}
}
