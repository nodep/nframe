package tcp

import (
	"errors"
)

var (
	ERR_RepeatKey = errors.New("repeat key")
	ERR_Dangerous = errors.New("dangerous")
)
