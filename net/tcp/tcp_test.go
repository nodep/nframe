package tcp

import (
	"fmt"
	"testing"

	"gitee.com/nodep/nframe/cfg"
)

func TestTcp(t *testing.T) {
	mgr := NewMgr()
	cf := &cfg.TcpServiceConfig{
		Port:          "8804",  //端口
		Url:           "/echo", //路径
		PackLenMax:    2048,    //请求封包最大长度
		SafePackSec:   5,       //多少秒检查一次累计封包
		SafePackLimit: 50000,   //规定时间范围内不能超过最大封包值
		SafeAddrSec:   1,       //IP地址链接检测单位时间
		SafeAddrCount: 10,      //IP地址单位时间允许最大链接数
		SafeAddrTotal: 50,      //同一个IP地址允许保持的最大链接数
		TlsCrt:        "",      //TLS证书 hunter.huamco.com.crt
		TlsKey:        "",      //TLS密钥 hunter.huamco.com.key
	}
	err := mgr.RegistService(TCP_Websocket, NewWebSocketXmlBin(cf))
	if err != nil {
		fmt.Println(err)
	}
	mgr.OpenService()
	mgr.OpenService()
	mgr.OpenService()
}
