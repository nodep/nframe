package tcp

import (
	"bytes"
	"encoding/binary"

	// "fmt"
	"net/http"
	"runtime/debug"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitee.com/nodep/nframe/util"

	"gitee.com/nodep/nframe/tran"

	"golang.org/x/net/websocket"

	"gitee.com/nodep/nframe/cfg"
	"gitee.com/nodep/nframe/log"
)

//通过配置文件创建一个websocketxmlBin
//@return nil
func NewWebSocketXmlBin(config *cfg.TcpServiceConfig) *webSocketXmlBin {
	ws := &webSocketXmlBin{
		_conf:          config,
		_handler:       make(map[int32]func(context *TcpContext, msg []byte)),
		_uncheckDic:    make(map[int32]bool),
		_idle:          1,
		_ipCountter:    util.NewKCountter(),
		_ipDurCountter: util.NewKCountter(),
	}
	return ws
}

type webSocketXmlBin struct {
	_closed        bool                                            //是否已关闭入口
	_kicked        bool                                            //是否kick所有玩家
	_handler       map[int32]func(context *TcpContext, msg []byte) //接口处理函数
	_conf          *cfg.TcpServiceConfig                           //配置文件
	_idle          int32                                           //当前是否空闲
	_uncheckDic    map[int32]bool                                  //不检查uuid得接口
	_ipCountter    *util.KCountter                                 //计数器
	_ipDurCountter *util.KCountter                                 //同ip地址时间段统计
	_ipCheckTime   sync.Map                                        //ip地址最后一次检查时间
}

//注册处理函数
//@return
func (this *webSocketXmlBin) AddHandler(cmd int32, handle func(context *TcpContext, msg []byte)) {
	this._handler[cmd] = handle
}

//添加不需要检测uuid得接口
func (this *webSocketXmlBin) RegistUncheckCmds(cmds []int32) {
	for _, cmd := range cmds {
		this._uncheckDic[cmd] = true
	}
}

//结束服务
func (this *webSocketXmlBin) Stop() {
	this._closed = true
}

//Kick所有玩家
func (this *webSocketXmlBin) KickAll() {
	this._kicked = true
}

//开始监听
func (this *webSocketXmlBin) Start() {
	defer func() {
		if r := recover(); r != nil {
			log.Admin.Error("webSocketXmlBin->recover by ", r)
		}
	}()
	idle := atomic.CompareAndSwapInt32(&this._idle, 1, 0)
	if !idle {
		return
	}
	this._kicked = false
	this._closed = false
	var crt, key string
	if this._conf.TlsCrt != "" && this._conf.TlsKey != "" {
		crt = this._conf.TlsCrt
		key = this._conf.TlsKey
	}
	log.Admin.Sys("Start WebSocketXmlBin", this._conf.Port)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				log.Admin.Error("webSocketXmlBin go->recover by ", r)
				panic(r) //启动失败,必须抛出错误并结束进程
			}
		}()
		http.Handle(this._conf.Url, websocket.Handler(this.lis))
		var err error
		if crt != "" && key != "" {
			err = http.ListenAndServeTLS(":"+this._conf.Port, crt, key, nil)
		} else {
			err = http.ListenAndServe(":"+this._conf.Port, nil)
		}
		if err != nil {
			panic(err)
		}
	}()
}

func (this *webSocketXmlBin) closeWs(wsl *wsLink_XmlBin, ip string) {
	if !wsl._closed {
		wsl._closed = true
		t := this._ipCountter.Dec(ip)
		if t <= 0 {
			this._ipCheckTime.Delete(ip)
			this._ipDurCountter.Clear(ip)
		}
		wsl.Close()
	}
}

//玩家链接
func (this *webSocketXmlBin) lis(ws *websocket.Conn) {
	var err error
	if this._closed {
		err = ws.Close()
		if err != nil {
			log.Game.Debug("websocketXmlBin is closed ", err)
		}
	}
	wsl := &wsLink_XmlBin{}
	wsl.con = ws
	ip := wsl.IPv4()
	//Whether the total number of links with the same ip address exceeds the limit
	if this._ipCountter.Inc(ip) > this._conf.SafeAddrTotal {
		log.Admin.Sys("该ip地址连接数过多->", ip)
		this.closeWs(wsl, ip)
		return
	}
	//Same ip frequency
	ut := time.Now().Unix()
	utV, loaded := this._ipCheckTime.LoadOrStore(ip, ut)
	lastUt := utV.(int64)
	if loaded {
		if ut-lastUt > this._conf.SafeAddrSec {
			this._ipCheckTime.Store(ip, ut)
			this._ipDurCountter.Set(ip, 0)
		}
	}
	if this._ipDurCountter.Inc(ip) > this._conf.SafeAddrCount {
		log.Admin.Sys("链接频率过快->", ip)
		this.closeWs(wsl, ip)
		return
	}
	//listen
	context := &TcpContext{
		Uuid:   "",
		SafeLv: LINK_unsafe,
		Count:  0,
		Link:   wsl,
		User:   nil,
	}
	wsl._context = context
	defer func() {
		rer := recover()
		if rer != nil {
			this.closeWs(wsl, ip)
			if log.Admin.Lv >= log.Lv_Debug {
				log.Admin.Debug("[websocketXmlBin recover]")
				log.Admin.Debug(rer)
				debug.PrintStack()
			}
		}
	}()
	errorMsg := 0
	maxLen := this._conf.PackLenMax
	aveTime := this._conf.SafePackSec
	aveLimit := this._conf.SafePackLimit
	msgTotal := 0
	msg := make([]byte, this._conf.PackLenMax)
	msgBody := make([]byte, 0, this._conf.PackLenMax)
	head := false
	var length int32 = 0
	var cmd int32 = 0
	var flag int16 = 0
	var checkTime int64 = time.Now().Unix()
	for {
		if this._kicked { //kick所有用户
			break
		}
		ws.SetReadDeadline(time.Now().Add(context.SafeLv * 5))
		n, err := ws.Read(msg)
		if err != nil || n > maxLen {
			break
		}
		msgTotal += n
		usec := time.Now().Unix()
		if usec-checkTime > aveTime {
			checkTime = usec
			msgTotal = 0
		}
		if msgTotal > aveLimit {
			break
		}
		if !head { //如果还没有读取头部
			head = true
			msgBody = append(msgBody, msg[:n]...)
			headBuf := bytes.NewBuffer(msgBody)
			binary.Read(headBuf, binary.BigEndian, &length)
			// fmt.Println("解析头部长度", length)
		} else {
			msgBody = append(msgBody, msg[:n]...)
			// fmt.Println("拼接")
		}
		//内容还没有加载完成
		if len(msgBody) < int(length) {
			// fmt.Println("长度不足，继续读取", len(msgBody), "/", int(length))
			continue
		}
		// fmt.Println("长度足够", len(msgBody), "/", int(length))
		b_buf := bytes.NewBuffer(msgBody)
		binary.Read(b_buf, binary.BigEndian, &length)
		binary.Read(b_buf, binary.BigEndian, &cmd)
		binary.Read(b_buf, binary.BigEndian, &flag)
		if context.Uuid == "" && !this._uncheckDic[cmd] { //错误的封包请求
			errorMsg++
			log.Game.Error("cmd not allow unlogin= ", cmd)
			if errorMsg >= 10 {
				break
			}
		}
		if length == 0 {
			// fmt.Println("长度不对，继续")
			continue
		}
		byt := msgBody[10:length]
		if this._handler[cmd] != nil {
			// if cmd == 1007 {
			// 	fmt.Println("请求了1007号接口")
			// }
			this._handler[cmd](context, byt)
		} else {
			log.Game.Error("cmd not lis= ", cmd)
		}
		head = false
		msgBody = msgBody[:len(msgBody)-int(length)]
	}
	// fmt.Println("关闭了")
	this.closeWs(wsl, ip)
}

//--------------------------------实现LinkVar接口---------------------------------

//协议处理器
type wsLink_XmlBin struct {
	con      *websocket.Conn
	_context *TcpContext
	_closed  bool
}

//发送pack
func (this *wsLink_XmlBin) SendPack(cmd int32, pack TcpPack) {
	this.SendBytes(cmd, pack.Marshal())
}

//发送消息
func (this *wsLink_XmlBin) SendBytes(cmd int32, msg []byte) {
	if this._closed {
		return
	}
	if msg != nil {
		bf := make([]byte, 0, 10+len(msg))
		bf = append(bf, tran.Int32ToBytes_B(int32(10+len(msg)))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		bf = append(bf, msg...)
		websocket.Message.Send(this.con, bf)
	} else {
		bf := make([]byte, 0, 10)
		bf = append(bf, tran.Int32ToBytes_B(int32(10))...)
		bf = append(bf, tran.Int32ToBytes_B(cmd)...)
		bf = append(bf, tran.Int16ToBytes_B(0)...)
		websocket.Message.Send(this.con, bf)
	}
}

//IPv4
//@return ""
func (this *wsLink_XmlBin) IPv4() string {
	addr := this.con.Request().RemoteAddr
	pos := strings.Index(addr, `:`)
	if pos == -1 {
		return addr
	} else {
		return addr[0:pos]
	}
}

//关闭链接
func (this *wsLink_XmlBin) Close() {
	_ = this.con.Close()
	if this._context != nil && this._context.User != nil {
		this._context.User.Offline(this._context)
		this._context.User = nil
	}
}
