package tcp

import (
	"time"

	"gitee.com/nodep/nframe/cfg"
)

const (
	LINK_unsafe   time.Duration = time.Second * 5  //未验证
	LINK_safe     time.Duration = time.Second * 10 //已验证
	TCP_Websocket string        = "TCP_Websocket"
	TCP_Socket    string        = "TCP_Socket"
)

//用户
type TcpUserVar interface {
	Kickout(context *TcpContext)      //离线
	Offline(context *TcpContext)      //离线
	Online(context *TcpContext)       //上线
	SendPack(cmd int32, pack TcpPack) //快速发送
	SendBytes(cmd int32, bts []byte)  //发送二进制
}

type TcpPack interface {
	Marshal() []byte
}

//link
type TcpLinkVar interface {
	SendBytes(cmd int32, msg []byte)
	SendPack(cmd int32, pack TcpPack)
	IPv4() string //获取链接的IP地址
	Close()       //关闭连接
}

//链接上下文
type TcpContext struct {
	Uuid   string        //用户uuid(全网唯一)
	SafeLv time.Duration //安全等级
	Count  int           //收到封包数量
	Link   TcpLinkVar    //链接
	User   TcpUserVar    //用户
}

//websocket,socket需要实现这个接口
type TcpService interface {
	AddHandler(cmd int32, handle func(context *TcpContext, msg []byte)) //注册处理函数
	RegistUncheckCmds(cmds []int32)                                     //添加不需要检测uuid得接口
	Start()                                                             //开始监听
	Stop()                                                              //结束服务
	KickAll()                                                           //Kick所有玩家
}

//获取一个tcp组
//@return nil
func NewMgr() *tcpMgr {
	mgr := &tcpMgr{}
	mgr._serviceMap = make(map[string]TcpService)
	mgr._cfMap = make(map[string]*cfg.TcpServiceConfig)
	return mgr
}

//tcp组
type tcpMgr struct {
	_serviceMap map[string]TcpService
	_cfMap      map[string]*cfg.TcpServiceConfig
}

//注册一个通信服务
//@return nil
func (this *tcpMgr) RegistService(key string, s TcpService) error {
	if this._serviceMap[key] != nil {
		return ERR_RepeatKey
	}
	this._serviceMap[key] = s
	return nil
}

func (this *tcpMgr) RegistConfig(key string, cf *cfg.TcpServiceConfig) {
	this._cfMap[key] = cf
}

func (this *tcpMgr) GetConfig(key string) *cfg.TcpServiceConfig {
	return this._cfMap[key]
}

//添加监听
//@return
func (this *tcpMgr) AddHandler(cmd int32, handle func(context *TcpContext, msg []byte)) {
	for _, s := range this._serviceMap {
		s.AddHandler(cmd, handle)
	}
}

//添加非安全列表
func (this *tcpMgr) RegistUncheckCmds(cmds []int32) {
	for _, s := range this._serviceMap {
		s.RegistUncheckCmds(cmds)
	}
}

//开启已注册得所有服务
func (this *tcpMgr) OpenService() {
	for _, s := range this._serviceMap {
		s.Start()
	}
}

//停止已注册得所有服务(并不Kick所有玩家)
func (this *tcpMgr) CloseService() {
	for _, s := range this._serviceMap {
		s.Stop()
	}
}

//懒方式踢出所有玩家
func (this *tcpMgr) KickAll() {
	for _, s := range this._serviceMap {
		s.KickAll()
	}
}

//关闭一个玩家会话
func contextClose(context *TcpContext) {
	if context.User != nil {
		context.User.Offline(context)
		context.User = nil
	}
	if context.Link != nil {
		context.Link.Close()
		context.Link = nil
	}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
