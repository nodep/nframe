package profiling

import (
	"net/http"
	_ "net/http/pprof"
)

/*
当前情况地址
http://127.0.0.1:8000/debug/pprof/
工具分析, Is Graphviz installed
go tool pprof -inuse_space http://127.0.0.1:8899/debug/pprof/heap
go tool pprof -alloc_space http://127.0.0.1:8899/debug/pprof/heap
导出
go tool pprof -inuse_space -cum -svg http://127.0.0.1:8899/debug/pprof/heap > heap_inuse.svg
go tool pprof -alloc_space -cum -svg http://127.0.0.1:8899/debug/pprof/heap > heap_inuse.svg
*/
func Start() {
	go func() {
		http.ListenAndServe("0.0.0.0:8000", nil)
	}()
}
