package uuid

import (
	"fmt"
	"sync"
	"testing"
)

var (
	wk, _  = NewIdWorker(1, 1)
	wk2, _ = NewPrivateIdWorker()
	umap   sync.Map
)

func TestUuid(t *testing.T) {
	// tm, _ := time.ParseInLocation("2006-01-02 15:04:05", "2019-11-21 13:19:00", time.Local)
	// fmt.Println(tm.Unix())
	fmt.Println(wk.NextString())
	fmt.Println(wk.NextString())
	fmt.Println(wk.NextString())
	fmt.Println(wk.NextString())
	// uids := wk.GetStrings(10)
	// for _, v := range uids {
	// 	fmt.Println(v)
	// }
	// fmt.Println(wk.NextString())
}

// func TestUid(t *testing.T) {
// 	fmt.Println(wk2.NextInt64())
// 	fmt.Println(wk2.GetInt64s(5))
// }

// // 性能测试
// func BenchmarkUuid(b *testing.B) {
// 	// b.N会根据函数的运行时间取一个合适的值
// 	for i := 0; i < b.N; i++ {
// 		wk.NextString()
// 	}
// }

// // 并发性能测试
// func BenchmarkUuidParallel(b *testing.B) {
// 	// 测试一个对象或者函数在多线程的场景下面是否安全
// 	b.RunParallel(func(pb *testing.PB) {
// 		for pb.Next() {
// 			wk.NextString()
// 		}
// 	})
// }
