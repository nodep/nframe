package uuid

import (
	"errors"
)

var (
	ERR_NodeId     = errors.New("uuid生成器节点号越界")
	ERR_DistrictId = errors.New("uuid生成器区域号越界")
)
