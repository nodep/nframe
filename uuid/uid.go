package uuid

import (
	"strconv"
	"sync/atomic"
	"time"

	"gitee.com/nodep/nframe/util"
)

const (
	//http://www.matools.com/timestamp
	psequenceBits       uint = 31                         //自增ID
	ptimestampLeftShift      = psequenceBits              //时间戳左移位数
	psequenceMask            = -1 ^ (-1 << psequenceBits) //自增ID最大值
)

// NewIdWorker new a snowflake id generator object.
//@return nil,nil
func NewPrivateIdWorker() (*PidWorker, error) {
	worker := &PidWorker{}
	worker.twepoch = time.Now().Unix() - util.GetTimeByFormat1(twepoch).Unix()
	return worker, nil
}

type PidWorker struct {
	sequence int64 //序号
	twepoch  int64 //默认开始时间
}

//@return ""
func (this *PidWorker) NextString() string {
	return strconv.FormatInt(this.NextInt64(), 10)
}

//@return 0
func (this *PidWorker) NextInt64() int64 {
	seqId := atomic.AddInt64(&this.sequence, 1)
	timestamp := this.twepoch + seqId/psequenceMask
	seqId = seqId % psequenceMask
	return (timestamp << ptimestampLeftShift) | seqId
}

//@return []int64{}
func (this *PidWorker) GetInt64s(num int64) []int64 {
	seqId := atomic.AddInt64(&this.sequence, num)
	ids := make([]int64, 0, num)
	for num >= 0 {
		num--
		id := seqId - num
		timestamp := this.twepoch + id/psequenceMask
		id = id % psequenceMask
		ids = append(ids, (timestamp<<ptimestampLeftShift)|id)
	}
	return ids
}

//@return []string{}
func (this *PidWorker) GetStrings(num int64) []string {
	seqId := atomic.AddInt64(&this.sequence, num)
	ids := make([]string, 0, num)
	for num > 0 {
		num--
		id := seqId - num
		timestamp := this.twepoch + id/psequenceMask
		id = id % psequenceMask
		ids = append(ids, strconv.FormatInt((timestamp<<ptimestampLeftShift)|id, 10))
	}
	return ids
}
