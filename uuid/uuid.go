package uuid

import (
	"strconv"
	"sync/atomic"
	"time"

	"gitee.com/nodep/nframe/util"
)

const (
	//http://www.matools.com/timestamp
	twepoch        string = "2019-11-21 13:19:00" //默认开始时间
	districtIdBits uint   = 8                     //区域 最多支持255
	nodeIdBits     uint   = 13                    //节点 最多支持8181
	sequenceBits   uint   = 10                    //自增ID 1023

	/*
	 * 1 符号位  |  32 时间戳                             |  8 区域  	|  	  13 节点       |    10自增ID
	 * 0        |  00000000 00000000 00000000 00000000  | 00000000  | 00000000 00000   |  00000000 00
	 */
	maxNodeId     = -1 ^ (-1 << nodeIdBits)     //节点 ID 最大范围
	maxDistrictId = -1 ^ (-1 << districtIdBits) //最大区域范围

	nodeIdShift        = sequenceBits                               //节点左移位数
	districtIdShift    = sequenceBits + nodeIdBits                  //区域左移位数
	timestampLeftShift = sequenceBits + nodeIdBits + districtIdBits //时间戳左移位数
	sequenceMask       = -1 ^ (-1 << sequenceBits)                  //自增ID最大值
)

/*
NodeId 节点8181
districtId 区域255
*/
//@return nil,nil
func NewIdWorker(NodeId int64, districtId int64) (*IdWorker, error) {
	worker := &IdWorker{}
	if NodeId > maxNodeId || NodeId < 0 {
		return nil, ERR_NodeId
	}
	if districtId > maxDistrictId || districtId < 0 {
		return nil, ERR_DistrictId
	}
	worker.nodeId = NodeId
	worker.districtId = districtId
	worker.twepoch = time.Now().Unix() - util.GetTimeByFormat1(twepoch).Unix()
	return worker, nil
}

type IdWorker struct {
	sequence   int64 //序号
	nodeId     int64 //节点ID
	twepoch    int64 //默认开始时间
	districtId int64 //区域号
}

//@return ""
func (this *IdWorker) NextString() string {
	return strconv.FormatInt(this.NextInt64(), 10)
}

//@return 0
func (this *IdWorker) NextInt64() int64 {
	seqId := atomic.AddInt64(&this.sequence, 1)
	timestamp := this.twepoch + seqId/sequenceMask
	seqId = seqId % sequenceMask
	return (timestamp << timestampLeftShift) | (this.districtId << districtIdShift) | (this.nodeId << nodeIdShift) | seqId
}

//@return []int64{}
func (this *IdWorker) GetInt64s(num int64) []int64 {
	seqId := atomic.AddInt64(&this.sequence, num)
	ids := make([]int64, 0, num)
	for num >= 0 {
		num--
		id := seqId - num
		timestamp := this.twepoch + id/sequenceMask
		id = id % sequenceMask
		ids = append(ids, (timestamp<<timestampLeftShift)|(this.districtId<<districtIdShift)|(this.nodeId<<nodeIdShift)|id)
	}
	return ids
}

//@return []string{}
func (this *IdWorker) GetStrings(num int64) []string {
	seqId := atomic.AddInt64(&this.sequence, num)
	ids := make([]string, 0, num)
	for num > 0 {
		num--
		id := seqId - num
		timestamp := this.twepoch + id/sequenceMask
		id = id % sequenceMask
		ids = append(ids, strconv.FormatInt((timestamp<<timestampLeftShift)|(this.districtId<<districtIdShift)|(this.nodeId<<nodeIdShift)|id, 10))
	}
	return ids
}
